require_recipe 'apt'
require_recipe 'gems'
require_recipe 'apache2'
require_recipe 'apache2::mod_rewrite'
require_recipe 'apache2::mod_php5'
require_recipe 'php::module_curl'
require_recipe 'php::module_mysql'
require_recipe 'php::module_gd'
require_recipe 'mysql::server'


web_app "local.dev" do
  server_name "local.dev"
  server_aliases "www.local.dev"
  docroot "/htdocs/site/www"
end


package "make" do
  action :install
end


gem_package "mysql" do
  source "http://rubygems.org/"
end


mysql_connection_info = {:host => node['mysql']['mysql_connection_info']['host'], :username => node['mysql']['mysql_connection_info']['username'], :password => node['mysql']['server_root_password']}


mysql_database_user 'vagrant' do
  connection mysql_connection_info
  password node['mysql']['server_root_password']
  host '%'
  action :grant
end


mysql_database 'create project database' do
  connection mysql_connection_info
  database_name node[:project_name]
  action :create
end


if FileTest.exist?( "/assets/db/#{node[:project_name]}.sql.gz" )
  execute "import #{node[:project_name]}.sql.gz" do
    user "root"
    cwd "/project/db"
    command "zcat #{node[:project_name]}.sql.gz | mysql -u root -p#{node[:mysql][:server_root_password]} #{node[:project_name]}"
    action :run
  end
end


cron "Hourly MySql Dump" do
  minute "*/60"
  command "mysqldump -u root -proot --databases #{node[:project_name]} | gzip > /project/db/_hourly_#{node[:project_name]}.sql.gz"
end