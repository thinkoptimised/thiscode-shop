#!/bin/bash

# Usage: ./deploy.sh [host]

# qF37NrXn1dep-test

host="${1:-root@31.222.155.37}"

# The host key might change when we instantiate a new VM, so
# we remove (-R) the old host key from known_hosts
ssh-keygen -R "${host#*@}" 2> /dev/null

tar cj _builds/cookbooks/ _builds/install.sh _builds/solo.json _builds/solo.rb | ssh -o 'StrictHostKeyChecking no' "$host" '
sudo rm -rf ~/chef &&
mkdir ~/chef &&
cd ~/chef &&
tar xj &&
cd _builds/ &&
sudo bash install.sh'