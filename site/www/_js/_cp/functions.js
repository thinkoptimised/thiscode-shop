function table_sorter()
{
  //Fix for collapsing widths when user trys to drag the item
  var fixHelper = function(e, ui) {
    ui.children().each(function() {
      $(this).width($(this).width());
    });
    return ui;
  };

  $('.sortable').mouseenter(function () {

    // Set the ID to use for sorting
    var table_sorter_id = $(this).children('tbody').attr('id');

    $( "#"+table_sorter_id ).sortable({
        helper: fixHelper,
        forcePlaceholderSize: true,
        forceHelperSize: true,
        containment: 'parent',
        axis: 'y',
        placeholder: 'ui-state-highlight',
        stop: function(event, ui) {
          var order = $('#'+table_sorter_id).sortable('serialize');
          var url = $('#'+table_sorter_id).attr('data-posturl');
          $.post(url, order);
        }
      });
    $( ".sortable" ).disableSelection();
  });
}


function create_chart()
{
  $('.flot_chart').resize();

  var options = {
      lines: { show: true },
      points: { show: true },
      xaxis: { mode: "time", timeformat: "%d %b" },
      grid: { hoverable: true, clickable: true }
  };

  var data = [];

  var dataurl = $('.flot_chart').attr('data-charturl');

  $.getJSON(dataurl, function(result) {
     $.each(result.visits, function(i, item) {
        data.push(item);
     });
     $.plot('.flot_chart', data, options);
  });

  function showTooltip(x, y, contents) {
    $('<div id="flot_chart_tooltip">' + contents + '</div>').css( {
        top: y -20,
        left: x + 15
    }).appendTo("body").fadeIn(200);
  }

  var previousPoint = null;
  $(".flot_chart").bind("plothover", function (event, pos, item) {

    if(item)
    {
      if(previousPoint != item.dataIndex)
      {
        previousPoint = item.dataIndex;
        $("#flot_chart_tooltip").remove();
        var x = moment(item.datapoint[0]).format('Do MMMM');
        var y = item.datapoint[1];
        showTooltip(item.pageX, item.pageY, item.series.label + " on " + x + " = " + y);
      }
    }
    else
    {
      $("#flot_chart_tooltip").remove();
      previousPoint = null;
    }
  });
}