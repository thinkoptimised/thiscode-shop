$(document).ready(function() {

    $(".alert").alert();
    $("#accordion-group").collapse('hide');
    $('[rel="popover"]').popover( {trigger: 'hover'});

    /* disabled links shouldn't work */
    $('a.disabled').click(function() { return false; });

    if($('.sortable').length > 0) { table_sorter(); }
    if($('.flot_chart').length > 0) { create_chart();}

});