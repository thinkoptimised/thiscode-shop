<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  $config['upload_path'] = '../uploads/documents';
  $config['allowed_types'] = 'doc|docx|ppt|pptx|pdf|jpeg|gif|png|jpg|csv|xls|xlsx'; 
  $config['encrypt_name'] = TRUE;
