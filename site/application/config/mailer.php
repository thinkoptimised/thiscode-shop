<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  $config['protocol'] = 'smtp';
  $config['smtp_auth'] = false;
  $config['smtp_timeout'] = 10;
  $config['smtp_host'] = '33.33.33.1';
  $config['smtp_user'] = '';
  $config['smtp_pass'] = '';
  $config['smtp_port'] = 2525;
