<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  if(preg_match('/local\.dev/',$_SERVER['SERVER_NAME']))
  {
    // Dev config
    $config['protocol'] = 'smtp';
    $config['smtp_host'] = '33.33.33.1';
    $config['smtp_port'] = 2525;
    $config['smtp_user'] = '';
    $config['smtp_pass'] = '';
    $config['validate'] = true;
  }
  else
  {
    // Live config
    $config['protocol'] = 'sendmail';
  }
