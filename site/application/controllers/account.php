<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Public_Controller {

  public function register()
  {
    $member = new Member;

    if($this->is_post())
    {
      $member->name = $this->input->post('name');
      $member->company = $this->input->post('company');
      $member->email = $this->input->post('email');
      if($member->save())
      {
        $email_data = array(
          'member' => $member,
          'site_name' => Setting::get('site_name'),
          'admin_url' => config_item('admin_url'),
          'email_content' => Email_template::findByShortName('member_register_admin')
        );
        // Send email to admin
        $this->mailer->subject(Setting::get('site_name').' - Member Registration Notification')
                     ->to_name(Setting::get('admin_name'))
                     ->to_email(Setting::get('admin_email'))
                     ->from_name(Setting::get('member_invite_email_from_name'))
                     ->from_email(Setting::get('member_invite_email_from_email'))
                     ->template('emails/email_content.tpl')
                     ->template_data($email_data);
        $this->mailer->send();

        $email_data = null;
        $email_data = array(
          'member' => $member,
          'site_name' => Setting::get('site_name'),
          'email_content' => Email_template::findByShortName('member_register')
        );
        // Send email to user
        $this->mailer->subject('Thanks for registering your interest in '.Setting::get('site_name'))
                     ->to_name($member->name)
                     ->to_email($member->email)
                     ->from_name(Setting::get('member_invite_email_from_name'))
                     ->from_email(Setting::get('member_invite_email_from_email'))
                     ->template('emails/email_content.tpl')
                     ->template_data($email_data);
        $this->mailer->send();
        // Redirect to thank you
        $this->redirectTo('/thank-you-for-registering');
      }
      else
      {
        $this->addTemplateData(array('error' => true));
      }
    }

    $page = Page::findBySlug('member-register',0);
    $this->render('front/members/register.tpl',array(
      'member' => $member,
      'page' => $page
    ));
  }

	public function login()
	{

    if($this->is_post())
    {
      $member = Member::authenticate($this->input->post('email'),$this->input->post('password'));
      if($member === FALSE)
      {
        $this->setFlash('Your acount could not be found, please check your email and password and try again.','error');
        $this->redirectTo('/account/login');
      }

      $this->loginUser($member);
      $this->redirectTo('/members/dashboard');
    }

    $page = Page::findBySlug('login',0);
    $this->render('front/members/login.tpl',array('page' => $page));
	}

	public function logout()
	{
    $this->logoutUser();
    $this->setFlash('You have been logged out','notice');
		$this->redirectTo('/account/login');
	}

  public function forgotten_password()
  {
    if($this->is_post())
    {
      $member = Member::findByEmail($this->input->post('email'));
      if($member !== false)
      {
        $member->initiate_password_reset();
        $email_data = array(
          'member' => $member,
          'reset_url' => config_item('base_url').'/account/reset_password/'.$member->password_reset_url_token(),
          'admin_url' => config_item('base_url'),
          'email_content' => Email_template::findByShortName('member_reset')
        );
        $this->mailer->subject(Setting::get('site_name').' - Reset your password')
                     ->to_name($member->name)
                     ->to_email($member->email)
                     ->from_name(Setting::get('member_invite_email_from_name'))
                     ->from_email(Setting::get('member_invite_email_from_email'))
                     ->template('emails/email_content.tpl')
                     ->template_data($email_data);
        if($this->mailer->send())
        {
          $this->setFlash('An email has been sent containing instructions on how to reset your password.','success');
          $this->redirectTo('/account/login');
        }
        else
        {
          $this->addTemplateData(array('error' => true));
        }
      }
      else
      {
        $this->addTemplateData(array('error' => true));
      }
    }
    $this->render('front/members/forgotten_password.tpl',array(
      'page' => Page::findBySlug('member-forgotten-password',0)
    ));
  }

  public function reset_password()
  {

    $token = $this->uri->segment(3,0);
    $member = Member::FindByResetToken($token);
    if($member === false)
    {
      $this->setFlash('There was a problem with your password reset, please try again.','error');
      $this->redirectTo('account/forgotten_password');
    }
    else
    {
      if($this->is_post())
      {
        $member->set_plain_text_password($this->input->post('password'));
        $member->set_plain_text_password_confirmation($this->input->post('password_confirmation'));
        if($member->is_valid())
        {
          $member->complete_password_reset();
          $member->save();
          $this->loginUser($member);
          $this->setFlash('Your password has been reset successfully, and you have been logged in.','success');
          $this->redirectTo('/members/dashboard');
        }
        else
        {
          $this->addTemplateData(array('error' => true));
        }
      }
      $this->render('front/members/reset.tpl',array(
        'member' => $member,
        'page' => Page::findBySlug('member-reset-password',0)
      ));
    }

  }

  public function activate()
  {

    $page = Page::findBySlug('account-activation',0);

    $token = $this->uri->segment(3,0);
    $member = Member::FindByActivationToken($token);
    if($member === false)
    {
      $this->setFlash('There was a problem with your account activation, please try again.','error');
      $this->redirectTo('account/login.tpl');
    }
    else
    {
      if($this->is_post())
      {
        $member->name = $this->input->post('name');
        $member->email = $this->input->post('email');
        $member->company = $this->input->post('company');
        $member->set_plain_text_password($this->input->post('password'));
        $member->set_plain_text_password_confirmation($this->input->post('password_confirmation'));
        if($member->is_valid())
        {
          $member->activate();
          $member->save();
          $this->loginUser($member);
          $this->setFlash('Your account has been activated, and you have been logged in.','success');
          $this->redirectTo('/members/dashboard');
        }
        else
        {
          $this->addTemplateData(array('error' => true));
        }
      }
      $this->render('front/members/activate.tpl',array('member' => $member,'page' => $page));
    }

  }

}
