<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Members extends Admin_Controller {

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->render('secure/members/home.tpl', array(
      'members' => Member::find('all')
    ));
  }

  public function edit()
  {

    $id = $this->uri->segment(4,0);
    if($id === 0 || $id === FALSE)
    {
      $this->setFlash('Cannot create a new member on the admin panel, please use the Register functionality on the site.','warning');
      $this->redirectTo('secure/members');
    }
    else
    {
      try {
        $member = Member::find($id);
      } catch(ActiveRecord\RecordNotFound $e) {
        $this->setFlash('Problem editing member - could not find the specified member','error');
        $this->redirectTo('/secure/members/');
      }
    }

    if($this->is_post())
    {

      $member->name = $this->input->post('name');
      $member->email = $this->input->post('email');
      $member->company = $this->input->post('company');

      if($member->save())
      {
        $this->setFlash('Member saved successfully','success');
        $this->redirectTo('/secure/members');
      }
      else
      {
        $this->addTemplateData(array('error' => true));
      }

    }

    $this->render('secure/members/edit.tpl',array(
      'member' => $member
    ));

  }

  public function delete()
  {

    $id = $this->uri->segment(4,0);

    try {
      $member = Member::find($id);
      if($this->is_post())
      {
        $member->delete();
        $this->setFlash('Member deleted','success');
        $this->redirectTo('/secure/members/');
      }

    } catch(ActiveRecord\RecordNotFound $e) {
      $this->setFlash('Problem deleting member - could not find the specified member','error');
      $this->redirectTo('/secure/members/');
    }

    $this->addTemplateData(array('member' => $member));
    $this->render('secure/members/delete.tpl');

  }

  public function toggle_approved()
  {

    $id = $this->uri->segment(4,0);
    if($id === 0 || $id === FALSE)
    {
      $this->setFlash('Could not find the specified member','warning');
      $this->redirectTo('secure/members');
    }
    else
    {
      try {
        $member = Member::find($id);
      } catch(ActiveRecord\RecordNotFound $e) {
        $this->setFlash('Problem editing member - could not find the specified member','error');
        $this->redirectTo('/secure/members/');
      }
    }

    $member->approved = !$member->approved;
    if($member->approved) $member->generateActivationToken();
    $member->approved_by = $this->session->userdata('current_user_name');
    $member->approved_at = date('Y-m-d G:i:s');
    $member->save();

    if($member->approved && !$member->activated)
    {
      // Send email to member....
      $email_data = array(
        'member' => $member,
        'site_name' => Setting::get('site_name'),
        'activation_url' => config_item('base_url').'/account/activate/'.$member->activation_url_token(),
        'email_content' => Email_template::findByShortName('member_invite')
      );
      $this->mailer->subject(Setting::get('site_name').' - Please activate your account')
                   ->to_name($member->name)
                   ->to_email($member->email)
                   ->from_name(Setting::get('member_invite_email_from_name'))
                   ->from_email(Setting::get('member_invite_email_from_email'))
                   ->template('emails/email_content.tpl')
                   ->template_data($email_data);
      $this->mailer->send();
    }

    $this->redirectTo('/secure/members/edit/'.$member->id);

  }

}
