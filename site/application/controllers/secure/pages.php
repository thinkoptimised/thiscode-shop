<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends Admin_Controller {

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $sections = Site_section::find('all',array('order' => 'members_only,position,name'));
    $miscellaneous_pages = Page::findBySection(0);
    $this->render('secure/pages/home.tpl',array(
      'sections' => $sections,
      'miscellaneous_pages' => $miscellaneous_pages
    ));
  }

  public function edit()
  {

    $id = $this->uri->segment(4,0);
    if($id === 0 || $id === FALSE)
    {
      $page = new Page;
    }
    else
    {
      try {
        $page = Page::find($id);
      } catch(ActiveRecord\RecordNotFound $e) {
        $this->setFlash('Problem editing page - could not find the specified page','error');
        $this->redirectTo('/secure/pages/');
      }
    }

    if($this->is_post())
    {

      $page->site_section_id = $this->input->post('site_section_id');
      $page->name = $this->input->post('name');
      $page->title = $this->input->post('title');
      $page->meta_title = $this->input->post('meta_title');
      $page->meta_description = $this->input->post('meta_description');
      $page->main_body = $this->input->post('main_body');
      $page->set_document_ids($this->input->post('documents'));
      $page->position = $this->input->post('position');

      if($page->save())
      {

        $this->setFlash('Page saved successfully','success');
        $this->redirectTo('/secure/pages');
      }
      else
      {
        $this->addTemplateData(array('error' => true));
      }

    }

    $this->render('secure/pages/edit.tpl',array(
      'page' => $page,
      'public_sections' => Site_section::allPublic(),
      'private_sections' => Site_section::allPrivate(),
      'documents' => Document::find('all',array('order' => 'name'))
    ));

  }

  public function delete()
  {

    $id = $this->uri->segment(4,0);
    try {
      $page = Page::find($id);
      if($this->is_post())
      {
        $page->delete();
        $this->setFlash('Page deleted','success');
        $this->redirectTo('/secure/pages/');
      }

    } catch(ActiveRecord\RecordNotFound $e) {
      $this->setFlash('Problem deleting page - could not find the specified page','error');
      $this->redirectTo('/secure/pages/');
    }

    $this->addTemplateData(array('page' => $page));
    $this->render('secure/pages/delete.tpl');

  }

  public function save_position()
  {
    if($this->is_post())
    {
      $i=1;
      foreach($_POST['pageid'] as $page)
      {
        $page = Page::find($page);
        $page->position = $i;
        $page->save();
        $i++;
      }
    }
  }

}
