<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Admin_Controller {

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->render('secure/settings/home.tpl',array(
      'settings' => Setting::find('all',array('order' => 'category,name'))
    ));
  }

  public function edit()
  {

    $id = $this->uri->segment(4,0);
    if($id === 0 || $id === FALSE) 
    {
      $this->setFlash('Cannot create a new setting','error');
      $this->redirectTo('/secure/settings');
    }
    else 
    {
      try {
        $setting = Setting::find($id);
      } catch(ActiveRecord\RecordNotFound $e) {
        $this->setFlash('Cannot find the specified setting','error');
        $this->redirectTo('/secure/settings');
      }
    }

    if($this->is_post())
    {

      $setting->value = $this->input->post('setting');

      if($setting->save()) 
      {

        $this->setFlash('Setting saved successfully','success');
        $this->redirectTo('/secure/settings');
      }
      else
      {
        $this->addTemplateData(array('error' => true));
      }

    }

    $this->render('secure/settings/edit.tpl',array(
      'setting' => $setting
    ));

  }

}
