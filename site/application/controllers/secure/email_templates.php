<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Email_templates extends Admin_Controller {

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->render('secure/email_templates/home.tpl',array(
      'email_templates' => Email_template::find('all')
    ));
  }

  public function edit()
  {

    $id = $this->uri->segment(4,0);
    if($id === 0 || $id === FALSE)
    {
      $this->setFlash('Cannot create a new email template','error');
      $this->redirectTo('/secure/email_templates');
    }
    else
    {
      try {
        $email_templates = Email_template::find($id);
      } catch(ActiveRecord\RecordNotFound $e) {
        $this->setFlash('Cannot find the specified email template','error');
        $this->redirectTo('/secure/email_templates');
      }
    }

    if($this->is_post())
    {

      $email_templates->title = $this->input->post('title');
      $email_templates->teaser = $this->input->post('teaser');
      $email_templates->heading = $this->input->post('heading');
      $email_templates->main_body = $this->input->post('main_body');

      if($email_templates->save())
      {

        $this->setFlash('Email template saved successfully','success');
        $this->redirectTo('/secure/email_templates');
      }
      else
      {
        $this->addTemplateData(array('error' => true));
      }

    }

    $this->render('secure/email_templates/edit.tpl',array(
      'email_templates' => $email_templates
    ));

  }

}
