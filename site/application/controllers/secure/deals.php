<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Deals extends Admin_Controller {

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $deal_sections = Deal_section::find('all',array('order' => 'position,name'));
    $deals = Deal::find('all',array('order' => 'position,name'));
    $this->render('secure/deals/home.tpl',array(
      'deal_sections' => $deal_sections,
      'deals' => $deals
    ));
  }

  public function edit()
  {

    $id = $this->uri->segment(4,0);
    if($id === 0 || $id === FALSE)
    {
      $deal = new Deal;
    }
    else
    {
      try {
        $deal = Deal::find($id);
      } catch(ActiveRecord\RecordNotFound $e) {
        $this->setFlash('Problem editing deal - could not find the specified deal','error');
        $this->redirectTo('/secure/deals/');
      }
    }

    if($this->is_post())
    {

      $deal->deal_section_id = $this->input->post('deal_section_id');
      $deal->deal_headline = $this->input->post('deal_headline');
      $deal->name = $this->input->post('name');
      $deal->price = $this->input->post('price');
      $deal->main_body = $this->input->post('main_body');

      if($deal->save())
      {
        $this->setFlash('Deal saved successfully','success');
        $this->redirectTo('/secure/deals');
      }
      else
      {
        $this->addTemplateData(array('error' => true));
      }

    }

    $this->render('secure/deals/edit.tpl',array(
      'deal' => $deal,
      'deal_sections' => Deal_section::find('all')
    ));

  }

  public function delete()
  {

    $id = $this->uri->segment(4,0);
    try {
      $deal = Deal::find($id);
      if($this->is_post())
      {
        $deal->delete();
        $this->setFlash('Deal deleted','success');
        $this->redirectTo('/secure/deals/');
      }

    } catch(ActiveRecord\RecordNotFound $e) {
      $this->setFlash('Problem deleting deal - could not find the specified deal','error');
      $this->redirectTo('/secure/deals/');
    }

    $this->addTemplateData(array('deal' => $deal));
    $this->render('secure/deals/delete.tpl');

  }

  public function save_position()
  {
    if($this->is_post())
    {
      $i=1;
      foreach($_POST['dealid'] as $deal)
      {
        $deal = Deal::find($deal);
        $deal->position = $i;
        $deal->save();
        $i++;
      }
    }
  }

}
