<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage_boxes extends Admin_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->library('upload');
    $upload_config['upload_path'] = './_img/uploads/homepage_boxes/';
    $upload_config['allowed_types'] = 'gif|jpg|png';
    //$upload_config['max_size'] = '100';
    $upload_config['max_width'] = '1024';
    $upload_config['max_height'] = '768';
    $upload_config['encrypt_name'] = TRUE;
    $this->upload->initialize($upload_config);
  }

  public function index()
  {
    $homepage_boxes = Homepage_box::findAllByPosition();
    $this->render('secure/homepage_boxes/home.tpl',array(
      'homepage_boxes' => $homepage_boxes,
    ));
  }

  public function edit()
  {

    $id = $this->uri->segment(4,0);
    if($id === 0 || $id === FALSE)
    {
      $homepage_box = new Homepage_box;
    }
    else
    {
      try {
        $homepage_box = Homepage_box::find($id);
      } catch(ActiveRecord\RecordNotFound $e) {
        $this->setFlash('Problem editing homepage box - could not find the specified homepage box','error');
        $this->redirectTo('/secure/homepage_boxes/');
      }
    }

    $current_imagefile = $homepage_box->imagefile;

    if($this->is_post())
    {
      $upload_success = false;
      $upload_attempted = false;
      if(isset($_FILES['imagefile']) && $_FILES['imagefile']['name'] != null) {
        $upload_attempted = true;
        $upload_success = $this->upload->do_upload('imagefile');
        $upload_info = $this->upload->data();
      }

      $homepage_box->title = $this->input->post('title');
      $homepage_box->main_body = $this->input->post('main_body');
      $homepage_box->imagefile = $this->input->post('imagefile');
      $homepage_box->link = $this->input->post('link');
      $homepage_box->link_text = $this->input->post('link_text');
      $homepage_box->image_alt = $this->input->post('image_alt');

      if($upload_success)
      {
        $homepage_box->imagefile = $upload_info['file_name'];
      }
      else
      {
        $homepage_box->imagefile = $current_imagefile;
      }

      $homepage_box->is_valid();
      if(!$upload_success && ($homepage_box->is_new_record()))
      {
        $this->addTemplateData(array('error' => true));
        $homepage_box->add_error('filename',': a valid file must be uploaded');
      }
      else
      {
        if($homepage_box->save())
        {
          $this->setFlash('Homepage box saved successfully','success');
          $this->redirectTo('/secure/homepage_boxes');
        }
        else
        {
          $this->addTemplateData(array('error' => true));
        }
      }
    }

    $this->render('secure/homepage_boxes/edit.tpl',array(
      'homepage_box' => $homepage_box
    ));

  }

  public function delete()
  {
    $this->load->helper('file');

    $id = $this->uri->segment(4,0);
    try {
      $homepage_box = Homepage_box::find($id);
      if($this->is_post())
      {
        $homepage_box->delete();
        $this->setFlash('Homepage box deleted','success');
        $this->redirectTo('/secure/homepage_boxes/');
      }

    } catch(ActiveRecord\RecordNotFound $e) {
      $this->setFlash('Problem deleting homepage box - could not find the specified homepage box','error');
      $this->redirectTo('/secure/homepage_boxes/');
    }

    $this->addTemplateData(array('homepage_box' => $homepage_box));
    $this->render('secure/homepage_boxes/delete.tpl');

  }

  public function save_position()
  {
    if($this->is_post())
    {
      $i=1;
      foreach($_POST['boxid'] as $homepage_box)
      {
        echo "box id ".$homepage_box." will be place at position ".$i."<br>";
        $homepage_box = Homepage_box::find($homepage_box);
        $homepage_box->position = $i;
        $homepage_box->save();
        $i++;
      }
    }
  }

  public function toggle_publish()
  {
    $id = $this->uri->segment(4);
    $toggle = $this->uri->segment(5);

    $box = Homepage_box::find($id);
    $box->published = $toggle;
    $box->save();

    $this->redirectTo('/secure/homepage_boxes/');
  }

}