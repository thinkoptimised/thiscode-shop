<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sections extends Admin_Controller {

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->render('secure/sections/home.tpl', array(
      'sections' => Site_section::find('all',array('order' => 'members_only,position,name'))
    ));
  }

  public function edit()
  {

    $id = $this->uri->segment(4,0);
    if($id === 0 || $id === FALSE)
    {
      $section = new Site_section;
    }
    else
    {
      try {
        $section = Site_section::find($id);
      } catch(ActiveRecord\RecordNotFound $e) {
        $this->setFlash('Problem editing section - could not find the specified section','error');
        $this->redirectTo('/secure/sections/');
      }
    }

    if($this->is_post())
    {
      $section->name = $this->input->post('name');
      $section->position = $this->input->post('position');
      $section->members_only = $this->input->post('members_only') === '1';
      if($section->save())
      {
        $this->setFlash('Section saved successfully','success');
        $this->redirectTo('/secure/sections');
      }
      else
      {
        $this->addTemplateData(array('error' => true));
      }
    }

    $this->render('secure/sections/edit.tpl',array('section' => $section));

  }

  public function delete()
  {

    $id = $this->uri->segment(4,0);

    try {
      $section = Site_section::find($id);

      if($section->page_count() > 0)
      {
        $this->setFlash('Cannot delete a section which has assigned pages. Please remove the pages first and then delete the section.','error');
        $this->redirectTo('/secure/sections');
      }

      if($this->is_post())
      {
        $section->delete();
        $this->setFlash('Section deleted','success');
        $this->redirectTo('/secure/sections/');
      }

    } catch(ActiveRecord\RecordNotFound $e) {
      $this->setFlash('Problem deleting section - could not find the specified section','error');
      $this->redirectTo('/secure/sections/');
    }

    $this->addTemplateData(array('section' => $section));
    $this->render('secure/sections/delete.tpl');

  }

  public function save_position()
  {
    if($this->is_post())
    {
      $i=1;
      foreach($_POST['section'] as $section)
      {
        $section = Site_section::find($section);
        $section->position = $i;
        $section->save();
        $i++;
      }
    }
  }

}
