<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Documents extends Admin_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->library('upload');
  }

  public function index()
  {
    $this->render('secure/documents/home.tpl', array(
      'documents' => Document::find('all',array('order' => 'name')) 
    ));
  }

  public function edit()
  {

    $id = $this->uri->segment(4,0);
    if($id === 0 || $id === FALSE) 
    {
      $was_new = true;
      $document = new Document;
    }
    else 
    {
      $was_new = false;
      try {
        $document = Document::find($id);
      } catch(ActiveRecord\RecordNotFound $e) {
        $this->setFlash('Problem editing document - could not find the specified document','error');
        $this->redirectTo('/secure/documents/');
      }
    }

    if($this->is_post())
    {

      $upload_success = false;
      $upload_attempted = false;
      if(isset($_FILES['file'])) {
        $upload_attempted = true;
        $upload_success = $this->upload->do_upload('file');
        $upload_info = $this->upload->data();
      }

      $document->name = $this->input->post('name');
      if($upload_success)
      {
        $document->filename = $upload_info['file_name'];
        $document->original_filename = $upload_info['orig_name'];
        $document->filetype = preg_replace('/\\./','',$upload_info['file_ext']);
      }

      $document->is_valid();
      if(!$upload_success && ($upload_attempted || $document->is_new_record()))
      {
        $this->addTemplateData(array('error' => true));
        $document->add_error('filename',': a valid file must be uploaded');
      } 
      else
      {
        if($document->save()) 
        {
          $this->setFlash('Document saved successfully','success');
          $this->redirectTo('/secure/documents');
        }
        else
        {
          $this->addTemplateData(array('error' => true));
        }
      }
    }

    $this->render('secure/documents/edit.tpl',array('document' => $document));

  }

  public function delete()
  {

    // TODO: Delete the file linked by this document...

    $id = $this->uri->segment(4,0);

    try {
      $document = Document::find($id);

      if($document->page_count() > 0)
      {
        $this->setFlash('Cannot delete a document which has assigned pages. Please remove the pages first and then delete the document.','error');
        $this->redirectTo('/secure/documents');
      }

      if($this->is_post())
      {
        $document->delete();
        $this->setFlash('Document deleted','success');
        $this->redirectTo('/secure/documents/');
      }

    } catch(ActiveRecord\RecordNotFound $e) {
      $this->setFlash('Problem deleting document - could not find the specified document','error');
      $this->redirectTo('/secure/documents/');
    }

    $this->addTemplateData(array('document' => $document));
    $this->render('secure/documents/delete.tpl');

  }

  public function view()
  {
    $this->load->helper('download');
    $id = $this->uri->segment(4,0);
    try {
      $document = Document::find($id);
      force_download($document->sanitized_filename(),$document->get_contents());
    } catch(ActiveRecord\RecordNotFound $e) {
      $this->setFlash('Problem viewing document - could not find the specified document','error');
      $this->redirectTo('/secure/documents/');
    }
  }

}
