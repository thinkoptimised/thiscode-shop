<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->ga_visits_json();
    $recently_published = News_item::findRecentlyPublished(5);
    $recently_created = News_item::findRecentlyCreated(5);
    $contact_form = Contact_form_submission::findIncomplete();

    $this->render('secure/home.tpl',array(
      'members_awaiting_approval' => Member::findAllAwaitingApproval(),
      'recently_published' => $recently_published,
      'recently_created' => $recently_created,
      'contact_form' => $contact_form
    ));
  }

  public function ga_visits_json()
  {
    $json = Ga_visits::get_visits();

    $this->render('secure/ga_visits_json.tpl',array(
      'json' => $json
    ));
  }
}
