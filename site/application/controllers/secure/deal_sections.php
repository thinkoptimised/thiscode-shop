<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Deal_sections extends Admin_Controller {

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->render('secure/deal_sections/home.tpl', array(
      'deal_sections' => Deal_section::find('all',array('order' => 'position,name'))
    ));
  }

  public function edit()
  {

    $id = $this->uri->segment(4,0);
    if($id === 0 || $id === FALSE)
    {
      $deal_section = new Deal_section;
    }
    else
    {
      try {
        $deal_section = Deal_section::find($id);
      } catch(ActiveRecord\RecordNotFound $e) {
        $this->setFlash('Problem editing section - could not find the specified section','error');
        $this->redirectTo('/secure/deal_section/');
      }
    }

    if($this->is_post())
    {
      $deal_section->name = $this->input->post('name');
      $deal_section->position = $this->input->post('position');
      if($deal_section->save())
      {
        $this->setFlash('Section saved successfully','success');
        $this->redirectTo('/secure/deal_sections');
      }
      else
      {
        $this->addTemplateData(array('error' => true));
      }
    }

    $this->render('secure/deal_sections/edit.tpl',array('deal_section' => $deal_section));

  }

  public function delete()
  {

    $id = $this->uri->segment(4,0);

    try {
      $deal_section = Deal_section::find($id);

      /*
      if($deal_section->page_count() > 0)
      {
        $this->setFlash('Cannot delete a deal section which has assigned deals. Please remove the deals first and then delete the deal section.','error');
        $this->redirectTo('/secure/deal_sections');
      }
      */

      if($this->is_post())
      {
        $deal_section->delete();
        $this->setFlash('Deal section deleted','success');
        $this->redirectTo('/secure/deal_sections/');
      }

    } catch(ActiveRecord\RecordNotFound $e) {
      $this->setFlash('Problem deleting deal section - could not find the specified deal section','error');
      $this->redirectTo('/secure/deal_sections/');
    }

    $this->addTemplateData(array('deal_section' => $deal_section));
    $this->render('secure/deal_sections/delete.tpl');

  }

  public function save_position()
  {
    if($this->is_post())
    {
      $i=1;
      foreach($_POST['deal_section_id'] as $deal_section)
      {
        $deal_section = Deal_section::find($deal_section);
        $deal_section->position = $i;
        $deal_section->save();
        $i++;
      }
    }
  }

}
