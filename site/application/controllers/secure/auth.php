<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends Admin_Controller {

	function __construct()
	{
    $this->_requires_login = false;
		parent::__construct();
		$this->load->model('admin_user');
	}

	public function index()
	{

    if($this->is_post())
    {
      $admin_user = Admin_user::authenticate($this->input->post('email'),$this->input->post('password'));
      if($admin_user === FALSE)
      {
        $this->setFlash('Email / password not found. Please try again.','error');
        $this->redirectTo('/secure/auth');
      }

      $this->loginUser($admin_user);
      $this->setFlash('Welcome back '.$admin_user->name,'success');
      $this->redirectTo('/secure/dashboard');
    }

    $this->render('secure/login.tpl');
	}

	public function logout()
	{
    $this->logoutUser();
    $this->setFlash('You have been logged out','notice');
		$this->redirectTo('/secure/auth');
	}


  public function activate()
  {
    $token = $this->uri->segment(4,0);
    $user = Admin_user::FindByActivationToken($token);
    if($user === false)
    {
      $this->render('secure/auth/activate_could_not_find_user.tpl');
    }
    else
    {
      if($this->is_post())
      {
        $user->email = $this->input->post('email');
        $user->name = $this->input->post('name');
        $user->set_plain_text_password($this->input->post('password'));
        $user->set_plain_text_password_confirmation($this->input->post('password_confirmation'));
        if($user->is_valid())
        {
          $user->activate();
          $user->save();
          $this->loginUser($user);
          $this->setFlash('Your account has been activated successfully and you have been logged in','success');
          $this->redirectTo('/secure');
        }
        else
        {
          $this->addTemplateData(array('error' => true));
        }
      }
      $this->render('secure/auth/activate.tpl',array('user' => $user));
    }
  }

  public function forgotten_password()
  {
    if($this->is_post())
    {
      $user = Admin_user::findByEmail($this->input->post('email'));
      if($user !== false)
      {
        $user->initiate_password_reset();
        $email_data = array(
          'user' => $user,
          'reset_url' => config_item('admin_url').'auth/reset_password/'.$user->password_reset_url_token(),
          'admin_url' => config_item('admin_url'),
          'email_content' => Email_template::findByShortName('admin_user_reset')
        );
        $this->mailer->subject(Setting::get('site_name').' - Reset your password')
                     ->to_name($user->name)
                     ->to_email($user->email)
                     ->from_name(Setting::get('admin_user_invite_email_from_name'))
                     ->from_email(Setting::get('admin_user_invite_email_from_email'))
                     ->template('emails/email_content.tpl')
                     ->template_data($email_data);
        if($this->mailer->send())
        {
          $this->setFlash('An email has been sent containing instructions on how to reset your password.','success');
          $this->redirectTo('/secure/auth');
        }
        else
        {
          $this->addTemplateData(array('error' => true));
        }
      }
      else
      {
        $this->addTemplateData(array('error' => true));
      }
    }
    $this->render('secure/auth/forgotten_password.tpl');
  }

  public function reset_password()
  {

    $token = $this->uri->segment(4,0);
    $user = Admin_user::FindByResetToken($token);
    if($user === false)
    {
      $this->render('secure/auth/reset_could_not_find_user.tpl');
    }
    else
    {
      if($this->is_post())
      {
        $user->set_plain_text_password($this->input->post('password'));
        $user->set_plain_text_password_confirmation($this->input->post('password_confirmation'));
        if($user->is_valid())
        {
          $user->complete_password_reset();
          $user->save();
          $this->loginUser($user);
          $this->setFlash('Your password has been reset successfully, and you have been logged in.','success');
          $this->redirectTo('/secure');
        }
        else
        {
          $this->addTemplateData(array('error' => true));
        }
      }
      $this->render('secure/auth/reset.tpl',array('user' => $user));
    }

  }

  public function tinymce()
  {
    $secretKey = 'NFTs47zg3wWZ';
    if(!$this->isLoggedIn()) die('Login error');
    $data['return_url'] = $this->input->get('return_url');
    $data['key'] = md5($secretKey);
    $this->render('secure/auth/tinymce.tpl',$data);
  }

}
