<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Admin_Controller {

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->render('secure/users/home.tpl', array(
      'users' => Admin_user::find('all')
    ));
  }

  public function edit()
  {

    $id = $this->uri->segment(4,0);
    if($id === 0 || $id === FALSE)
    {
      $user = new Admin_user;
      $was_new = true;
    }
    else
    {
      try {
        $user = Admin_user::find($id);
        $was_new = false;
      } catch(ActiveRecord\RecordNotFound $e) {
        $this->setFlash('Problem editing user - could not find the specified user','error');
        $this->redirectTo('/secure/users/');
      }
    }

    if($this->is_post())
    {

      $user->name = $this->input->post('name');
      $user->email = $this->input->post('email');

      if($user->save())
      {

        // Send activation email
        if($was_new) {

          $email_data = array(
            'user' => $user,
            'activation_url' => config_item('admin_url').'auth/activate/'.$user->activation_url_token(),
            'site_name' => Setting::get('site_name'),
            'admin_url' => config_item('admin_url'),
            'email_content' => Email_template::findByShortName('admin_user_activation')
          );
          $this->mailer->subject('Activate your '.Setting::get('site_name').' account')
                       ->to_name($user->name)
                       ->to_email($user->email)
                       ->from_name(Setting::get('admin_user_invite_email_from_name'))
                       ->from_email(Setting::get('admin_user_invite_email_from_email'))
                       ->template('emails/email_content.tpl')
                       ->template_data($email_data);
          if($this->mailer->send())
          {
            $this->setFlash('User saved successfully and activation email sent','success');
          }
          else
          {
            $this->setFlash('User saved, but unable to send activation email.','error');
          }
        }
        else
        {
          $this->setFlash('User saved successfully','success');
        }

        $this->redirectTo('/secure/users/edit/'.$user->id);
      }
      else
      {
        $this->addTemplateData(array(
          'error' => true
        ));
      }

    }

    $this->render('secure/users/edit.tpl',array(
      'user' => $user
    ));

  }

  public function resend_activation()
  {

    $id = $this->uri->segment(4,0);
    try {
      $user = Admin_user::find($id);
      $email_data = array(
        'user' => $user,
        'activation_url' => config_item('admin_url').'auth/activate/'.$user->activation_url_token(),
        'admin_url' => config_item('admin_url'),
        'email_content' => Email_template::findByShortName('admin_user_activation')
      );
      $this->mailer->subject('Activate your '.Setting::get('site_name').' account')
                   ->to_name($user->name)
                   ->to_email($user->email)
                   ->from_name(Setting::get('admin_user_invite_email_from_name'))
                   ->from_email(Setting::get('admin_user_invite_email_from_email'))
                   ->template('emails/email_content.tpl')
                   ->template_data($email_data);
      if($this->mailer->send())
      {
        $this->setFlash('Activation email sent successfully','success');
      }
      else
      {
        $this->setFlash('Unable to send activation email','error');
      }

    } catch(ActiveRecord\RecordNotFound $ex) {
      $this->setFlash('Could not find user to resend activation email','error');
    }
    $this->redirectTo('/secure/users');
  }

  public function delete()
  {

    $id = $this->uri->segment(4,0);

    try {
      $user = Admin_user::find($id);
      if(Admin_user::is_super_user($id))
      {
        $this->setFlash('You do not have permission to delete this user','error');
        $this->redirectTo('/secure/users/');
      }
      else
      {
        if($this->is_post())
        {
          $user->delete();
          $this->setFlash('Admin user deleted','success');
          $this->redirectTo('/secure/users/');
        }
      }

    } catch(ActiveRecord\RecordNotFound $e) {
      $this->setFlash('Problem deleting user - could not find the specified user','error');
      $this->redirectTo('/secure/users/');
    }

    $this->addTemplateData(array('user' => $user));
    $this->render('secure/users/delete.tpl');

  }

  public function profile()
  {
    $user = $this->currentUser();
    if($this->is_post())
    {

      $user->name = $this->input->post('name');
      $user->email = $this->input->post('email');
      if(strlen($this->input->post('password')) > 0 || strlen($this->input->post('password_confirmation')) > 0)
      {
        $user->set_plain_text_password($this->input->post('password'));
        $user->set_plain_text_password_confirmation($this->input->post('password_confirmation'));
      }

      if($user->save())
      {
        $this->setFlash('Profile Updated','success');
        $this->redirectTo('secure/users/profile');
      }
      else
      {
        $this->addTemplateData(array('error' => true));
      }
    }
    $this->render('secure/users/profile.tpl',array(
      'user' => $user
    ));
  }


}
