<?php defined('BASEPATH') OR exit('No direct script access allowed');

class News_items extends Admin_Controller {

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $recently_published = News_item::findRecentlyPublished(5);
    $recently_created = News_item::findRecentlyCreated(5);
    $news_items = News_item::findAll();
    $this->render('secure/news_items/home.tpl',array(
      'recently_published' => $recently_published,
      'recently_created' => $recently_created,
      'news_items' => $news_items
    ));
  }

  public function edit()
  {

    $id = $this->uri->segment(4,0);
    if($id === 0 || $id === FALSE)
    {
      $news_item = new News_item;
    }
    else
    {
      try {
        $news_item = News_item::find($id);
      } catch(ActiveRecord\RecordNotFound $e) {
        $this->setFlash('Problem editing article - could not find the specified article','error');
        $this->redirectTo('/secure/news_items/');
      }
    }

    if($this->is_post())
    {

      $news_item->title = $this->input->post('title');
      $news_item->main_body = $this->input->post('main_body');
      $news_item->excerpt = $this->input->post('excerpt');
      $news_item->meta_title = $this->input->post('meta_title');
      $news_item->meta_description = $this->input->post('meta_description');
      $news_item->author = $this->session->userdata('current_user_name');

      if($news_item->save())
      {

        $this->setFlash('Article saved successfully','success');
        $this->redirectTo('/secure/news_items');
      }
      else
      {
        $this->addTemplateData(array('error' => true));
      }

    }

    $this->render('secure/news_items/edit.tpl',array(
      'news_item' => $news_item
    ));

  }

  public function delete()
  {

    $id = $this->uri->segment(4,0);
    try {
      $news_item = News_item::find($id);
      if($this->is_post())
      {
        $news_item->delete();
        $this->setFlash('Article deleted','success');
        $this->redirectTo('/secure/news_items/');
      }

    } catch(ActiveRecord\RecordNotFound $e) {
      $this->setFlash('Problem deleting article - could not find the specified article','error');
      $this->redirectTo('/secure/news_items/');
    }

    $this->addTemplateData(array('news_item' => $news_item));
    $this->render('secure/news_items/delete.tpl');
  }

  public function toggle_publish()
  {
    $id = $this->uri->segment(4);
    $toggle = $this->uri->segment(5);

    $news_item = News_item::find($id);
    if($toggle != '0')
    {
      $news_item->published_at = date('Y-m-d G:i:s');
    }

    $news_item->published_by = $this->session->userdata('current_user_name');
    $news_item->is_published = $toggle;
    $news_item->save();

    $this->redirectTo('/secure/news_items/');
  }

}