<?php defined('BASEPATH') OR exit('No direct script access allowed');

class contact_form_submissions extends Admin_Controller {

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $incomplete = Contact_form_submission::findIncomplete();
    $contact_form_submissions = Contact_form_submission::findAll();
    $this->render('secure/contact_form_submissions/home.tpl',array(
      'incomplete' => $incomplete,
      'contact_form_submissions' => $contact_form_submissions
    ));
  }

  public function view()
  {

    $id = $this->uri->segment(4,0);
    if($id === 0 || $id === FALSE)
    {
      $this->setFlash('Problem editing article - could not find the specified article','error');
      $this->redirectTo('/secure/contact_form_submissions/');
    }
    else
    {
      try {
        $contact_form_submission = Contact_form_submission::find($id);
      } catch(ActiveRecord\RecordNotFound $e) {
        $this->setFlash('Problem editing article - could not find the specified article','error');
        $this->redirectTo('/secure/contact_form_submissions/');
      }
    }

    $this->render('secure/contact_form_submissions/view.tpl',array(
      'contact_form_submission' => $contact_form_submission
    ));

  }

  public function delete()
  {

    $id = $this->uri->segment(4,0);
    try {
      $contact_form_submission = Contact_form_submission::find($id);
      if($this->is_post())
      {
        $contact_form_submission->delete();
        $this->setFlash('Article deleted','success');
        $this->redirectTo('/secure/contact_form_submissions/');
      }

    } catch(ActiveRecord\RecordNotFound $e) {
      $this->setFlash('Problem deleting article - could not find the specified article','error');
      $this->redirectTo('/secure/contact_form_submissions/');
    }

    $this->addTemplateData(array('news_item' => $contact_form_submission));
    $this->render('secure/contact_form_submissions/delete.tpl');
  }

  public function toggle_complete()
  {
    $id = $this->uri->segment(4);
    $toggle = $this->uri->segment(5);

    $contact_form_submission = Contact_form_submission::find($id);
    if($toggle != '0')
    {
      $contact_form_submission->actioned_at = date('Y-m-d G:i:s');
    }

    $contact_form_submission->actioned_by = $this->session->userdata('current_user_name');
    $contact_form_submission->is_actioned = $toggle;
    $contact_form_submission->save();

    $this->redirectTo('/secure/contact_form_submissions/');
  }

}