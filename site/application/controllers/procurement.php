<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Procurement extends Public_Controller {

  public function index()
  {
    $deal_sections = Deal_section::find('all',array('order' => 'position,name'));

    $this->addTemplateData(array(
      'deal_sections' => $deal_sections,
    ));

    if($this->uri->segment(2) == null)
    {
      $page = Page::findBySlug('procurement',11);
      $deals = Deal::find('all',array('order' => 'position,name'));

      $this->addTemplateData(array(
        'page' => $page,
        'deals' => $deals
      ));

      $template = 'all_deals.tpl';
    }
    else
    {
      $slug = $this->uri->segment(2);

      $deal_sections = Deal_section::findBySlug($slug);

      $this->addTemplateData(array(
        'deals' => Deal::findBySection($deal_sections->id),
        'current_section' => $deal_sections
      ));

      $template = 'deal_section.tpl';
    }

    $this->render('front/'.$template);
  }

}