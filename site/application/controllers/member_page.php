<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_Page extends Member_Controller {

  public function index()
  {

    $section_slug = $this->uri->segment(2,0);
    $page_slug = $this->uri->segment(3,0);

    if($section_slug === 0 && $page_slug === 0)
    {
      // Homepage
      $page = Page::findBySlug('members',0);
      $section = false;
      $template = 'page.tpl';
    }
    else
    {
      // Could be a section index, section page, or 404

      if($section_slug === 0)
      {
        $section = Site_section::first();
      } 
      else
      {
        $section = Site_section::findBySlug($section_slug);
      }

      if($section == false)
      {
        $page_slug = $section_slug;
        $section_id = 0;
      } 
      else
      {
        $section_id = $section->id;
      }

      if($page_slug === 0)
      {
        $page = $section->first_page();
      }
      else
      {
        $page = Page::findBySlug($page_slug,$section_id);
      }

      if($page == false)
      {
        header('HTTP/1.1 404 Not Found');
        $page = Page::findBySlug('page-not-found',0);
      }

      $template = 'page.tpl';

    }

    $this->render('front/'.$template,array(
      'section' => $section,
      'page' => $page,
      'documents' => $page->documents
    ));

  }


}
