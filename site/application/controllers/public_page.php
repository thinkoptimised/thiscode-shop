<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Public_Page extends Public_Controller {

  public function index()
  {
    $section_slug = $this->uri->segment(1,0);
    $page_slug = $this->uri->segment(2,0);

    if($section_slug === 0 && $page_slug === 0)
    {
      // Homepage
      $page = Page::findBySlug('homepage',0);
      $section = false;
      $this->addTemplateData(array('homepage_boxes' => Homepage_box::findAllForFrontPage()));
      $template = 'homepage.tpl';
    }
    else
    {
      // Could be a section index, section page, or 404

      if($section_slug === 0)
      {
        $section = Site_section::first();
      }
      else
      {
        $section = Site_section::findBySlug($section_slug);
      }

      if($section == false)
      {
        $page_slug = $section_slug;
        $section_id = 0;
      }
      else
      {
        $section_id = $section->id;
      }

      if($page_slug === 0)
      {
        $page = $section->first_page();
      }
      else
      {
        $page = Page::findBySlug($page_slug,$section_id);
      }

      if($page == false)
      {
        header('HTTP/1.1 404 Not Found');
        $page = Page::findBySlug('page-not-found',0);
      }

      $template = 'page.tpl';

    }

    if($page->contact_form)
    {
      $this->process_contact_form();
    }

    $this->render('front/'.$template,array(
      'section' => $section,
      'page' => $page,
      'documents' => $page->documents
    ));

  }


  protected function process_contact_form()
  {
    $submission = new Contact_form_submission;
    if($this->is_post())
    {

      $submission->name = $this->input->post('name');
      $submission->email = $this->input->post('email');
      $submission->company = $this->input->post('company');
      $submission->telephone = $this->input->post('telephone');
      $submission->message = $this->input->post('message');
      if($submission->save())
      {
        $this->addTemplateData(array('contact_success' => true));

        $email_data = array(
          'submission' => $submission,
          'site_name' => Setting::get('site_name'),
          'email_content' => Email_template::findByShortName('contact_form')
        );
        $this->mailer->subject(Setting::get('site_name').' - Contact Form Submission')
                     ->to_name(Setting::get('contact_form_name'))
                     ->to_email(Setting::get('contact_form_email'))
                     ->from_name(Setting::get('admin_user_invite_email_from_name'))
                     ->from_email(Setting::get('admin_user_invite_email_from_email'))
                     ->template('emails/email_content.tpl')
                     ->template_data($email_data);
        $this->mailer->send();

      }
      else
      {
        $this->addTemplateData(array('contact_error' => true));
      }

    }

    $this->addTemplateData(array('submission' => $submission));
  }


}
