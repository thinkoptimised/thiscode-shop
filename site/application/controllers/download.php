<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Download extends Public_Controller {

  public function index()
  {
    $this->load->helper('download');

    $page_id = $this->uri->segment(2);
    $document_id = $this->uri->segment(3);
    $document = false;

    try
    {
      $page = Page::find($page_id);
      foreach($page->documents as $d)
      {
        if($d->id == (int)$document_id)
        {
          $document = $d;
        }
      }

      if($page->members_only() && !$this->isLoggedIn())
      {
        $this->setFlash('You must login to access this download','error');
        $this->redirectTo('/account/login');
      }

      if($document !== false)
      {
        force_download($document->sanitized_filename(),$document->get_contents());
        die();
      }

    } catch(ActiveRecord\RecordNotFound $ex) {

    }

    // Could not find document, or hack attempt where document does not 
    // belong to specified page
    $this->redirectTo('/');

  }

}

