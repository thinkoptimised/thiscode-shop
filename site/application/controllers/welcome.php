<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends Public_Controller {

	public function index()
	{
		$slug = 'homepage';

		if($this->uri->segment(3) != '')
		{
			$slug = $this->uri->segment(3);
		}

		$page = Page::find_by_slug($slug);

		$data = array(
			'timestamp_css' => get_css_timestamp(),
			'timestamp_js' => get_js_timestamp(),
			'base_url' => base_url(),
			'settings' => '',
			'homepage_boxes' => Homepage_box::find('all'),
			'page' => $page
		);


	}

	public function about()
	{
		$slug = 'about';

		if($this->uri->segment(3) != '')
		{
			$slug = $this->uri->segment(3);
		}

		$page = Page::find_by_slug($slug);

		$data = array(
			'timestamp_css' => get_css_timestamp(),
			'timestamp_js' => get_js_timestamp(),
			'base_url' => base_url(),
			'settings' => '',
			'page' => $page
		);

		$this->twig->display('front/sections/content.tpl', $data);
	}

	public function forms()
	{
		$slug = 'forms';

		if($this->uri->segment(3) != '')
		{
			$slug = $this->uri->segment(3);
		}

		$page = Page::find_by_slug($slug);

		$data = array(
			'timestamp_css' => get_css_timestamp(),
			'timestamp_js' => get_js_timestamp(),
			'base_url' => base_url(),
			'settings' => '',
			'page' => $page
		);

		$this->twig->display('front/sections/forms.tpl', $data);
	}

	public function membership()
	{
		$slug = 'forms';

		if($this->uri->segment(3) != '')
		{
			$slug = $this->uri->segment(3);
		}

		$page = Page::find_by_slug($slug);

		$data = array(
			'timestamp_css' => get_css_timestamp(),
			'timestamp_js' => get_js_timestamp(),
			'base_url' => base_url(),
			'settings' => '',
			'page' => $page
		);

		$this->twig->display('front/sections/secure_content.tpl', $data);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
