<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <title>{% block title %}{% endblock %}</title>

    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="width: 100% !important; -webkit-text-size-adjust: none; margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0; background-color: #FAFAFA;" bgcolor="#FAFAFA">
        <center>
            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable" style="height: 100% !important; width: 100% !important; margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0; background-color: #FAFAFA;" bgcolor="#FAFAFA">
                <tr>
                    <td align="center" valign="top" style="border-collapse: collapse;">
                        <!-- // Begin Template Preheader \\ -->
                        <table border="0" cellpadding="10" cellspacing="0" width="600" id="templatePreheader" style="background-color: #FAFAFA;" bgcolor="#FAFAFA">
                            <tr>
                                <td valign="top" class="preheaderContent" style="border-collapse: collapse;">

                                    <!-- // Begin Module: Standard Preheader \ -->
                                    <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                        <tr>
                                            <td valign="top" style="border-collapse: collapse;">
                                                <div style="color: #505050; font-family: Arial; font-size: 10px; line-height: 100%; text-align: left;" align="left">
                                                     {% block teaser %}{% endblock %}
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Module: Standard Preheader \ -->

                                </td>
                            </tr>
                        </table>
                        <!-- // End Template Preheader \\ -->
                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer" style="border-top-color: #dddddd; border-right-color: #dddddd; border-bottom-color: #dddddd; border-left-color: #dddddd; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; background-color: #FFFFFF;" bgcolor="#FFFFFF">
                            <tr>
                                <td align="center" valign="top" style="border-collapse: collapse;">
                                    <!-- // Begin Template Header \\ -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader" style="background-color: #FFFFFF; border-bottom-width: 0;" bgcolor="#FFFFFF">
                                        <tr>
                                            <td class="headerContent" style="border-collapse: collapse; color: #202020; font-family: Arial; font-size: 34px; font-weight: bold; line-height: 100%; text-align: center; vertical-align: middle; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0;" align="center" valign="middle">

                                                <!-- // Begin Module: Standard Header Image \\ -->
                                                <img src="http://local.dev/_img/emails/logo_on_white.jpg" style="max-width: 600px; height: auto; line-height: 100%; outline: none; text-decoration: none; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0;" id="headerImage campaign-icon" />
                                                <!-- // End Module: Standard Header Image \\ -->

                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Header \\ -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="border-collapse: collapse;">
                                    <!-- // Begin Template Body \\ -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
                                        <tr>
                                            <td valign="top" class="bodyContent" style="border-collapse: collapse; background-color: #FFFFFF;" bgcolor="#FFFFFF">

                                                <!-- // Begin Module: Standard Content \\ -->
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top" style="border-collapse: collapse;">
                                                            <div style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;" align="left">
                                                                {% block content %}{% endblock %}
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- // End Module: Standard Content \\ -->

                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Body \\ -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="border-collapse: collapse;">
                                    <!-- // Begin Template Footer \\ -->
                                    <table border="0" cellpadding="10" cellspacing="0" width="600" id="templateFooter" style="background-color: #FFFFFF; border-top-width: 0;" bgcolor="#FFFFFF">
                                        <tr>
                                            <td valign="top" class="footerContent" style="border-collapse: collapse;">

                                                <!-- // Begin Module: Standard Footer \\ -->
                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top" style="border-collapse: collapse;">
                                                            <div mc:edit="std_footer" style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: left;" align="left">
                                                                <em>Copyright &copy; {{"now"|date("m/d/Y")}} {{ site_name }}, All rights reserved.</em>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- // End Module: Standard Footer \\ -->

                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Footer \\ -->
                                </td>
                            </tr>
                        </table>
                        <br />
                    </td>
                </tr>
            </table>
        </center>

<style type="text/css">
body { width: 100% !important; }
.ReadMsgBody { width: 100% !important; }
.ExternalClass { width: 100% !important; }
body { -webkit-text-size-adjust: none !important; }
body { margin: 0 !important; padding: 0 !important; }
img { border: 0 !important; height: auto !important; line-height: 100% !important; outline: none !important; text-decoration: none !important; }
#backgroundTable { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }
body { background-color: #FAFAFA !important; }
#backgroundTable { background-color: #FAFAFA !important; }
.preheaderContent div a:visited { color: #336699 !important; font-weight: normal !important; text-decoration: underline !important; }
.headerContent a:visited { color: #336699 !important; font-weight: normal !important; text-decoration: underline !important; }
.bodyContent div a:visited { color: #336699 !important; font-weight: normal !important; text-decoration: underline !important; }
.footerContent div a:visited { color: #336699 !important; font-weight: normal !important; text-decoration: underline !important; }
</style>
</body>
</html>