{% extends "emails/templates/base.tpl" %}

{% block title %}
    {{ email_content.title|evaluate }}
{% endblock %}

{% block teaser %}
    {{ email_content.teaser|evaluate }}
{% endblock %}

{% block content %}
    <h1 class="h1">{% autoescape false %}{{ email_content.heading|evaluate }}{% endautoescape %}</h1>
    {% autoescape false %}
    {{ email_content.main_body|evaluate }}
    {% endautoescape %}
{% endblock %}