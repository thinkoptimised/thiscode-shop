{% extends "front/templates/base_content.tpl" %}

{% block title %}
  {% if not contact_error and not contact_success  %}
    <h1>{{ page.title }}</h1>
  {% endif %}
{% endblock %}

{% block content %}

  {% if not contact_error and not contact_success  %}
    {% autoescape false %}
      {{ page.main_body }}
    {% endautoescape %}
  {% endif %}

  {% if page.contact_form %}
    {% include 'front/contact_form.tpl' %}
  {% endif %}
  
{% endblock %}

{% block sidebar %}

{% if section and section.page_count > 1 %}
  <h2>{{ section.name }}</h2>

  <ul>
    {% for p in section.myPages %}
    <li {% if page.id == p.id %}class="current"{% endif %}><a href="{{ p.url }}" title="{{ p.title }}">&raquo; {{ p.name }}</a></li>
    {% endfor %}
  </ul>
{% endif %}

{% endblock %}


  {% block documents %}
    {% if documents %}
      <div class="documents">
        <h3>Related documents</h3>
        <ul>
       {% for doc in documents %}
       <li><a href="{{ doc.public_url(page.id) }}"><img src="{{ page_settings.base_url }}_img/filetypes/{{ doc.filetype }}.png" width="20" height="20" alt ="download {{ doc.name }}" />{{doc.name}}</a> <small>Upload on {{ doc.updated_at|date("m/d/Y") }}</small></li>
       {% endfor %}
       </ul>
     </div>
    {% endif %}
  {% endblock %}
