
  {% if contact_success %}
    <div class="success">
      <p>Thank you, your message has been sent.</p>
    </div>
  {% else %}
    <h2>Get in touch</h2>
    {% if contact_error %}
      <div class="error">
        <p>There was a problem with your submission, please check the following and try again.</p>
        <ul>
          {% for error in submission.errors %}
            <li>{{ error }}</li>
          {% endfor %}
        </ul>
      </div>
    {% endif %}
    
    {{ form_open('',{'class': 'form-horizontal'}) }}
      <fieldset>
        <div>
          <label for="name">Name *</label>
          <input type="text" id="name" name="name" value="{{ submission.name }}">
        </div>
        <div>
          <label for="email">Email *</label>
          <input type="text" id="email" name="email" value="{{ submission.email }}">
        </div>
        <div>
          <label for="company">Company</label>
          <input type="text" id="company" name="company" value="{{ submission.company }}">
        </div>
        <div>
          <label for="telephone">Telephone</label>
          <input type="text" id="telephone" name="telephone" value="{{ submission.telephone }}">
        </div>
        <div>
          <label for="message">Message *</label>
          <textarea name="message" id="message">{{ submission.message }}</textarea>
        </div>
        <div class="actions">
          <input type="submit" class="" value="Send Message" />
        </div>
      </fieldset>
    </form>
  {% endif %}
