{% extends "front/templates/base_content.tpl" %}

{% block title %}
    <h1>{{ page.title }}</h1>
{% endblock %}

{% block content %}

  {% autoescape false %}
    {{ page.main_body }}
  {% endautoescape %}

  {% for section in deal_sections %}
  {% for deal in section.myDeals %}
    <section>
      <header><h2>{{ section.name }}</h2></header>
        <h3>{{ deal.deal_headline }}</h3>
        {% autoescape false %}
          {{ deal.main_body }}
        {% endautoescape %}
        <span>{{ deal.price }}</span>
        <a href="{{ section.url }}" title="{{ deal.deal_headline }}">Find out more &raquo;</a>
    </section>
  {% endfor %}
  {% endfor %}

{% endblock %}

{% block sidebar %}

{% if deal_sections %}
  <ul>
    {% for deal_section in deal_sections %}
    {% if deal_section.deal_count > 0 %}
    <li><a href="{{ deal_section.url }}" title="{{ deal_section.name }}">&raquo; {{ deal_section.name }}</a></li>
    {% endif %}
    {% endfor %}
  </ul>
{% endif %}

{% endblock %}