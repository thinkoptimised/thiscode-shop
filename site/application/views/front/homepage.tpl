{% extends "front/templates/base_homepage.tpl" %}
{% block content %}

  <figure>
    <img src="{{ page_settings.base_url }}_img/homepage/man.jpg" alt="" />
    <figcaption>
      <h1>{{ page.title }}</h1>
      {% autoescape false %}
        {{ page.main_body }}
      {% endautoescape %}
    </figcaption>
  </figure>

  <div class="features">
    {% for box in homepage_boxes %}
    <section>
      <img src="{{page_Setting.baseurl}}_img/uploads/homepage_boxes/{{box.imagefile}}" width="64" height="64" alt="{{box.image_alt}}" /><h4>{{box.title}}</h4>
      {% autoescape false %}
        {{box.main_body}}
      {% endautoescape %}
      <a href="{{box.link}}" title="">{{box.link_text}} &raquo;</a>
    </section>
    {% endfor %}
  </div>

  <article>
    <h2>What we do</h2>
    <p>"They're coming!" a woman shrieked, and incontinently everyone was turning and pushing at those behind, in order to clear their way to Woking again.  They must have bolted as blindly as a flock of sheep. Where the road grows narrow and black between the high banks the crowd jammed, and a desperate struggle occurred.  All that crowd did not escape; three persons at least, two women and a little boy, were crushed and trampled there, and left to die amid the terror and the darkness.</p>
  </article>

  <article>
    <h2>How we help</h2>
    <p>And then he perceived that, very slowly, the circular top of the cylinder was rotating on its body.  It was such a gradual movement that he discovered it only through noticing that a black mark that had been near him five minutes ago was now at the other side of the circumference.  Even then he scarcely understood what this indicated, until he heard a muffled grating sound and saw the black mark jerk forward an inch or so.  Then the thing came upon him in a flash.  The cylinder was artificial--hollow--with an end that screwed out! Something within the cylinder was unscrewing the top!</p>
  </article>
{% endblock %}
