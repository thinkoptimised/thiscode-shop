{% extends "front/templates/base_content.tpl" %}

{% block title %}
  <h1>{{ page.title }}</h1>
{% endblock %}

{% block content %}

  {% if flash_success %}
    <div class="success">
      <p>{{ flash_success }}</p>
    </div>
  {% endif %}

  {% if flash_error %}
    <div class="error">
      <p>{{ flash_error }}</p>
    </div>
  {% else %}
    {% autoescape false %}
      {{ page.main_body }}
    {% endautoescape %}
  {% endif %}

  {{ form_open('account/login',{'class': 'form-horizontal'}) }}
    <fieldset>
      <div>
        <label for="email">Email</label>
        <input type="text" id="email" name="email" value="">
      </div>
      <div>
        <label for="password">Password</label>
        <input type="password" id="password" name="password" value="">
      </div>
      <div class="actions">
        <input type="submit" class="" value="Login" /> |
        <a href="/account/forgotten_password">Forgotten your password</a>
      </div>
    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
{% endblock %}
