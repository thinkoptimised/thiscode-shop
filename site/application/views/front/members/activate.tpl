{% extends "front/templates/base_content.tpl" %}

{% block title %}
  <h1>{{ page.title }}</h1>
{% endblock %}

{% block content %}

  {% if error %}
    <div class="error">
      <p>There was a problem with your account activation, please check the following and try again.</p>
      <ul>
        {% for error in member.errors %}
          <li>{{ error }}</li>
        {% endfor %}
      </ul>
    </div>
  {% else %}
    {% autoescape false %}
      {{ page.main_body }}
    {% endautoescape %}
  {% endif %}

  {{ form_open('account/activate/' ~ member.activation_url_token,{'class': 'form-horizontal'}) }}
    <fieldset>
      <div>
        <label for="name">Name</label>
        <input type="text" id="name" name="name" value="{{ member.name }}">
      </div>
      <div>
        <label for="email">Email</label>
        <input type="text" id="email" name="email" value="{{ member.email }}">
      </div>
      <div>
        <label for="company">Company Name</label>
        <input type="text" id="company" name="company" value="{{ member.company }}">
      </div>
      <div>
        <label for="password">Password</label>
        <input type="password" id="password" name="password" value="">
      </div>
      <div>
        <label for="password_confirmation">Confirm Password</label>
        <input type="password" id="password_confirmation" name="password_confirmation" value="">
      </div>
      <div class="actions">
        <input type="submit" class="" value="Activate your account" />
      </div>
    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
{% endblock %}
