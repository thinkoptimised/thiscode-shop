{% extends "front/templates/base_content.tpl" %}

{% block title %}
  <h1>{{ page.title }}</h1>
{% endblock %}

{% block content %}

  {% if error %}
    <div class="error">
      <p>There was a problem resetting your password. Please check the following and try again</p>
      <ul>
        {% for error in member.errors %}
          <li>{{ error }}</li>
        {% endfor %}
      </ul>
    </div>
  {% else %}
    {% autoescape false %}
      {{ page.main_body }}
    {% endautoescape %}
  {% endif %}

  {{ form_open('account/reset_password/' ~ member.password_reset_url_token,{'class': 'form-horizontal'}) }}
    <fieldset>
      <div>
        <label for="password">New Password</label>
        <input type="password" id="password" name="password" value="">
      </div>
      <div>
        <label for="password_confirmation">Confirm Password</label>
        <input type="password" id="password_confirmation" name="password_confirmation" value="">
      </div>
      <div class="actions">
        <input type="submit" class="" value="Reset My Password" />
      </div>
    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
{% endblock %}
