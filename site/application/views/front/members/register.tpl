{% extends "front/templates/base_content.tpl" %}

{% block title %}
  <h1>{{ page.title }}</h1>
{% endblock %}

{% block content %}

  {% if error %}
    <div class="error">
      <p>There was a problem with your registration, please check the following and try again.</p>
      <ul>
        {% for error in member.errors %}
          <li>{{ error }}</li>
        {% endfor %}
      </ul>
    </div>
  {% else %}
    {% autoescape false %}
      {{ page.main_body }}
    {% endautoescape %}
  {% endif %}

  {{ form_open('account/register',{'class': 'form-horizontal'}) }}
    <fieldset>
      <div>
        <label for="name">Name</label>
        <input type="text" id="name" name="name" value="{{ member.name }}">
      </div>
      <div>
        <label for="email">Email</label>
        <input type="text" id="email" name="email" value="{{ member.email }}">
      </div>
      <div>
        <label for="company">Company Name</label>
        <input type="text" id="company" name="company" value="{{ member.company }}">
      </div>
      <div class="actions">
        <input type="submit" class="" value="Register your interest" />
      </div>
    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
{% endblock %}
