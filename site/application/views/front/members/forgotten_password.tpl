{% extends "front/templates/base_content.tpl" %}

{% block title %}
  <h1>{{ page.title }}</h1>
{% endblock %}

{% block content %}

  {% if flash_error %}
    <div class="error">
      <p>{{ flash_error }}</p>
    </div>
  {% endif %}

  {% if error %}
    <div class="error">
      <p>There was a problem initiating your password reset. Please enter your email address again below.</p>
    </div>
  {% else %}
    {% autoescape false %}
      {{ page.main_body }}
    {% endautoescape %}
  {% endif %}

  {{ form_open('account/forgotten_password',{'class': 'form-horizontal'}) }}
    <fieldset>
      <div>
        <label for="email">Email</label>
        <input type="text" id="email" name="email" value="">
      </div>
      <div class="actions">
        <input type="submit" class="" value="Request Password Reset" />
      </div>
    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
{% endblock %}
