{% extends "front/templates/base_content.tpl" %}

{% block title %}
    <h1>{{ current_section.name }}</h1>
{% endblock %}

{% block content %}

  {% for deal in deals %}
    <section>
        <h3>{{ deal.deal_headline }}</h3>
        {% autoescape false %}
          {{ deal.main_body }}
        {% endautoescape %}
        <span>{{ deal.price }}</span>
    </section>
  {% endfor %}

{% endblock %}

{% block sidebar %}

{% if deal_sections %}
  <ul>
    {% for deal_section in deal_sections %}
    {% if deal_section.deal_count > 0 %}
    <li {% if current_section.id == deal_section.id %}class="current"{% endif %}><a href="{{ deal_section.url }}" title="{{ deal_section.name }}">&raquo; {{ deal_section.name }}</a></li>
    {% endif %}
    {% endfor %}
  </ul>
{% endif %}

{% endblock %}