<body class="clearfix" {% if page.members_only %}id="membership"{% endif %}>
  <!--[if lte IE 7]><p class=chromeframe>Your running an out of date browser! <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

  <div class="navigation">
    <nav role="navigation">
      <a id="responsive_nav_toggle" href="#" title=""><img src="{{ page_settings.base_url }}_img/icons/responsive_nav_toggle.gif" width="" height="" /> Menu</a>
      <ul>
        {% if page.members_only %}
          {% set sections = page_settings.member_sections %}
        {% else %}
          {% set sections = page_settings.sections %}
        {% endif %}
        {% for s in sections %}
          {% if s.page_count > 0 %}
            <li {% if section.id == s.id %}class="current"{% endif %}><a href="{{ s.url }}" title="{{ s.name }}">{{ s.name }}</a></li>
          {% endif %}
        {% endfor %}
      </ul>
    </nav>
  </div>

  <header role="banner">
    {% if page.members_only %}
      <a class="logo" href="/"><img src="{{ page_settings.base_url }}_img/logo_blue.png" alt="" /><span>&laquo; Back home</span></a>
    {% else %}
      <a class="logo" href="/"><img src="{{ page_settings.base_url }}_img/logo.png" alt="" /><span>&laquo; Back home</span></a>
    {% endif %}

    {% if current_user %}
      <div id="login">
        <a href="/members/dashboard" title="">Members Area</a>
        <a href="/account/logout" title="">Logout</a>
      </div>
    {% else %}
      <div id="login">
        <a href="/account/register" title="">Register</a>
        <a href="/account/login" title="">Login Now</a>
      </div>
    {% endif %}
    {% if page.members_only %}
    <h2>Welcome back, {{ current_user.name }}</h2>
    {% endif %}

  </header>

