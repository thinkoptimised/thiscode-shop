{% include "front/templates/_dochead.tpl" %}
{% include "front/templates/_header.tpl" %}
  <div role="main">
    {% block content %}{% endblock %}
  </div>
{% include "front/templates/_footer.tpl" %}
