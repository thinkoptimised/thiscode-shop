{% include "front/templates/_dochead.tpl" %}
{% include "front/templates/_header.tpl" %}
  <div class="outside_wrapper">
    <div role="main" class="content_holder {{ section.slug }}">
      {% block title %}{% endblock %}
      <div class="content">

        {% block content %}{% endblock %}
        {% block documents %}{% endblock %}

      </div>

      <aside role="complementary">

        {% block sidebar %}{% endblock %}

      </aside>
    </div>
  </div>
{% include "front/templates/_footer.tpl" %}