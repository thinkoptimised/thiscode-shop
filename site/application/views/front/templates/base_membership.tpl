{% include "front/templates/_dochead.tpl" %}
{% include "front/templates/_header_membership.tpl" %}
  <div class="outside_wrapper">
    <div role="main" class="content_holder">
      {% block title %}{% endblock %}
      <div class="content">

        {% block content %}{% endblock %}

      </div>

      <aside role="complementary">

        {% block sidebar %}{% endblock %}

      </aside>
    </div>
  </div>
{% include "front/templates/_footer.tpl" %}