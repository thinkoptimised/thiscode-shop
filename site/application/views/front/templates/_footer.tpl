  <footer role="contentinfo" class="clearfix">
    <p>
      {% autoescape false %}
      {{ page_settings.footer_copyright }} | 
      {{ page_settings.footer_address }} | 
      {{ page_settings.footer_telephone }} | 
      email: <a href="mailto:{{ page_settings.footer_email }}">{{ page_settings.footer_email }}</a>
      {% endautoescape %}
    </p>
  </footer>

  <script src="{{ page_settings.base_url }}_js/script-ck.{{ page_settings.timestamp_js }}.js"></script>
  <!--[if (gte IE 6)&(lte IE 8)]>
    <script src="{{ page_settings.base_url }}_js/libs/nwmatcher.js"></script>
    <script src="{{ page_settings.base_url }}_js/libs/selectivizr.js"></script>
  <![endif]-->

  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-34078268-1']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

  </script>

</body>
</html>
