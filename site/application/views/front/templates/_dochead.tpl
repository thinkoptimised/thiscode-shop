<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="cleartype" content="on">
  <meta http-equiv="imagetoolbar" content="false" />
  <meta name="viewport" content="width=device-width">

  <title>{{ page.meta_title }} | {{ page_settings.site_name }}</title>
  <meta name="description" content="{{ page.meta_description }}">

  <link rel="stylesheet" href="{{ page_settings.base_url }}_css/production.{{ page_settings.timestamp_css }}.css">
  <script src="{{ page_settings.base_url }}_js/libs/modernizr-2.5.3.min.js"></script>

</head>
