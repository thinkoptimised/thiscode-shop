{% extends "front/templates/base_homepage.tpl" %}
{% block content %}
  <figure>
    <img src="{{ page_settings.base_url }}_img/homepage/man.jpg" width="450" height="330" alt="" />
    <figcaption>
      <h1>Raising standards</h1>
      <p>For a moment perhaps I stood there, breast-high in the almost boiling water, dumbfounded at my position, hopeless of escape.  Through the reek I could see the people who had been with me in the river scrambling out of the water through the reeds, like little frogs hurrying through grass from the advance of a man, or running to and fro in utter dismay on the towing path.</p>
      <a href="" title="">Find out more &raquo;</a>
    </figcaption>
  </figure>

  <div class="features">
    <section>
      <img src="" width="64" height="64" alt="" /><h4>Snappy feature title</h4>
      <p>The night was dark, and a fine, steady rain was falling.  Phileas Fogg, snugly ensconced in his corner, did not open his lips.  Passepartout, not yet recovered from his stupefaction, clung mechanically to the carpet-bag, with its enormous treasure.</p>
      <a href="" title="">Find out more &raquo;</a>
    </section>

    <section>
      <img src="" width="64" height="64" alt="" /><h4>Snappy feature</h4>
      <p>Had Passepartout been a little less preoccupied, he would have espied the detective ensconced in a corner of the court-room, watching the proceedings with an interest easily understood; for the warrant had failed to reach him at Calcutta, as it had done at Bombay and Suez.</p>
      <a href="" title="">Find out more &raquo;</a>
    </section>

    <section>
      <img src="" width="64" height="64" alt="" /><h4>Another feature title</h4>
      <p>The best course was to wait patiently, and regain the lost time by greater speed when the obstacle was removed.  The procession of buffaloes lasted three full hours, and it was night before the track was clear. </p>
      <a href="" title="">Find out more &raquo;</a>
    </section>

    <section>
      <img src="" width="64" height="64" alt="" /><h4>Another feature title</h4>
      <p>The best course was to wait patiently, and regain the lost time by greater speed when the obstacle was removed.  The procession of buffaloes lasted three full hours, and it was night before the track was clear. </p>
      <a href="" title="">Find out more &raquo;</a>
    </section>
  </div>

  <article>
    <h2>What we do</h2>
    <p>"They're coming!" a woman shrieked, and incontinently everyone was turning and pushing at those behind, in order to clear their way to Woking again.  They must have bolted as blindly as a flock of sheep. Where the road grows narrow and black between the high banks the crowd jammed, and a desperate struggle occurred.  All that crowd did not escape; three persons at least, two women and a little boy, were crushed and trampled there, and left to die amid the terror and the darkness.</p>
  </article>

  <article>
    <h2>How we help</h2>
    <p>And then he perceived that, very slowly, the circular top of the cylinder was rotating on its body.  It was such a gradual movement that he discovered it only through noticing that a black mark that had been near him five minutes ago was now at the other side of the circumference.  Even then he scarcely understood what this indicated, until he heard a muffled grating sound and saw the black mark jerk forward an inch or so.  Then the thing came upon him in a flash.  The cylinder was artificial--hollow--with an end that screwed out! Something within the cylinder was unscrewing the top!</p>
  </article>
{% endblock %}
