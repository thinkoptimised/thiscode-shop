{% extends "front/templates/base_content.tpl" %}

{% block title %}
  <h1>Testing form design</h1>
{% endblock %}

{% block content %}

<form class="form-horizontal">
  <fieldset>

    <div>
      <label class="" for="input01">Text input</label>
      <input type="text" class="" id="input01">
    </div>

    <div>
      <label class="" for="textarea">Textarea</label>
      <textarea class="textarea-small" id="textarea">This is using the small class</textarea>
    </div>

    <div>
      <label class="" for="textarea">Textarea</label>
      <textarea class="textarea-medium" id="textarea">This is using the medium class</textarea>
    </div>

    <div>
      <label class="" for="textarea">Textarea</label>
      <textarea class="" id="textarea">This is using no class</textarea>
    </div>

    <div>
      <label class="" for="textarea">Textarea</label>
      <textarea class="textarea-large" id="textarea">This is using the large class</textarea>
    </div>

    <div>
      <label class="" for="fileInput">File input</label>
      <input class="" id="fileInput" type="file">
    </div>

    <div class="checkbox">
      <span>
        <label for="choice">First choice</label>
        <input type="checkbox" value="yes" name="choice" id="choice">
      </span>
      <span>
        <label for="choice">Second choice</label>
        <input type="checkbox" value="yes" name="choice" id="choice">
      </span>
      <span>
        <label for="choice">Third choice</label>
        <input type="checkbox" value="yes" name="choice" id="choice">
      </span>
    </div>

    <div class="radios">
      <span>
        <label for="choice-first">First choice</label>
        <input type="radio" value="first" checked name="choices" id="choice-first">
      </span>
      <span>
        <label for="choice-second">Second choice</label>
        <input type="radio" value="second" name="choices" id="choice-second">
      </span>
    </div>

    <div>
      <label class="" for="select01">Select list</label>
      <select id="select01">
        <option>something</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
      </select>
    </div>

    <div>
      <label class="" for="multiSelect">Multi-select</label>
      <select multiple="multiple" id="multiSelect">
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
      </select>
    </div>

    <div>
      <input type="submit" class="" value="Save" />
    </div>

  </fieldset>
</form>

{% endblock %}


{% block sidebar %}

  <h2>About us</h2>

  <ul>
    <li><a href="" title="">&raquo; Our people</a></li>
    <li><a href="" title="">&raquo; Committee (AGM)</a></li>
    <li><a href="" title="">&raquo; Our vision and goal</a></li>
    <li><a href="" title="">&raquo; Governance</a></li>
    <li><a href="" title="">&raquo; Corporate Social Responsibility</a></li>
    <li><a href="" title="">&raquo; Responsible Employment</a></li>
    <li><a href="" title="">&raquo; Sustainability / Environmental</a></li>
  </ul>

{% endblock %}