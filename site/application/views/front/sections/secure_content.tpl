{% extends "front/templates/base_membership.tpl" %}

{% block title %}
  <h1>Membership Section</h1>
{% endblock %}

{% block content %}

  <p><a href="">I stood staring</a>, not as yet realising that this was death leaping from man to man in that little distant crowd.  All I felt was that it was something very strange.  An almost noiseless and blinding flash of light, and a man fell headlong and lay still; and as the unseen shaft of heat passed over them, pine trees burst into fire, and every dry furze bush became with one dull thud a mass of flames.  And far away towards Knaphill <a href="">I saw the flashes of trees</a> and hedges and wooden buildings suddenly set alight.</p>

  <ul>
    <li>"Here they are!" shouted a man in a blue jersey</li>
    <li>  "Yonder! D'yer see them?  Yonder!" Quickly, one after the other, one, two, three, four of the armoured Martians appeared, far away over the little trees, across the flat meadows that stretched towards Chertsey, and striding hurriedly towards the river</li>
    <li>  Little cowled figures they seemed at first, going with a rolling motion and as fast as flying birds</li>
    <li> Then, advancing obliquely towards us, came a fifth</li>
    <li>  Their armoured bodies glittered in the sun as they swept swiftly forward upon the guns, growing rapidly larger as they drew nearer</li>
  </ul>

  <ol>
    <li>"You have been asking for water for the last hour," he said</li>
    <li> For a moment we were silent, taking stock of each other</li>
    <li>  I dare say he found me a strange enough figure, naked, save for my water-soaked trousers and socks, scalded, and my face and shoulders blackened by the smoke</li>
    <li>  His face was a fair weakness, his chin retreated, and his hair lay in crisp, almost flaxen curls on his low forehead; his eyes were rather large, pale blue, and blankly staring</li>
    <li> He spoke abruptly, looking vacantly away from me</li>
  </ol>

  <p>It was sweeping round swiftly and steadily, this flaming death, this invisible, inevitable sword of heat.  I perceived it coming towards me by the flashing bushes it touched, and was too astounded and stupefied to stir.  I heard the crackle of fire in the sand pits and the sudden squeal of a horse that was as suddenly stilled.  Then it was as if an invisible yet intensely heated finger were drawn through the heather between me and the Martians, and all along a curving line beyond the sand pits the dark ground smoked and crackled. Something fell with a crash far away to the left where the road from Woking station opens out on the common.  Forth-with the hissing and humming ceased, and the black, dome-like object sank slowly out of sight into the pit.</p>

  <blockquote>All this had happened with such swiftness that I had stood motionless, dumbfounded and dazzled by the flashes of light.  Had that death swept through a full circle, it must inevitably have slain me in my surprise.  But it passed and spared me, and left the night about me suddenly dark and unfamiliar.</blockquote>

  <p>The undulating common seemed now dark almost to blackness, except where its roadways lay grey and pale under the deep blue sky of the early night.  It was dark, and suddenly void of men.  Overhead the stars were mustering, and in the west the sky was still a pale, bright, almost greenish blue.  The tops of the pine trees and the roofs of Horsell came out sharp and black against the western afterglow.  The Martians and their appliances were altogether invisible, save for that thin mast upon which their restless mirror wobbled.  Patches of bush and isolated trees here and there smoked and glowed still, and the houses towards Woking station were sending up spires of flame into the stillness of the evening air.</p>

  <p>Nothing was changed save for that and a terrible astonishment.  The little group of black specks with the flag of white had been swept out of existence, and the stillness of the evening, so it seemed to me, had scarcely been broken.</p>

  <p>It came to me that I was upon this dark common, helpless, unprotected, and alone.  Suddenly, like a thing falling upon me from without, came--fear.</p>

  <p>With an effort I turned and began a stumbling run through the heather.</p>

  <p>The fear I felt was no rational fear, but a panic terror not only of the Martians, but of the dusk and stillness all about me.  Such an extraordinary effect in unmanning me it had that I ran weeping silently as a child might do.  Once I had turned, I did not dare to look back.</p>

  <p>I remember I felt an extraordinary persuasion that I was being played with, that presently, when I was upon the very verge of safety, this mysterious death--as swift as the passage of light--would leap after me from the pit about the cylinder and strike me down.</p>

  <p>It is still a matter of wonder how the Martians are able to slay men so swiftly and so silently.  Many think that in some way they are able to generate an intense heat in a chamber of practically absolute non-conductivity.  This intense heat they project in a parallel beam against any object they choose, by means of a polished parabolic mirror of unknown composition, much as the parabolic mirror of a lighthouse projects a beam of light.  But no one has absolutely proved these details.  However it is done, it is certain that a beam of heat is the essence of the matter.  Heat, and invisible, instead of visible, light.  Whatever is combustible flashes into flame at its touch, lead runs like water, it softens iron, cracks and melts glass, and when it falls upon water, incontinently that explodes into steam.</p>

{% endblock %}


{% block sidebar %}

  <h2>About us</h2>

  <ul>
    <li><a href="" title="">&raquo; Our people</a></li>
    <li><a href="" title="">&raquo; Committee (AGM)</a></li>
    <li><a href="" title="">&raquo; Our vision and goal</a></li>
    <li><a href="" title="">&raquo; Governance</a></li>
    <li><a href="" title="">&raquo; Corporate Social Responsibility</a></li>
    <li><a href="" title="">&raquo; Responsible Employment</a></li>
    <li><a href="" title="">&raquo; Sustainability / Environmental</a></li>
  </ul>

{% endblock %}