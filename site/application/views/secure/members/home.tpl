{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  <div class="hero-unit">
    <h1>Members</h1>
    <p>In this area you will find all the pages currently available on the website. This includes all public facing content, members only content and system pages.</p>
    <p>Listed below are the sections and the pages they contain. Clicking "View Pages" will show you all the pages currently listed in that section.</p>
    <p>Pages can be reordered by dargging the pages to a new position in the list</p>
  </div>

{% if members %}
<table class="table table-striped table-bordered table-condensed">
  <thead>
    <th>Name</th>
    <th>Email</th>
    <th>Activated</th>
    <th>Approved</th>
    <th></th>
  </thead>
  <tbody>
  {% for m in members %}
    <tr>
      <td>{{ m.name }}</td>
      <td>{{ m.email }}</td>
      <td>
        {% if m.activated %}
          <i class="icon-ok"></i>
        {% endif %}
      </td>
      <td>
        {% if m.approved %}
          <i class="icon-ok"></i>
        {% endif %}
      </td>
      <td nowrap="nowrap">
        <a class="btn btn-mini btn-info" href="{{ page_settings.admin_url }}members/edit/{{ m.id }}"><i class="icon-pencil icon-white"></i> Edit</a>
      </td>
    </tr>
  {% endfor %}
  </tbody>
</table>

{% else %}

<div class="alert alert-block alert-error">
  <h4 class="alert-heading">No members have signed up yet</h4>
  When new members signup they will appear in this list, ready for you to approve
</div>

{% endif %}

{% endblock %}

{% block sidebar %}
  {% include 'secure/members/sidebar.tpl' %}
{% endblock %}
