{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  {% if error %}
    <div class="alert alert-error">
      <button class="close" data-dismiss="alert">x</button>
      <p>
        There was a problem saving this member. Please check the following and try again.
      </p>
      <ul>
        {% for e in member.errors %}
          <li>{{ e }}</li>
        {% endfor %}
      </ul>
    </div>
  {% endif %}

  {{ form_open('/secure/members/edit/' ~ member.id,{'class': 'form-horizontal'}) }}
    <fieldset>
      <legend>
        Edit member
        <a style="float: right;" href="{{ page_settings.admin_url}}members/delete/{{ member.id }}" class="btn btn-danger"><i class="icon-white icon-trash"></i> Delete Member</a> 
      </legend>

      {% if member.approved %}
        <div class="control-group">
          <div class="controls">
            <a class="btn btn-danger" rel="nofollow" onclick="return confirm('Are you sure you want to revoke access for this member?');" href="{{ page_settings.admin_url }}members/toggle_approved/{{ member.id }}"><i class="icon-lock icon-white"></i> Revoke Access</a>
          </div>
        </div>
      {% else %}
        <div class="control-group">
          <div class="controls">
            <a class="btn btn-success" rel="nofollow" onclick="return confirm('Are you sure you want to allow access for this member?');" href="{{ page_settings.admin_url }}members/toggle_approved/{{ member.id }}"><i class="icon-ok icon-white"></i> Approve</a>
          </div>
        </div>
      {% endif %}

      <div class="control-group">
        <label class="control-label" for="name">Name: </label>
        <div class="controls">
          <input type="text" class="" id="name" name="name" value="{{ member.name }}">
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="email">Email: </label>
        <div class="controls">
          <input type="text" class="" id="email" name="email" value="{{ member.email }}">
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="company">Company: </label>
        <div class="controls">
          <input type="text" class="" id="company" name="company" value="{{ member.company }}">
        </div>
      </div>


      <div class="form-actions">
        <input type="submit" class="btn btn-primary" value="Save" />
        <a href="{{ page_settings.admin_url }}members" class="btn">Cancel</a>
      </div>

    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
  {% include 'secure/members/sidebar.tpl' %}
{% endblock %}
