<div class="well" style="padding: 8px 0;">
  <ul class="nav nav-list">
    <li class="nav-header">Members</li>
    <li><a href="{{ page_settings.admin_url }}members"><i class="icon-home"></i> View all</a></li>
    <li><a href="{{ page_settings.admin_url }}"><i class="icon-ok"></i> Awaiting Approval</a></li>
  </ul>
</div>
