{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  {{ form_open('secure/deal_sections/delete/' ~ deal_section.id,{'class': 'form-horizontal'}) }}
    <fieldset>
      <legend>
        Delete deal section
      </legend>

      <div class="alert alert-warning">
        <button class="close" data-dismiss="alert">x</button>
        Are you sure you want to delete the deal section: <strong>{{ deal_section.name }}</strong>
        <br><br>
        Please note that there is NO UNDO facility available.
      </div>

      <div class="form-actions">
        <input type="submit" class="btn btn-danger" value="Delete" />
        <a href="{{ page_settings.admin_url }}deal_sections" class="btn">Cancel</a>
      </div>

    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
  {% include 'secure/deal_sections/sidebar.tpl' %}
{% endblock %}
