{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

<div class="hero-unit">
  <h1>Deal sections</h1>
  <p>In this area you will find all the sections currently available on the website. This includes all public facing sections and  members only sections.</p>
  <p>Listed below are the sections and the order they are displayed in their relevant navigation bars. You can change this order by dragging sections to a another place in the list.</p>
</div>

{% if deal_sections %}
<table class="table table-striped table-bordered table-condensed sortable">
  <thead>
    <th></th>
    <th>Name</th>
    <th># Pages</th>
    <th></th>
  </thead>
  <tbody id="deal_sections" data-posturl="/secure/deal_sections/save_position">
  {% for section in deal_sections %}
    <tr class="ui-state-default" id="deal_section_id_{{section.id}}">
      <td style="width: 5%; background-image: url(/_img/_cp/grab_handle.png); background-repeat: no-repeat; background-position: left center"></td>
      <td style="width: 55%">{{ section.name }}</td>
      <td style="width: 20%">{{ section.deal_count }}</td>
      <td style="width: 20%" nowrap="nowrap">
        <a class="btn btn-mini btn-success" href="{{ page_settings.admin_url }}deal_sections/edit/{{ section.id }}"><i class="icon-pencil icon-white"></i> Edit</a>
        {% if section.deal_count == 0 %}
          <a class="btn btn-mini btn-danger" href="{{ page_settings.admin_url }}deal_sections/delete/{{ section.id }}"><i class="icon-trash icon-white"></i> Delete</a>
        {% else %}
          <a class="btn btn-mini btn-danger disabled" href="" rel="popover" data-placement="left" data-content="You can't delete this section as it has at least one page. Removing all the pages from this section will allow you to delete it" data-original-title="You can't delete this section"><i class="icon-trash icon-white"></i> Delete</a>
        {% endif %}
      </td>
    </tr>
  {% endfor %}
  </tbody>
</table>
{% else %}
<div class="alert alert-block alert-error">
  <h4 class="alert-heading">No deal sections have been created yet</h4>
  <p>When new deal sections are created they will appear in this list.</p>
</div>
{% endif %}


{% endblock %}

{% block sidebar %}
  {% include 'secure/deal_sections/sidebar.tpl' %}
{% endblock %}
