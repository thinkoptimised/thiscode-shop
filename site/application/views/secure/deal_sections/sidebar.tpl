<div class="well" style="padding: 8px 0;">
  <ul class="nav nav-list">
    <li class="nav-header">Sections</li>
    <li><a href="{{ page_settings.admin_url }}deal_sections"><i class="icon-home"></i> View all</a></li>
    <li><a href="{{ page_settings.admin_url }}deal_sections/edit"><i class="icon-plus"></i> New deal section</a></li>
  </ul>
</div>
