{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  {% if error %}
    <div class="alert alert-error">
      <button class="close" data-dismiss="alert">x</button>
      <p>
        There was a problem saving this deal section. Please check the following and try again.
      </p>
      <ul>
        {% for e in deal_section.errors %}
          <li>{{ e }}</li>
        {% endfor %}
      </ul>
    </div>
  {% endif %}

  {% if deal_section.is_new_record %}
    {{ form_open('secure/deal_sections/edit',{'class': 'form-horizontal'}) }}
  {% else %}
    {{ form_open('secure/deal_sections/edit/' ~ deal_section.id,{'class': 'form-horizontal'}) }}
  {% endif %}
    <fieldset>
      <legend>
        {% if deal_section.is_new_record %}
          Create a new section
        {% else %}
          Edit section
        {% endif %}
      </legend>

      <div class="control-group">
        <label class="control-label" for="name">Name: </label>
        <div class="controls">
          <input type="text" class="" id="name" name="name" value="{{ deal_section.name }}" tabindex="1" autofucos />
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="position">Position: </label>
        <div class="controls">
          <select name="position" id="position">
            {% for i in 1..20 %}
              <option value="{{ i }}" {% if deal_section.position == i %}selected="selected"{% endif %}>{{ i }}</option>
            {% endfor %}
          </select>
        </div>
      </div>

      <div class="form-actions">
        <input type="submit" class="btn btn-primary" value="Save" />
        <a href="{{ page_settings.admin_url }}deal_sections" class="btn">Cancel</a>
      </div>

    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
  {% include 'secure/deal_sections/sidebar.tpl' %}
{% endblock %}
