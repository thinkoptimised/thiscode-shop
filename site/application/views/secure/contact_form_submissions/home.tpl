{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  <div class="hero-unit">
    <h1>Contact Form Submissions</h1>
    <p>In this area you will find all the pages currently available on the website. This includes all public facing content, members only content and system pages.</p>
    <p>Listed below are the sections and the pages they contain. Clicking "View Pages" will show you all the pages currently listed in that section.</p>
    <p>Pages can be reordered by dargging the pages to a new position in the list</p>
  </div>

  <div class="page-header">
    <h1>Unanswered Submissions <small>Oldest first</small></h1>
  </div>
  {% if incomplete %}
  <table class="table table-striped table-bordered table-condensed">
    <thead>
      <tr>
        <th style="width: 10%">Date Received</th>
        <th style="width: 10%">From</th>
        <th style="width: 10%">Company</th>
        <th style="width: 55%">Message</th>
        <th style="width: 15%"></th>
      </tr>
    </thead>
    <tbody>
    {% for contact in incomplete %}
      <tr>
        <td>{{ contact.created_at | date("F jS") }}<br /><small>{{ contact.created_at | date("g:ia") }}</small></td>
        <td>{{ contact.name }} <br /><a href="mailto:{{ r.email }}"><small>{{ contact.email }}</small></a></td>
        <td>{{ contact.company }}</td>
        <td>{{ contact.message| truncate(200) }}</td>
        <td>
          <a class="btn btn-mini btn-info" href="{{ page_settings.admin_url }}contact_form_submissions/view/{{ contact.id }}"><i class="icon-search icon-white"></i> View</a>
          <a class="btn btn-mini btn-info" href="{{ page_settings.admin_url }}contact_form_submissions/toggle_complete/{{ contact.id }}/1"><i class="icon-check icon-white"></i> Mark as answered</a>
        </td>
      </tr>
    {% endfor %}
    </tbody>
  </table>
  {% else %}
  <div class="alert alert-block alert-error">
    <h4 class="alert-heading">You don't have any unanswered contact form submissions</h4>
    <p>When users submit messages via the contact form, they will appear here.</p>
  </div>
  {% endif %}

  <div class="page-header">
    <h1>All Submissions <small>Newest first</small></h1>
  </div>
  {% if contact_form_submissions %}
  <table class="table table-striped table-bordered table-condensed">
    <thead>
      <tr>
        <th style="width: 10%">Date Received</th>
        <th style="width: 10%">From</th>
        <th style="width: 10%">Company</th>
        <th style="width: 55%">Message</th>
        <th style="width: 15%"></th>
      </tr>
    </thead>
    <tbody>
    {% for submissions in contact_form_submissions %}
      <tr>
        <td>{{ submissions.created_at | date("F jS") }}<br /><small>{{ submissions.created_at | date("g:ia") }}</small></td>
        <td>{{ submissions.name }} <br /><a href="mailto:{{ submissions.email }}"><small>{{ submissions.email }}</small></a></td>
        <td>{{ submissions.company }}</td>
        <td>{{ submissions.message| truncate(200) }}</td>
        <td>
          <a class="btn btn-mini btn-info" href="{{ page_settings.admin_url }}contact_form_submissions/view/{{ submissions.id }}"><i class="icon-search icon-white"></i> View</a>
          {% if submissions.is_actioned %}
            <a class="btn btn-mini btn-success disabled" href="#"><i class="icon-ok icon-white"></i> Answered</a>
          {% else %}
            <a class="btn btn-mini btn-info" href="{{ page_settings.admin_url }}contact_form_submissions/toggle_complete/{{ submissions.id }}/1"><i class="icon-check icon-white"></i> Mark as answered</a>
          {% endif %}
        </td>
      </tr>
    {% endfor %}
    </tbody>
  </table>
  {% else %}
  <div class="alert alert-block alert-error">
    <h4 class="alert-heading">You don't have any contact form submissions</h4>
    <p>When users submit messages via the contact form, they will appear here.</p>
  </div>
  {% endif %}

{% endblock %}

{% block sidebar %}
  {% include 'secure/contact_form_submissions/sidebar.tpl' %}
{% endblock %}
