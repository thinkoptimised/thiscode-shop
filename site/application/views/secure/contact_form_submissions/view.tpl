{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  {% if error %}
    <div class="alert alert-error">
      <button class="close" data-dismiss="alert">x</button>
      <p>
        There was a problem saving this contact_form_submission. Please check the following and try again.
      </p>
      <ul>
        {% for e in contact_form_submission.errors %}
          <li>{{ e }}</li>
        {% endfor %}
      </ul>
    </div>
  {% endif %}

  <form class="form-horizontal">
    <fieldset>
      <legend>
        {% if contact_form_submission.is_new_record %}
          Create a new contact_form_submission
        {% else %}
          Edit contact_form_submission
        {% endif %}
      </legend>

      <div class="control-group">
        <label class="control-label" for="title">Name</label>
        <div class="controls">
          <span class="span6 uneditable-input">{{ contact_form_submission.name }}</span>
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="title">EmailTitle</label>
        <div class="controls">
          <span class="span6 uneditable-input">{{ contact_form_submission.email }}</span>
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="title">Company</label>
        <div class="controls">
          <span class="span6 uneditable-input">{{ contact_form_submission.company }}</span>
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="title">Telephone</label>
        <div class="controls">
          <span class="span6 uneditable-input">{{ contact_form_submission.telephone }}</span>
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="main_body">Main body</label>
        <div class="controls">
          <textarea class="span6 textarea-large uneditable-textarea" disabled="disabled">{% autoescape false %}{{ contact_form_submission.message }}{% endautoescape %}</textarea>
        </div>
      </div>

      <div class="form-actions">
        <a href="/secure/contact_form_submissions" class="btn">Back</a>
        {% if not contact_form_submission.is_actioned %}
        <a class="btn btn-success" href="{{ page_settings.admin_url }}contact_form_submissions/toggle_complete/{{ contact_form_submission.id }}/1"><i class="icon-check icon-white"></i> Mark as answered</a>
        {% endif %}
      </div>

    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
  {% include 'secure/contact_form_submissions/sidebar.tpl' %}
{% endblock %}
