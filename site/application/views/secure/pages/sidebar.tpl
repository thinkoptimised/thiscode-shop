<div class="well" style="padding: 8px 0;">
  <ul class="nav nav-list">
    <li class="nav-header">Pages</li>
    <li><a href="{{ page_settings.admin_url }}pages"><i class="icon-home"></i> View all</a></li>
    <li><a href="{{ page_settings.admin_url }}pages/edit"><i class="icon-plus"></i> New page</a></li>
  </ul>
</div>
