{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  {{ form_open('secure/pages/delete/' ~ page.id,{'class': 'form-horizontal'}) }}
    <fieldset>
      <legend>
        Delete page
      </legend>

      <div class="alert alert-warning">
        <button class="close" data-dismiss="alert">x</button>
        Are you sure you want to delete the page: <strong>{{ page.name }}</strong>
        <br><br>
        Please note that there is NO UNDO facility available.
      </div>

      <div class="form-actions">
        <input type="submit" class="btn btn-danger" value="Delete" />
        <a href="{{ page_settings.admin_url }}pages" class="btn">Cancel</a>
      </div>

    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
  {% include 'secure/pages/sidebar.tpl' %}
{% endblock %}
