{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  {% if error %}
    <div class="alert alert-error">
      <button class="close" data-dismiss="alert">x</button>
      <p>
        There was a problem saving this page. Please check the following and try again.
      </p>
      <ul>
        {% for e in page.errors %}
          <li>{{ e }}</li>
        {% endfor %}
      </ul>
    </div>
  {% endif %}

  {% if page.is_new_record %}
    {{ form_open('secure/pages/edit',{'class': 'form-horizontal'}) }}
  {% else %}
    {{ form_open('secure/pages/edit/' ~ page.id,{'class': 'form-horizontal'}) }}
  {% endif %}
    <fieldset>
      <legend>
        {% if page.is_new_record %}
          Create a new page
        {% else %}
          Edit page
        {% endif %}
      </legend>

      <div class="control-group">
        <label class="control-label" for="section">Section</label>
        <div class="controls">
          <select name="site_section_id" id="site_section_id">
            <optgroup label="Public Sections">
              {% for s in public_sections %}
                <option value="{{ s.id }}" {% if page.site_section_id == s.id %}selected="selected"{% endif %}>{{ s.name }}</option>
              {% endfor %}
            </optgroup>
            <optgroup label="Private Sections">
              {% for s in private_sections %}
                <option value="{{ s.id }}" {% if page.site_section_id == s.id %}selected="selected"{% endif %}>{{ s.name }}</option>
              {% endfor %}
            </optgroup>
            <optgroup label="Other">
              <option value="0" {% if page.site_section_id == 0 %}selected="selected"{% endif %}>Miscellanous Pages</option>
            </optgroup>
          </select>
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="name">Page Name</label>
        <div class="controls">
          <input type="text" class="span6" name="name" value="{{ page.name }}" />
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="title">Page Title</label>
        <div class="controls">
          <input type="text" class="span6" name="title" value="{{ page.title }}" />
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="main_body">Main body</label>
        <div class="controls">
          <textarea class="span6 textarea-large tinymce" name="main_body">{% autoescape false %}{{ page.main_body }}{% endautoescape %}</textarea>
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="meta_title">Meta title</label>
        <div class="controls">
          <input type="text" class="span6" name="meta_title" value="{{ page.meta_title }}" />
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="meta_description">Meta description</label>
        <div class="controls">
          <textarea class="span6 textarea-small" name="meta_description">{{ page.meta_description }}</textarea>
          <p class="help-block">SEO best Practice says the meta description should optimally be between 150-160 characters</p>
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="meta_description">Documents</label>
        <div class="controls" style="width: 500px;">
          <table class="table table-condensed table-striped">
            <thead>
              <tr>
                <th></th>
                <th>Name</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {% for document in documents %}
                <tr>
                  <td style="width: 20px;">
                    <input type="checkbox" name="documents[]" id="document-{{ document.id }}" value="{{ document.id }}" {% if page.is_linked_to_document(document.id) %}checked="checked"{% endif %}> 
                  </td>
                  <td>
                    <label for="document-{{ document.id }}">{{ document.name }}</label>
                  </td>
                  <td style="text-align: right">
                    <a class="btn btn-mini" href="{{ document.admin_url }}"><i class="icon-search"></i> view</a>
                  </td>
                </tr>
              {% endfor %}
            </tbody>
          </table>
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="position">Position: </label>
        <div class="controls">
          <select name="position" id="position">
            {% for i in 1..20 %}
              <option value="{{ i }}" {% if page.position == i %}selected="selected"{% endif %}>{{ i }}</option>
            {% endfor %}
          </select>
        </div>
      </div>

      <div class="form-actions">
        <input type="submit" class="btn btn-success btn-large" name="submit" value="Save Page" />
        <a href="/secure/pages" class="btn">Cancel</a>
      </div>

    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
  {% include 'secure/pages/sidebar.tpl' %}
{% endblock %}
