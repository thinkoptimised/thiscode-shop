{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  <div class="hero-unit">
    <h1>Pages</h1>
    <p>In this area you will find all the pages currently available on the website. This includes all public facing content, members only content and system pages.</p>
    <p>Listed below are the sections and the pages they contain. Clicking "View Pages" will show you all the pages currently listed in that section.</p>
    <p>Pages can be reordered by dargging the pages to a new position in the list</p>
  </div>

  <div id="accordion" class="pages">

  {% for section in sections %}
    {% if section.myPages %}
      <div class="accordion-group" {% if section.members_only %}rel="popover" data-placement="top" data-content="The pages in the {{section.name}} section are only available to signed up {{ page_settings.site_name }} members" data-original-title="Members Only" {% endif %}>
        <div class="accordion-heading">
          <table class="table">
            <thead>
              <tr>
                <th style="width: 5%; text-align: center;">{% if section.members_only %}<i class="icon-lock"></i> {% endif %}</th>
                <th style="width: 45%">{{ section.name }}</th>
                <th style="width: 20%">{{ section.myPages|length }} Pages</th>
                <th style="width: 30%">
                  <a class="btn btn-mini btn-success" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ section.id }}"><i class="icon-search icon-white"></i> View Pages</a>
                  <a class="btn btn-mini btn-info" href="{{ page_settings.admin_url }}sections/edit/{{ section.id }}"><i class="icon-arrow-right icon-white"></i> Goto Section</a>
                </th>
              </tr>
            </thead>
          </table>
        </div>
        <div id="collapse-{{ section.id }}" class="accordion-body collapse">
          <div class="accordion-inner">
            <table class="table table-striped table-bordered table-condensed sortable">
              <thead>
                <tr>
                  <th style="width: 5%"></th>
                  <th style="width: 35%">Page Name</th>
                  <th style="width: 10%">Created</th>
                  <th style="width: 20%">Updated</th>
                  <th style="width: 30%"></th>
                </tr>
              </thead>
              <tbody id="{{section.slug}}" data-posturl="/secure/pages/save_position">
              {% for page in section.myPages %}
                <tr class="ui-state-default" id="pageid_{{page.id}}">
                  <td style="width: 5%; background-image: url(/_img/_cp/grab_handle.png); background-repeat: no-repeat; background-position: left center"></td>
                  <td>{{ page.name }}</td>
                  <td>{{ page.created_at | date('d/m/Y') }}</td>
                  <td>{{ page.updated_at | date('d/m/Y h:i:s') }}</td>
                  <td nowrap="nowrap">
                    <a class="btn btn-mini btn-success" href="{{ page_settings.admin_url }}pages/edit/{{ page.id }}"><i class="icon-pencil icon-white"></i> Edit</a>
                    {% if not page.secured %}
                      <a class="btn btn-mini btn-danger" href="{{ page_settings.admin_url }}pages/delete/{{ page.id }}"><i class="icon-trash icon-white"></i> Delete</a>
                    {% endif %}
                  </td>
                </tr>
              {% endfor %}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    {% endif %}
  {% endfor %}



  {% if miscellaneous_pages %}
  <div class="accordion-group" rel="popover" data-placement="top" data-content="These pages are core system pages and can not be removed. Only edit these pages if you understand how they are used by the system" data-original-title="Warning">
    <div class="accordion-heading">
      <table class="table">
        <thead>
          <tr>
            <th style="width: 5%; text-align: center;"><i class="icon-warning-sign"></i></th>
            <th style="width: 45%">Miscellaneous Pages</th>
            <th style="width: 20%"></th>
            <th style="width: 30%">
              <a class="btn btn-mini btn-success" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ section.id }}"><i class="icon-search icon-white"></i> View Pages</a>
              <a class="btn btn-mini btn-info" href=""><i class="icon-arrow-right icon-white"></i> Goto Section</a>
            </th>
          </tr>
        </thead>
      </table>
    </div>
    <div id="collapse-{{ section.id }}" class="accordion-body collapse">
      <div class="accordion-inner">
        <table class="table table-striped table-bordered table-condensed sortable">
          <thead>
            <tr>
              <th style="width: 5%"></th>
              <th style="width: 35%">Name</th>
              <th style="width: 10%">Created</th>
              <th style="width: 20%">Updated</th>
              <th style="width: 30%"></th>
            </tr>
          </thead>
          <tbody id="misc_pages" data-posturl="/secure/pages/save_position">
          {% for page in miscellaneous_pages %}
            <tr class="ui-state-default" id="pageid_{{page.id}}">
              <td style="width: 5%; background-image: url(/_img/_cp/grab_handle.png); background-repeat: no-repeat; background-position: left center"></td>
              <td>{{ page.name }}</td>
              <td>{{ page.created_at | date('d/m/Y') }}</td>
              <td>{{ page.updated_at | date('d/m/Y h:i:s') }}</td>
              <td nowrap="nowrap">
                <a class="btn btn-mini btn-success" href="{{ page_settings.admin_url }}pages/edit/{{ page.id }}"><i class="icon-pencil icon-white"></i> Edit</a>
                {% if not page.secured %}
                  <a class="btn btn-mini btn-danger" href="{{ page_settings.admin_url }}pages/delete/{{ page.id }}"><i class="icon-trash icon-white"></i> Delete</a>
                {% endif %}
              </td>
            </tr>
          {% endfor %}
          </tbody>
        </table>
      </div>
    </div>
  </div>
  {% endif %}
</div>


{% endblock %}

{% block sidebar %}
  {% include 'secure/pages/sidebar.tpl' %}
{% endblock %}
