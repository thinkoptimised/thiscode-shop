{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  {% if error %}
    <div class="alert alert-error">
      <button class="close" data-dismiss="alert">x</button>
      <p>
        There was a problem saving this setting. Please check the following and try again.
      </p>
      <ul>
        {% for e in setting.errors %}
          <li>{{ e }}</li>
        {% endfor %}
      </ul>
    </div>
  {% endif %}

  {{ form_open('secure/settings/edit/' ~ setting.id,{'class': 'form-horizontal'}) }}
    <fieldset>
      <legend>
        {{ setting.name }}
        {% if setting.required %}
          <span class="label label-important">Required</span>
        {% endif %}
        {% if setting.email %}
          <span class="label label-important">Email Address</span>
        {% endif %}
      </legend>

      <p>{{ setting.description }}</p>

      <div class="control-group">
        <label class="control-label" for="name">Value: </label>
        <div class="controls">
          <textarea name="setting">{{ setting.value }}</textarea>
        </div>
      </div>

      <div class="form-actions">
        <input type="submit" class="btn btn-primary" value="Save" />
        <a href="{{ page_settings.admin_url }}settings" class="btn">Cancel</a>
      </div>

    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
  {% include 'secure/settings/sidebar.tpl' %}
{% endblock %}
