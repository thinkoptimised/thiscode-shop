{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  <div class="hero-unit">
    <h1>Site Settings</h1>
    <p>In this area you will find all the pages currently available on the website. This includes all public facing content, members only content and system pages.</p>
    <p>Listed below are the sections and the pages they contain. Clicking "View Pages" will show you all the pages currently listed in that section.</p>
    <p>Pages can be reordered by dargging the pages to a new position in the list</p>
  </div>

<table class="table table-striped table-bordered table-condensed">
  <thead>
    <th></th>
    <th>Category</th>
    <th>Name</th>
    <th>Value</th>
    <th></th>
    <th></th>
  </thead>
  <tbody>
  {% for s in settings %}
    <tr>
      <td>
        {% if s.required %}
          <span class="label label-important">Required</span>
        {% endif %}
      </td>
      <td>{{ s.category }}</td>
      <td>{{ s.name }}</td>
      <td>{{ s.value | truncate(30) }}</td>
      <td>
        {% if s.email %}
          <span class="label label-info">Email Address</span>
        {% endif %}
      </td>
      <td nowrap="nowrap">
        <a class="btn btn-mini btn-info" href="{{ page_settings.admin_url }}settings/edit/{{ s.id }}"><i class="icon-pencil icon-white"></i> Edit</a>
      </td>
    </tr>
  {% endfor %}
  </tbody>
</table>

{% endblock %}

{% block sidebar %}
  {% include 'secure/settings/sidebar.tpl' %}
{% endblock %}
