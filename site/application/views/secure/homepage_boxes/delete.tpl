{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  {{ form_open('secure/homepage_boxes/delete/' ~ homepage_box.id,{'class': 'form-horizontal'}) }}
    <fieldset>
      <legend>
        Delete homepage box
      </legend>

      <div class="alert alert-warning">
        <button class="close" data-dismiss="alert">x</button>
        Are you sure you want to delete the homepage box: <strong>{{ homepage_box.title }}</strong>
        <br><br>
        Please note that there is NO UNDO facility available.
      </div>

      <div class="form-actions">
        <input type="submit" class="btn btn-danger" value="Delete" />
        <a href="{{ page_settings.admin_url }}hoomepage_boxes" class="btn">Cancel</a>
      </div>

    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
  {% include 'secure/homepage_boxes/sidebar.tpl' %}
{% endblock %}
