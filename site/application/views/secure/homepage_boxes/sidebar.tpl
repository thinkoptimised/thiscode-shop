<div class="well" style="padding: 8px 0;">
  <ul class="nav nav-list">
    <li class="nav-header">Pages</li>
    <li><a href="{{ page_settings.admin_url }}homepage_boxes"><i class="icon-home"></i> View all</a></li>
    <li><a href="{{ page_settings.admin_url }}homepage_boxes/edit"><i class="icon-plus"></i> New homepage box</a></li>
  </ul>
</div>
