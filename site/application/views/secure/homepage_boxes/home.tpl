{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  <div class="hero-unit">
    <h1>Homepage Boxes</h1>
    <p>Listed below are the all the boxes that are available to be shown on the homepage. They can be reordered by dragging them to a new position and <strong>the first 4 boxes will be shown on the homepage</strong></p>
  </div>

  <table class="table table-striped table-bordered table-condensed sortable">
    <thead>
      <tr>
        <th style="width: 5%"></th>
        <th style="width: 5%;">Published</th>
        <th style="width: 65%">Box title</th>
        <th style="width: 15%"></th>
      </tr>
    </thead>
    <tbody id="homepage_boxes" data-posturl="/secure/homepage_boxes/save_position">
    {% for box in homepage_boxes %}
      <tr class="ui-state-default" id="boxid_{{box.id}}">
        <td style="width: 5%; background-image: url(/_img/_cp/grab_handle.png); background-repeat: no-repeat; background-position: left center"></td>
        <td>
          {% if box.published %}
            <div class="btn-group">
              <a class="btn btn-info btn-mini dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="icon-ok icon-white"></i> Published
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li><a href="{{ page_settings.admin_url }}homepage_boxes/toggle_publish/{{ box.id }}/0"><i class="icon-remove"></i> Unpublish</a></li>
              </ul>
            </div>
          {% else %}
              <a class="btn btn-success btn-mini" href="{{ page_settings.admin_url }}homepage_boxes/toggle_publish/{{ box.id }}/1"><i class="icon-ok icon-white"></i> Publish</a>
          {% endif %}
        </td>
        <td>{{ box.title }}</td>
        <td nowrap="nowrap">
          <a class="btn btn-mini btn-success" href="{{ page_settings.admin_url }}homepage_boxes/edit/{{ box.id }}"><i class="icon-pencil icon-white"></i> Edit</a>
          <a class="btn btn-mini btn-danger" href="{{ page_settings.admin_url }}homepage_boxes/delete/{{ box.id }}"><i class="icon-trash icon-white"></i> Delete</a>
        </td>
      </tr>
    {% endfor %}
    </tbody>
  </table>


{% endblock %}

{% block sidebar %}
  {% include 'secure/homepage_boxes/sidebar.tpl' %}
{% endblock %}
