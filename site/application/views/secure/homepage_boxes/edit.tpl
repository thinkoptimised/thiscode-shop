{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  {% if error %}
    <div class="alert alert-error">
      <button class="close" data-dismiss="alert">x</button>
      <p>
        There was a problem saving this homepage box. Please check the following and try again.
      </p>
      <ul>
        {% for e in homepage_box.errors %}
          <li>{{ e }}</li>
        {% endfor %}
      </ul>
    </div>
  {% endif %}

  {% if homepage_box.is_new_record %}
    {{ form_open_multipart('secure/homepage_boxes/edit',{'class': 'form-horizontal'}) }}
  {% else %}
    {{ form_open_multipart('secure/homepage_boxes/edit/' ~ homepage_box.id,{'class': 'form-horizontal'}) }}
  {% endif %}
    <fieldset>
      <legend>
        {% if homepage_box.is_new_record %}
          Create a new homepage box
        {% else %}
          Edit homepage box
        {% endif %}
      </legend>

      <div class="control-group">
        <label class="control-label" for="title">Title</label>
        <div class="controls">
          <input type="text" class="span6" name="title" value="{{ homepage_box.title }}" />
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="main_body">Main body</label>
        <div class="controls">
          <textarea class="span6 textarea-large tinymce" name="main_body">{% autoescape false %}{{ homepage_box.main_body }}{% endautoescape %}</textarea>
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="link">Link</label>
        <div class="controls">
          <input type="text" class="span6" name="link" value="{{ homepage_box.link }}" />
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="link_text">Link text</label>
        <div class="controls">
          <input type="text" class="span6" name="link_text" value="{{ homepage_box.link_text }}" />
        </div>
      </div>


      {% if homepage_box.imagefile %}
        <div class="modal hide fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-header">
            <a type="button" class="close" data-dismiss="modal" aria-hidden="true">×</a>
            <h3 id="myModalLabel">{{ homepage_box.title }}</h3>
          </div>
          <div class="modal-body">
            <img src='{{ page_settings.base_url }}_img/uploads/homepage_boxes/{{ homepage_box.imagefile }}' />
          </div>
          <div class="modal-footer">
            <a class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
          </div>
        </div>
      {% endif %}

      <div class="control-group">
        <label class="control-label" for="imagefile">Upload an image: </label>
        <div class="controls">
          <input type="file" name="imagefile">{% if homepage_box.imagefile %}<a href="#myModal" class="btn btn-info" data-toggle="modal"><i class="icon-search icon-white"></i> View current image</a>{% endif %}
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="image_alt">Image Alt</label>
        <div class="controls">
          <input type="text" class="span6" name="image_alt" value="{{ homepage_box.image_alt }}" />
        </div>
      </div>

      <div class="form-actions">
        <input type="submit" class="btn btn-success btn-large" name="submit" value="Save Page" />
        <a href="/secure/homepage_boxes" class="btn">Cancel</a>
      </div>

    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
  {% include 'secure/homepage_boxes/sidebar.tpl' %}
{% endblock %}
