{% extends "secure/templates/logged_in.tpl" %}

{% block sidebar %}
  <div class="well" style="padding: 8px 0;">
    <ul class="nav nav-list">
      <li class="nav-header">Quick Links</li>
      <li><a href="{{ page_settings.admin_url }}users"><i class="icon-user"></i> Manage users</a></li>
      <li><a href="{{ page_settings.admin_url }}homepage_boxes"><i class="icon-th"></i> Manage homepage boxes</a></li>
      <li><a href="{{ page_settings.admin_url }}sections/edit"><i class="icon-plus"></i> Add a new section</a></li>
      <li><a href="{{ page_settings.admin_url }}pages/edit"><i class="icon-plus"></i> Add a new page</a></li>
      <li><a href="{{ page_settings.admin_url }}documents/edit"><i class="icon-arrow-up"></i> Upload a document</a></li>
    </ul>
  </div>
{% endblock %}

{% block content %}
<div class="page-header">
  <h1>Visits to the site <small>Over the last month</small></h1>
</div>
<div class="flot_chart" data-charturl="dashboard/ga_visits_json"></div>

<div class="page-header">
  <h1>Newly registered members</h1>
</div>
{% if members_awaiting_approval %}
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      {% for member in members_awaiting_approval %}
        <tr>
          <td>{{ member.name }}</td>
          <td>{{ member.email }}</td>
          <td>
            <a class="btn btn-mini btn-success" href="{{ page_settings.admin_url }}members/toggle_approved/{{ member.id }}"><i class="icon-ok icon-white"></i> Approve</a>
            <a class="btn btn-mini btn-info" href="{{ page_settings.admin_url }}members/edit/{{ member.id }}"><i class="icon-pencil icon-white"></i> Edit</a>
          </td>
        </tr>
      {% endfor %}
    </tbody>
  </table>

{% else %}

<div class="alert alert-block alert-error">
  <h4 class="alert-heading">No members have signed up yet</h4>
  When new members signup they will appear in this list, ready for you to approve
</div>

{% endif %}

  <div class="row-fluid">
    <div class="span6 well">
      <div class="page-header">
        <h1>Latest Created Articles</h1>
      </div>
      {% if recently_created %}
      <table class="table table-striped table-bordered table-condensed">
        <thead>
          <tr>
            <th style="width: 15%"></th>
            <th style="width: 75%">Article title</th>
            <th style="width: 15%"></th>
          </tr>
        </thead>
        <tbody>
        {% for rc in recently_created %}
          <tr>
            <td>
              {% if rc.is_published %}
                <div class="btn-group">
                  <a class="btn btn-info btn-mini dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="icon-ok icon-white"></i> Published
                    <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a href="{{ page_settings.admin_url }}news_items/toggle_publish/{{ rc.id }}/0"><i class="icon-remove"></i> Unpublish</a></li>
                  </ul>
                </div>
              {% else %}
                  <a class="btn btn-success btn-mini" href="{{ page_settings.admin_url }}news_items/toggle_publish/{{ rc.id }}/1"><i class="icon-share icon-white"></i> Publish</a>
              {% endif %}
            </td>
            <td>{{ rc.title }}</td>
            <td nowrap="nowrap">
              <a class="btn btn-mini btn-success" href="{{ page_settings.admin_url }}news_items/edit/{{ rc.id }}"><i class="icon-pencil icon-white"></i> Edit</a>
              <a class="btn btn-mini btn-danger" href="{{ page_settings.admin_url }}news_items/delete/{{ rc.id }}"><i class="icon-trash icon-white"></i> Delete</a>
            </td>
          </tr>
        {% endfor %}
        </tbody>
      </table>
      {% else %}
      <div class="alert alert-block alert-error">
        <h4 class="alert-heading">No articles have been created yet</h4>
        <p>When new articles are craeted they will appear in this list.</p>
      </div>
      {% endif %}
    </div>

    <div class="span6 well">
      <div class="page-header">
        <h1>Latest Published Articles</h1>
      </div>
      {% if recently_published %}
      <table class="table table-striped table-bordered table-condensed">
        <thead>
          <tr>
            <th style="width: 15%"></th>
            <th style="width: 70%">Article title</th>
            <th style="width: 15%"></th>
          </tr>
        </thead>
        <tbody>
        {% for rp in recently_published %}
          <tr>
            <td>
              {% if rp.is_published %}
                <div class="btn-group">
                  <a class="btn btn-info btn-mini dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="icon-ok icon-white"></i> Published
                    <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a href="{{ page_settings.admin_url }}news_items/toggle_publish/{{ rp.id }}/0"><i class="icon-remove"></i> Unpublish</a></li>
                  </ul>
                </div>
              {% else %}
                  <a class="btn btn-success btn-mini" href="{{ page_settings.admin_url }}news_items/toggle_publish/{{ rp.id }}/1"><i class="icon-share icon-white"></i> Publish</a>
              {% endif %}
            </td>
            <td>{{ rp.title }}</td>
            <td nowrap="nowrap">
              <a class="btn btn-mini btn-success" href="{{ page_settings.admin_url }}news_items/edit/{{ rp.id }}"><i class="icon-pencil icon-white"></i> Edit</a>
              <a class="btn btn-mini btn-danger" href="{{ page_settings.admin_url }}news_items/delete/{{ rp.id }}"><i class="icon-trash icon-white"></i> Delete</a>
            </td>
          </tr>
        {% endfor %}
        </tbody>
      </table>
      {% else %}
      <div class="alert alert-block alert-error">
        <h4 class="alert-heading">No articles have been published yet</h4>
        <p>When new articles are published they will appear in this list.</p>
      </div>
      {% endif %}
    </div>
  </div>

  <div class="page-header">
    <h1>Unanswered Contact Form Submissions <small>Oldest first</small></h1>
  </div>
  {% if contact_form %}
  <table class="table table-striped table-bordered table-condensed">
    <thead>
      <tr>
        <th style="width: 10%">Date Received</th>
        <th style="width: 10%">From</th>
        <th style="width: 10%">Company</th>
        <th style="width: 55%">Message</th>
        <th style="width: 15%"></th>
      </tr>
    </thead>
    <tbody>
    {% for contact in contact_form %}
      <tr>
        <td>{{ contact.created_at | date("F jS") }}<br /><small>{{ contact.created_at | date("g:ia") }}</small></td>
        <td>{{ contact.name }} <br /><a href="mailto:{{ r.email }}"><small>{{ contact.email }}</small></a></td>
        <td>{{ contact.company }}</td>
        <td>{{ contact.message| truncate(200) }}</td>
        <td>
          <a class="btn btn-mini btn-info" href="{{ page_settings.admin_url }}contact_form_submissions/view/{{ contact.id }}"><i class="icon-search icon-white"></i> View</a>
          <a class="btn btn-mini btn-info" href="{{ page_settings.admin_url }}contact_form_submissions/toggle_complete/{{ contact.id }}/1"><i class="icon-check icon-white"></i> Mark as answered</a>
        </td>
      </tr>
    {% endfor %}
    </tbody>
  </table>
  {% else %}
  <div class="alert alert-block alert-error">
    <h4 class="alert-heading">You don't have any unanswered contact form submissions</h4>
    <p>When users submit messages via the contact form, they will appear here.</p>
  </div>
  {% endif %}

{% endblock %}
