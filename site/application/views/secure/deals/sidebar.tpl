<div class="well" style="padding: 8px 0;">
  <ul class="nav nav-list">
    <li class="nav-header">Deals</li>
    <li><a href="{{ page_settings.admin_url }}deals"><i class="icon-home"></i> View all</a></li>
    <li><a href="{{ page_settings.admin_url }}deals/edit"><i class="icon-plus"></i> New deal</a></li>
  </ul>
</div>
