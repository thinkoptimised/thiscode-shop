{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  <div class="hero-unit">
    <h1>Deals</h1>
    <p>In this area you will find all the pages currently available on the website. This includes all public facing content, members only content and system pages.</p>
    <p>Listed below are the sections and the pages they contain. Clicking "View Pages" will show you all the pages currently listed in that section.</p>
    <p>Pages can be reordered by dragging the pages to a new position in the list</p>
  </div>

  <div id="accordion" class="pages">
  {% if deals %}
  {% for section in deal_sections %}
    {% if section.myDeals %}
      <div class="accordion-group">
        <div class="accordion-heading">
          <table class="table">
            <thead>
              <tr>
                <th style="width: 5%; text-align: center;"></th>
                <th style="width: 45%">{{ section.name }}</th>
                <th style="width: 20%">{{ section.myDeals|length }} Deals</th>
                <th style="width: 30%">
                  <a class="btn btn-mini btn-success" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ section.id }}"><i class="icon-search icon-white"></i> View Pages</a>
                  <a class="btn btn-mini btn-info" href="{{ page_settings.admin_url }}deal_sections/edit/{{ section.id }}"><i class="icon-arrow-right icon-white"></i> Goto Section</a>
                </th>
              </tr>
            </thead>
          </table>
        </div>
        <div id="collapse-{{ section.id }}" class="accordion-body collapse">
          <div class="accordion-inner">
            <table class="table table-striped table-bordered table-condensed sortable">
              <thead>
                <tr>
                  <th style="width: 5%"></th>
                  <th style="width: 35%">Deal Name</th>
                  <th style="width: 10%">Created</th>
                  <th style="width: 20%">Updated</th>
                  <th style="width: 30%"></th>
                </tr>
              </thead>
              <tbody id="{{section.slug}}" data-posturl="/secure/deals/save_position">
              {% for deal in section.myDeals %}
                <tr class="ui-state-default" id="dealid_{{deal.id}}">
                  <td style="width: 5%; background-image: url(/_img/_cp/grab_handle.png); background-repeat: no-repeat; background-position: left center"></td>
                  <td>{{ deal.name }}</td>
                  <td>{{ deal.created_at | date('d/m/Y') }}</td>
                  <td>{{ deal.updated_at | date('d/m/Y h:i:s') }}</td>
                  <td nowrap="nowrap">
                    <a class="btn btn-mini btn-success" href="{{ page_settings.admin_url }}deals/edit/{{ deal.id }}"><i class="icon-pencil icon-white"></i> Edit</a>
                    <a class="btn btn-mini btn-danger" href="{{ page_settings.admin_url }}deals/delete/{{ deal.id }}"><i class="icon-trash icon-white"></i> Delete</a>
                  </td>
                </tr>
              {% endfor %}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    {% endif %}
  {% endfor %}

  {% else %}
    <div class="alert alert-block alert-error">
      <h4 class="alert-heading">No deals have been created yet</h4>
      <p>When new deals are created they will appear in this list.</p>
    </div>
  {% endif %}
</div>


{% endblock %}

{% block sidebar %}
  {% include 'secure/deals/sidebar.tpl' %}
{% endblock %}
