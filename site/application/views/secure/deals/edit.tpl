{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  {% if error %}
    <div class="alert alert-error">
      <button class="close" data-dismiss="alert">x</button>
      <p>
        There was a problem saving this deal. Please check the following and try again.
      </p>
      <ul>
        {% for e in deal.errors %}
          <li>{{ e }}</li>
        {% endfor %}
      </ul>
    </div>
  {% endif %}

  {% if deal.is_new_record %}
    {{ form_open('secure/deals/edit',{'class': 'form-horizontal'}) }}
  {% else %}
    {{ form_open('secure/deals/edit/' ~ deal.id,{'class': 'form-horizontal'}) }}
  {% endif %}
    <fieldset>
      <legend>
        {% if deal.is_new_record %}
          Create a new deal
        {% else %}
          Edit deal
        {% endif %}
      </legend>

      <div class="control-group">
        <label class="control-label" for="section">Deal Sections</label>
        <div class="controls">
          <select name="deal_section_id" id="deal_section_id">
            {% for s in deal_sections %}
              <option value="{{ s.id }}" {% if deal.deal_section_id == s.id %}selected="selected"{% endif %}>{{ s.name }}</option>
            {% endfor %}
          </select>
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="name">Deal Name</label>
        <div class="controls">
          <input type="text" class="span6" name="name" value="{{ deal.name }}" />
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="deal_headline">Deal Headline</label>
        <div class="controls">
          <input type="text" class="span6" name="deal_headline" value="{{ deal.deal_headline }}" />
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="main_body">Main body</label>
        <div class="controls">
          <textarea class="span6 textarea-large tinymce" name="main_body">{% autoescape false %}{{ deal.main_body }}{% endautoescape %}</textarea>
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="price">Price</label>
        <div class="controls">
          <input type="text" class="span2" name="price" value="{{ deal.price }}" />
        </div>
      </div>

      <div class="form-actions">
        <input type="submit" class="btn btn-success btn-large" name="submit" value="Save Page" />
        <a href="/secure/deals" class="btn">Cancel</a>
      </div>

    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
  {% include 'secure/deals/sidebar.tpl' %}
{% endblock %}
