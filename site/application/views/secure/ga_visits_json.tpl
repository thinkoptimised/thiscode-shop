{ "visits": [
    {
        "label": "Visitors",
        "data":
            [
            {% for data in json %}
                ["{{ data.date|date('U') * 1000 }}", {{ data.visitors }}]
                {% if not loop.last %},{% endif %}
            {% endfor %}
            ]
    },
    {
        "label": "New Visitors",
        "data":
            [
            {% for data in json %}
                ["{{ data.date|date('U') * 1000 }}", {{ data.newvisits }}]
                {% if not loop.last %},{% endif %}
            {% endfor %}
            ]
    }
]}