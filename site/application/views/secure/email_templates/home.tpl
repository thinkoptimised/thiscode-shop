{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  <div class="hero-unit">
  <h1>Email Templates</h1>
  <p>In this area you will find all the sections currently available on the website. This includes all public facing sections and  members only sections.</p>
  <p>Listed below are the sections and the order they are displayed in their relevant navigation bars. You can change this order by dragging sections to a another place in the list.</p>
</div>

<table class="table table-striped table-bordered table-condensed">
  <thead>
    <th>Template Name</th>
    <th>Description</th>
    <th></th>
  </thead>
  <tbody>
  {% for template in email_templates %}
    <tr>
      <td style="width: 20%">{{ template.name }}</td>
      <td style="width: 70%">{{ template.description }}</td>
      <td style="width: 10%" nowrap="nowrap">
        <a class="btn btn-mini btn-success" href="{{ page_settings.admin_url }}email_templates/edit/{{ template.id }}"><i class="icon-pencil icon-white"></i> Edit</a>
      </td>
    </tr>
  {% endfor %}
  </tbody>
</table>


{% endblock %}

{% block sidebar %}
  {% include 'secure/email_templates/sidebar.tpl' %}
{% endblock %}
