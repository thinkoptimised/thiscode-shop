{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  {% if error %}
    <div class="alert alert-error">
      <button class="close" data-dismiss="alert">x</button>
      <p>
        There was a problem saving this setting. Please check the following and try again.
      </p>
      <ul>
        {% for e in email_templates.errors %}
          <li>{{ e }}</li>
        {% endfor %}
      </ul>
    </div>
  {% endif %}

  {{ form_open('secure/email_templates/edit/' ~ email_templates.id,{'class': 'form-horizontal'}) }}
    <fieldset>
      <legend>{{ email_templates.name }}</legend>

      <p>{{ email_templates.description }}</p>

      <div class="control-group">
        <label class="control-label" for="title">Title: </label>
        <div class="controls">
          <input type="text" class="span6" name="title" value="{{ email_templates.title }}" />
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="teaser">Teaser: </label>
        <div class="controls">
          <textarea name="teaser" class="span6 textarea-small">{{ email_templates.teaser }}</textarea>
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="heading">Heading: </label>
        <div class="controls">
          <input type="text" class="span6" name="heading" value="{{ email_templates.heading }}" />
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="main_body">Main Body: </label>
        <div class="controls">
          <textarea name="main_body" class="span6 textarea-large tinymce">{{ email_templates.main_body }}</textarea>
        </div>
      </div>

      <div class="form-actions">
        <input type="submit" class="btn btn-primary" value="Save" />
        <a href="{{ page_settings.admin_url }}email_templates" class="btn">Cancel</a>
      </div>

    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
  {% include 'secure/email_templates/sidebar.tpl' %}
{% endblock %}
