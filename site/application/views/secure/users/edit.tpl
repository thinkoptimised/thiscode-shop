{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  {% if error %}
    <div class="alert alert-error">
      <button class="close" data-dismiss="alert">x</button>
      <p>
        There was a problem saving this user. Please check the following and try again.
      </p>
      <ul>
        {% for e in user.errors %}
          <li>{{ e }}</li>
        {% endfor %}
      </ul>
    </div>
  {% endif %}

  {% if user.is_new_record %}
    {{ form_open('secure/users/edit',{'class': 'form-horizontal'}) }}
  {% else %}
    {{ form_open('secure/users/edit/' ~ user.id,{'class': 'form-horizontal'}) }}
  {% endif %}
    <fieldset>
      <legend>
        {% if user.is_new_record %}
          Create a new user
        {% else %}
          Edit user
        {% endif %}
      </legend>

      <div class="control-group">
        <label class="control-label" for="name">Name: </label>
        <div class="controls">
          <input type="text" class="" id="name" name="name" value="{{ user.name }}">
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="email">Email: </label>
        <div class="controls">
          <input type="text" class="" id="email" name="email" value="{{ user.email }}">
        </div>
      </div>

      <div class="form-actions">
        <input type="submit" class="btn btn-primary" value="Save" />
        <a href="{{ page_settings.admin_url }}users" class="btn">Cancel</a>
      </div>

    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
  {% include 'secure/users/sidebar.tpl' %}
{% endblock %}
