{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  <div class="hero-unit">
    <h1>Admin Users</h1>
    <p>In this area you will find all the pages currently available on the website. This includes all public facing content, members only content and system pages.</p>
    <p>Listed below are the sections and the pages they contain. Clicking "View Pages" will show you all the pages currently listed in that section.</p>
    <p>Pages can be reordered by dargging the pages to a new position in the list</p>
  </div>

<table class="table table-striped table-bordered table-condensed">
  <thead>
    <th>Name</th>
    <th>Email</th>
    <th>Activated</th>
    <th></th>
  </thead>
  <tbody>
  {% for user in users %}
    <tr>
      <td>{{ user.name }}</td>
      <td>{{ user.email }}</td>
      <td>
        {% if user.activated %}
          <i class="icon-ok"></i>
        {% endif %}
      </td>
      <td nowrap="nowrap">
        <a class="btn btn-mini btn-success" href="{{ page_settings.admin_url }}users/edit/{{ user.id }}"><i class="icon-pencil icon-white"></i> Edit</a>
        {% if not user.super_user %}
        <a class="btn btn-mini btn-danger" href="{{ page_settings.admin_url }}users/delete/{{ user.id }}"><i class="icon-trash icon-white"></i> Delete</a>
        {% endif %}
        {% if not user.activated %}
          <a class="btn btn-mini btn-email" href="{{ page_settings.admin_url }}users/resend_activation/{{ user.id }}" rel="popover" data-placement="left" data-content="This user has not logged into the system yet. You can send them another account activation email by clicking this button" data-original-title="User not yet activated"><i class="icon-envelope"></i> Resend Activation</a>
        {% endif %}
      </td>
    </tr>
  {% endfor %}
  </tbody>
</table>

{% endblock %}

{% block sidebar %}
  {% include 'secure/users/sidebar.tpl' %}
{% endblock %}
