{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  {{ form_open('secure/users/delete/' ~ user.id,{'class': 'form-horizontal'}) }}
    <fieldset>
      <legend>
        Delete user
      </legend>

      <div class="alert alert-warning">
        <button class="close" data-dismiss="alert">x</button>
        Are you sure you want to delete the user: <strong>{{ user.name }} &lt;{{ user.email }}&gt;</strong>
        <br><br>
        Please note that there is NO UNDO facility available.
      </div>

      <div class="form-actions">
        <input type="submit" class="btn btn-danger" value="Delete" />
        <a href="{{ page_settings.admin_url }}users" class="btn">Cancel</a>
      </div>

    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
  {% include 'secure/users/sidebar.tpl' %}
{% endblock %}
