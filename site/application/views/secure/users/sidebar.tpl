<div class="well" style="padding: 8px 0;">
  <ul class="nav nav-list">
    <li class="nav-header">Admin Users</li>
    <li><a href="{{ page_settings.admin_url }}users"><i class="icon-home"></i> View all</a></li>
    <li><a href="{{ page_settings.admin_url }}users/edit"><i class="icon-plus"></i> New user</a></li>
  </ul>
</div>
