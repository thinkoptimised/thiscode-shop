{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  {% if error %}
    <div class="alert alert-error">
      <button class="close" data-dismiss="alert">x</button>
      <p>
        There was a problem with your profile. Please check the following and try again.
      </p>
      <ul>
        {% for e in user.errors %}
          <li>{{ e }}</li>
        {% endfor %}
      </ul>
    </div>
  {% endif %}

  {{ form_open('secure/users/profile',{'class': 'form-horizontal'}) }}
    <fieldset>
      <legend>My Profile</legend>
      <div class="control-group">
        <label class="control-label" for="name">Name:</label>
        <div class="controls">
          <input type="text" id="name" name="name" value="{{ user.name }}">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="email">Email:</label>
        <div class="controls">
          <input type="email" id="email" name="email" value="{{ user.email }}">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="password">Password:</label>
        <div class="controls">
          <input type="password" id="password" name="password" value="">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="password_confirmation">Confirm Password:</label>
        <div class="controls">
          <input type="password" id="password_confirmation" name="password_confirmation" value="">
          <span class="help-block">Leave blank to keep existing password</span>
        </div>
      </div>
      <div class="form-actions">
        <button type="submit" class="btn btn-primary">Update My Profile</button>
      </div>
    </fieldset>
  </form>
{% endblock %}
