{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  {% if error %}
    <div class="alert alert-error">
      <button class="close" data-dismiss="alert">x</button>
      <p>
        There was a problem saving this section. Please check the following and try again.
      </p>
      <ul>
        {% for e in section.errors %}
          <li>{{ e }}</li>
        {% endfor %}
      </ul>
    </div>
  {% endif %}

  {% if section.is_new_record %}
    {{ form_open('secure/sections/edit',{'class': 'form-horizontal'}) }}
  {% else %}
    {{ form_open('secure/sections/edit/' ~ section.id,{'class': 'form-horizontal'}) }}
  {% endif %}
    <fieldset>
      <legend>
        {% if section.is_new_record %}
          Create a new section
        {% else %}
          Edit section
        {% endif %}
      </legend>

      <div class="control-group">
        <label class="control-label" for="name">Name: </label>
        <div class="controls">
          <input type="text" class="" id="name" name="name" value="{{ section.name }}">
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="members_only">Members Only: </label>
        <div class="controls">
          <input type="checkbox" name="members_only" id="members_only" value="1" {% if section.members_only %}checked="checked"{% endif %}>
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="position">Position: </label>
        <div class="controls">
          <select name="position" id="position">
            {% for i in 1..20 %}
              <option value="{{ i }}" {% if section.position == i %}selected="selected"{% endif %}>{{ i }}</option>
            {% endfor %}
          </select>
        </div>
      </div>

      <div class="form-actions">
        <input type="submit" class="btn btn-primary" value="Save" />
        <a href="{{ page_settings.admin_url }}sections" class="btn">Cancel</a>
      </div>

    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
  {% include 'secure/sections/sidebar.tpl' %}
{% endblock %}
