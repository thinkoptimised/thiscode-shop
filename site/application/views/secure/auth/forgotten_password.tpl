{% extends "secure/templates/logged_out.tpl" %}
{% block content %}


  {{ form_open('secure/auth/forgotten_password',{'class': 'form-horizontal'}) }}
    <fieldset>
      <legend>Forgotten Password</legend>

      {% if error %}
        <div class="alert alert-error">
            An account with that email address could not be found. Please try again.
        </div>
      {% else %}
        <div class="alert alert-notice">
          Enter your email address below and we will send you an email containing instructions on how to reset your password.
        </div>
      {% endif %}

      <div class="control-group">
        <label class="control-label" for="email">Email:</label>
        <div class="controls">
          <input type="email" id="email" name="email" value="">
        </div>
      </div>
      <div class="form-actions">
        <button type="submit" class="btn btn-primary">Reset My Password</button>
        |
        <a href="/secure/auth">Back to Login</a>
      </div>
    </fieldset>
  </form>
{% endblock %}
