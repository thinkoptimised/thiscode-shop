{% extends "secure/templates/logged_out.tpl" %}
{% block content %}

  {% if error %}
    <div class="alert alert-error">
      <button class="close" data-dismiss="alert">x</button>
      <p>
        There was a problem with your password reset. Please check the following and try again.
      </p>
      <ul>
        {% for e in user.errors %}
          <li>{{ e }}</li>
        {% endfor %}
      </ul>
    </div>
  {% endif %}

  {{ form_open('secure/auth/reset_password/' ~ user.password_reset_url_token,{'class': 'form-horizontal'}) }}
    <fieldset>
      <legend>Reset Your Password</legend>
      <div class="control-group">
        <label class="control-label" for="password">Password:</label>
        <div class="controls">
          <input type="password" id="password" name="password" value="">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="password_confirmation">Confirm Password:</label>
        <div class="controls">
          <input type="password" id="password_confirmation" name="password_confirmation" value="">
        </div>
      </div>
      <div class="form-actions">
        <button type="submit" class="btn btn-primary">Reset Password</button>
        | <a href="/secure/auth">Back to Login</a>
      </div>
    </fieldset>
  </form>
{% endblock %}
