{% extends "secure/templates/logged_out.tpl" %}
{% block content %}

  <div class="alert alert-error">
    <button class="close" data-dismiss="alert">x</button>
    <p>
      There was a problem with your password reset. Please click the link in your email again.
      If the problem persists, please contact the site administrator.
    </p>
  </div>

{% endblock %}
