{% extends "secure/templates/logged_out.tpl" %}
{% block content %}

  {{ form_open('secure/auth',{'class': 'form-horizontal'}) }}
    <fieldset>
      <legend>Login</legend>
      <div class="control-group">
        <label class="control-label" for="email">Email:</label>
        <div class="controls">
          <input type="email" id="email" name="email" value="" tabindex="1" autofucus>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="password">Password:</label>
        <div class="controls">
          <input type="password" id="password" name="password" value="" tabindex="2">
        </div>
      </div>
      <div class="form-actions">
        <button type="submit" class="btn btn-primary">Login</button>
        |
        <a href="/secure/auth/forgotten_password">Forgotten Your Password?</a>
      </div>
    </fieldset>
  </form>
{% endblock %}
