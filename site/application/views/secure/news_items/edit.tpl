{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  {% if error %}
    <div class="alert alert-error">
      <button class="close" data-dismiss="alert">x</button>
      <p>
        There was a problem saving this news_item. Please check the following and try again.
      </p>
      <ul>
        {% for e in news_item.errors %}
          <li>{{ e }}</li>
        {% endfor %}
      </ul>
    </div>
  {% endif %}

  {% if news_item.is_new_record %}
    {{ form_open('secure/news_items/edit',{'class': 'form-horizontal'}) }}
  {% else %}
    {{ form_open('secure/news_items/edit/' ~ news_item.id,{'class': 'form-horizontal'}) }}
  {% endif %}
    <fieldset>
      <legend>
        {% if news_item.is_new_record %}
          Create a new news_item
        {% else %}
          Edit news_item
        {% endif %}
      </legend>

      <div class="control-group">
        <label class="control-label" for="title">Article Title</label>
        <div class="controls">
          <input type="text" class="span6" name="title" value="{{ news_item.title }}" />
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="main_body">Main body</label>
        <div class="controls">
          <textarea class="span6 textarea-large tinymce" name="main_body">{% autoescape false %}{{ news_item.main_body }}{% endautoescape %}</textarea>
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="excerpt">Excerpt</label>
        <div class="controls">
          <textarea class="span6 textarea-small tinymce" name="excerpt">{% autoescape false %}{{ news_item.excerpt }}{% endautoescape %}</textarea>
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="meta_title">Meta title</label>
        <div class="controls">
          <input type="text" class="span6" name="meta_title" value="{{ news_item.meta_title }}" />
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="meta_description">Meta description</label>
        <div class="controls">
          <textarea class="span6 textarea-small" name="meta_description">{{ news_item.meta_description }}</textarea>
          <p class="help-block">SEO best Practice says the meta description should optimally be between 150-160 characters</p>
        </div>
      </div>

      <div class="form-actions">
        <input type="submit" class="btn btn-success btn-large" name="submit" value="Save Page" />
        <a href="/secure/news_items" class="btn">Cancel</a>
      </div>

    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
  {% include 'secure/news_items/sidebar.tpl' %}
{% endblock %}
