{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  {{ form_open('secure/news_items/delete/' ~ news_item.id,{'class': 'form-horizontal'}) }}
    <fieldset>
      <legend>
        Delete article
      </legend>

      <div class="alert alert-warning">
        <button class="close" data-dismiss="alert">x</button>
        Are you sure you want to delete the article: <strong>{{ news_item.title }}</strong>
        <br><br>
        Please note that there is NO UNDO facility available.
      </div>

      <div class="form-actions">
        <input type="submit" class="btn btn-danger" value="Delete" />
        <a href="{{ page_settings.admin_url }}news_items" class="btn">Cancel</a>
      </div>

    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
  {% include 'secure/news_items/sidebar.tpl' %}
{% endblock %}
