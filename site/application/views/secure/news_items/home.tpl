{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  <div class="hero-unit">
    <h1>News Articles</h1>
    <p>In this area you will find all the pages currently available on the website. This includes all public facing content, members only content and system pages.</p>
    <p>Listed below are the sections and the pages they contain. Clicking "View Pages" will show you all the pages currently listed in that section.</p>
    <p>Pages can be reordered by dargging the pages to a new position in the list</p>
  </div>

  <div class="row-fluid">
    <div class="span6">
      <div class="page-header">
        <h1>Latest Created</h1>
      </div>
      {% if recently_created %}
      <table class="table table-striped table-bordered table-condensed">
        <thead>
          <tr>
            <th style="width: 15%"></th>
            <th style="width: 55%">Article title</th>
            <th style="width: 15%">Author</th>
            <th style="width: 15%"></th>
          </tr>
        </thead>
        <tbody>
        {% for rc in recently_created %}
          <tr>
            <td>
              {% if rc.is_published %}
                <div class="btn-group">
                  <a class="btn btn-info btn-mini dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="icon-ok icon-white"></i> Published
                    <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a href="{{ page_settings.admin_url }}news_items/toggle_publish/{{ rc.id }}/0"><i class="icon-remove"></i> Unpublish</a></li>
                  </ul>
                </div>
              {% else %}
                  <a class="btn btn-success btn-mini" href="{{ page_settings.admin_url }}news_items/toggle_publish/{{ rc.id }}/1"><i class="icon-share icon-white"></i> Publish</a>
              {% endif %}
            </td>
            <td>{{ rc.title }}</td>
            <td>{{ rc.author }}</td>
            <td nowrap="nowrap">
              <a class="btn btn-mini btn-success" href="{{ page_settings.admin_url }}news_items/edit/{{ rc.id }}"><i class="icon-pencil icon-white"></i> Edit</a>
              <a class="btn btn-mini btn-danger" href="{{ page_settings.admin_url }}news_items/delete/{{ rc.id }}"><i class="icon-trash icon-white"></i> Delete</a>
            </td>
          </tr>
        {% endfor %}
        </tbody>
      </table>
      {% else %}
      <div class="alert alert-block alert-error">
        <h4 class="alert-heading">No articles have been created yet</h4>
        <p>When new articles are created they will appear in this list.</p>
      </div>
      {% endif %}
    </div>

    <div class="span6">
      <div class="page-header">
        <h1>Latest Published</h1>
      </div>
      {% if recently_published %}
      <table class="table table-striped table-bordered table-condensed">
        <thead>
          <tr>
            <th style="width: 15%"></th>
            <th style="width: 55%">Article title</th>
            <th style="width: 15%">Publisher</th>
            <th style="width: 15%"></th>
          </tr>
        </thead>
        <tbody>
        {% for rp in recently_published %}
          <tr>
            <td>
              {% if rp.is_published %}
                <div class="btn-group">
                  <a class="btn btn-info btn-mini dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="icon-ok icon-white"></i> Published
                    <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a href="{{ page_settings.admin_url }}news_items/toggle_publish/{{ rp.id }}/0"><i class="icon-remove"></i> Unpublish</a></li>
                  </ul>
                </div>
              {% else %}
                  <a class="btn btn-success btn-mini" href="{{ page_settings.admin_url }}news_items/toggle_publish/{{ rp.id }}/1"><i class="icon-share icon-white"></i> Publish</a>
              {% endif %}
            </td>
            <td>{{ rp.title }}</td>
            <td>{{ rp.published_by }}</td>
            <td nowrap="nowrap">
              <a class="btn btn-mini btn-success" href="{{ page_settings.admin_url }}news_items/edit/{{ rp.id }}"><i class="icon-pencil icon-white"></i> Edit</a>
              <a class="btn btn-mini btn-danger" href="{{ page_settings.admin_url }}news_items/delete/{{ rp.id }}"><i class="icon-trash icon-white"></i> Delete</a>
            </td>
          </tr>
        {% endfor %}
        </tbody>
      </table>
      {% else %}
      <div class="alert alert-block alert-error">
        <h4 class="alert-heading">No articles have been published yet</h4>
        <p>When new articles are published they will appear in this list.</p>
      </div>
      {% endif %}
    </div>
  </div>

  <div class="page-header">
    <h1>All Articles</h1>
  </div>
  {% if news_items %}
  <table class="table table-striped table-bordered table-condensed">
    <thead>
      <tr>
        <th style="width: 5%"></th>
        <th style="width: 55%">Article title</th>
        <th style="width: 10%">Created</th>
        <th style="width: 10%">Published on</th>
        <th style="width: 10%">Updated</th>
        <th style="width: 10%"></th>
      </tr>
    </thead>
    <tbody>
    {% for news_item in news_items %}
      <tr>
        <td>
          {% if news_item.is_published %}
            <div class="btn-group">
              <a class="btn btn-info btn-mini dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="icon-ok icon-white"></i> Published
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li><a href="{{ page_settings.admin_url }}news_items/toggle_publish/{{ news_item.id }}/0"><i class="icon-remove"></i> Unpublish</a></li>
              </ul>
            </div>
          {% else %}
              <a class="btn btn-success btn-mini" href="{{ page_settings.admin_url }}news_items/toggle_publish/{{ news_item.id }}/1"><i class="icon-share icon-white"></i> Publish</a>
          {% endif %}
        </td>
        <td>{{ news_item.title }}</td>
        <td rel="popover" data-placement="left" data-content="{{ news_item.created_at | date('jS \\o\\f F \\a\\t h:i:s') }}" data-original-title="Created at">{{ news_item.created_at | date("F jS") }}</td>
        <td {% if news_item.is_published %} rel="popover" data-placement="left" data-content="{{ news_item.published_at | date('jS \\o\\f F \\a\\t h:i:s') }}" data-original-title="Published at"{% endif %}>
          {% if news_item.is_published %}
            {{ news_item.published_at | date("F jS") }}
          {% endif %}
        </td>
        <td rel="popover" data-placement="left" data-content="{{ news_item.updated_at | date('jS \\o\\f F \\a\\t h:i:s') }}" data-original-title="Updated at">{{ news_item.updated_at | date("F jS") }}</td>
        <td nowrap="nowrap">
          <a class="btn btn-mini btn-success" href="{{ page_settings.admin_url }}news_items/edit/{{ news_item.id }}"><i class="icon-pencil icon-white"></i> Edit</a>
          <a class="btn btn-mini btn-danger" href="{{ page_settings.admin_url }}news_items/delete/{{ news_item.id }}"><i class="icon-trash icon-white"></i> Delete</a>
        </td>
      </tr>
    {% endfor %}
    </tbody>
  </table>
  {% else %}
  <div class="alert alert-block alert-error">
    <h4 class="alert-heading">No articles have been created yet</h4>
    <p>When new articles are craeted they will appear in this list.</p>
  </div>
  {% endif %}

{% endblock %}

{% block sidebar %}
  {% include 'secure/news_items/sidebar.tpl' %}
{% endblock %}
