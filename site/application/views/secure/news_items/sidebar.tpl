<div class="well" style="padding: 8px 0;">
  <ul class="nav nav-list">
    <li class="nav-header">News Articles</li>
    <li><a href="{{ page_settings.admin_url }}news_items"><i class="icon-home"></i> View all</a></li>
    <li><a href="{{ page_settings.admin_url }}news_items/edit"><i class="icon-plus"></i> New article</a></li>
  </ul>
</div>
