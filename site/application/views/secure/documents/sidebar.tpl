<div class="well" style="padding: 8px 0;">
  <ul class="nav nav-list">
    <li class="nav-header">Documents</li>
    <li><a href="{{ page_settings.admin_url }}documents"><i class="icon-home"></i> View all</a></li>
    <li><a href="{{ page_settings.admin_url }}documents/edit"><i class="icon-plus"></i> New document</a></li>
  </ul>
</div>
