{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  <div class="hero-unit">
    <h1>Documents</h1>
    <p>In this area you will find all the pages currently available on the website. This includes all public facing content, members only content and system pages.</p>
    <p>Listed below are the sections and the pages they contain. Clicking "View Pages" will show you all the pages currently listed in that section.</p>
    <p>Pages can be reordered by dargging the pages to a new position in the list</p>
  </div>

{% if documents %}

<table class="table table-striped table-bordered table-condensed">
  <thead>
    <th>Name</th>
    <th># Pages</th>
    <th></th>
  </thead>
  <tbody>
  {% for document in documents %}
    <tr>
      <td>{{ document.name }}</td>
      <td>{{ document.page_count }}</td>
      <td nowrap="nowrap">
        <a class="btn btn-mini" href="{{ document.admin_url }}"><i class="icon-search"></i> View</a>
        <a class="btn btn-mini btn-success" href="{{ page_settings.admin_url }}documents/edit/{{ document.id }}"><i class="icon-pencil icon-white"></i> Edit</a>
        {% if document.page_count == 0 %}
          <a class="btn btn-mini btn-danger" href="{{ page_settings.admin_url }}documents/delete/{{ document.id }}"><i class="icon-trash icon-white"></i> Delete</a>
        {% else %}
          <a class="btn btn-mini btn-danger disabled" href="{{ page_settings.admin_url }}documents/" rel="popover" data-placement="left" data-content="You can't delete this file as it is attached to at least one page. Editing the document will show you which pages currently have this document attached to it" data-original-title="You can't delete this file"><i class="icon-trash icon-white"></i> Delete</a>
        {% endif %}
      </td>
    </tr>
  {% endfor %}
  </tbody>
</table>

{% else %}

<div class="alert alert-block alert-error">
  <h4 class="alert-heading">No documents have been uploaded</h4>
  You can upload a new document by clicking the "New document" link in the navigation bar on the left
</div>
{% endif %}

{% endblock %}

{% block sidebar %}
  {% include 'secure/documents/sidebar.tpl' %}
{% endblock %}
