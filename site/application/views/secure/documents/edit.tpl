{% extends "secure/templates/logged_in.tpl" %}
{% block content %}

  {% if error %}
    <div class="alert alert-error">
      <button class="close" data-dismiss="alert">x</button>
      <p>
        There was a problem saving this document. Please check the following and try again.
      </p>
      <ul>
        {% for e in document.errors %}
          <li>{{ e }}</li>
        {% endfor %}
      </ul>
    </div>
  {% endif %}

  {% if document.is_new_record %}
    {{ form_open_multipart('secure/documents/edit',{'class': 'form-horizontal'}) }}
  {% else %}
    {{ form_open_multipart('secure/documents/edit/' ~ document.id,{'class': 'form-horizontal'}) }}
  {% endif %}
    <fieldset>
      <legend>
        {% if document.is_new_record %}
          Upload a new document
        {% else %}
          Edit document
        {% endif %}
      </legend>

      <div class="control-group">
        <label class="control-label" for="name">Name: </label>
        <div class="controls">
          <input type="text" class="" id="name" name="name" value="{{ document.name }}">
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="file">Upload File: </label>
        <div class="controls">
          <input type="file" name="file">
        </div>
      </div>

      {% if document.has_file %}
        <div class="alert alert-info">
          <p>
            Current file: <a href="{{ document.admin_url }}">{{ document.sanitized_filename }}</a>
          </p>
          <p>Leave 'Upload File' blank to keep the existing file</p>
        </div>
      {% endif %}

      {% if not document.is_new_record %}
        <div class="control-group">
          <label class="control-label" for="list">Attached To Pages: </label>
          <div class="controls">
            <ul>
              {% for page in document.myPages %}
                <li><a href="/secure/pages/edit/{{ page.id }}">{{ page.name }}</a></li>
              {% endfor %}
            </ul>
          </div>
        </div>
      {% endif %}

      <div class="form-actions">
        <input type="submit" class="btn btn-primary" value="Save" />
        <a href="{{ page_settings.admin_url }}documents" class="btn">Cancel</a>
      </div>

    </fieldset>
  </form>

{% endblock %}

{% block sidebar %}
  {% include 'secure/documents/sidebar.tpl' %}
{% endblock %}
