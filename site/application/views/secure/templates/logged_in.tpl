<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="cleartype" content="on">
  <meta http-equiv="imagetoolbar" content="false" />
  <meta name="viewport" content="width=1024">

  <title>{{ page_settings.site_name }}</title>

  <link rel="stylesheet" href="{{ page_settings.base_url }}_css/_cp/production.{{ page_settings.timestamp_css }}.css">
  <script src="{{ page_settings.base_url }}_js/libs/modernizr-2.5.3.min.js"></script>
  <link rel="canonical" href="" />
</head>

<body class="clearfix">
  <div class="navbar">
    <div class="navbar-inner">
      <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <a class="brand" href="{{ page_settings.admin_url }}"><img src="{{ page_settings.base_url }}_img/secure/logo.png" width="100" height="27" /></a>
        <div class="nav-collapse">
          <ul class="nav">
            <li><a href="{{ page_settings.admin_url }}">Dashboard</a></li>
            <li><a href="{{ page_settings.admin_url }}sections">Sections</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Content <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="{{ page_settings.admin_url }}pages">Pages</a></li>
                <li><a href="{{ page_settings.admin_url }}homepage_boxes">Homepage Boxes</a></li>
                <li><a href="{{ page_settings.admin_url }}news_items">News Articles</a></li>
                <li><a href="{{ page_settings.admin_url }}email_templates">Email Templates</a></li>
              </ul>
            </li>
            <li><a href="{{ page_settings.admin_url }}documents">Documents</a></li>
            <li class="divider-vertical"></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Deals <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="{{ page_settings.admin_url }}deal_sections">Deal Sections</a></li>
                <li><a href="{{ page_settings.admin_url }}deals">Deals</a></li>
              </ul>
            </li>
            <li class="divider-vertical"></li>
            <li><a href="{{ page_settings.admin_url }}members">Members</a></li>
            <li class="divider-vertical"></li>
            <li><a href="{{ page_settings.admin_url }}contact_form_submissions">Contact Form</a></li>
          </ul>
          <ul class="nav pull-right">
            <li><a href="{{ page_settings.admin_url }}settings">Site Settings</a></li>
            <li><a href="{{ page_settings.admin_url }}users">Admin Users</a></li>
            <li class="divider-vertical"></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ page_settings.current_user }} <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="/secure/users/profile">My Details</a></li>
                <li class="divider"></li>
                <li><a href="{{ page_settings.admin_url }}auth/logout">Log out</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.nav-collapse -->
      </div>
    </div><!-- /navbar-inner -->
  </div>

  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span2">
        {% block sidebar %}{% endblock %}
      </div>
      <div class="span10">
        {% if flash_error %}
          <div class="alert alert-error">
            <button class="close" data-dismiss="alert">x</button>
            {{ flash_error }}
          </div>
        {% endif %}
        {% if flash_notice %}
          <div class="alert alert-info">
            <button class="close" data-dismiss="alert">x</button>
            {{ flash_notice }}
          </div>
        {% endif %}
        {% if flash_warning %}
          <div class="alert">
            <button class="close" data-dismiss="alert">x</button>
            {{ flash_warning }}
          </div>
        {% endif %}
        {% if flash_success %}
          <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            {{ flash_success }}
          </div>
        {% endif %}
        {% block content %}{% endblock %}
      </div>
    </div>
  </div>

  <script src="{{ page_settings.base_url }}_js/_cp/script-ck.{{ page_settings.timestamp_js }}.js"></script>
  <script src="{{ page_settings.base_url }}_js/libs/tiny_mce/jquery.tinymce.js" type="text/javascript" charset="utf-8"></script>
  <script>
    $().ready(function() {
      $('textarea.tinymce').tinymce({

        script_url : '{{ page_settings.base_url }}/_js/libs/tiny_mce/tiny_mce.js',

        theme : "advanced",
        plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,template,spellchecker,imagemanager",

        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,formatselect,attribs,|,hr,|,charmap,spellchecker,media,|,fullscreen,code,preview",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image",
        theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        extended_valid_elements: 'img[style|src|class|alt|title|id]',

        document_base_url: "/",
        relative_urls: true

      });
    });

  </script>

</body>
</html>
