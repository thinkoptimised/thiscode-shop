<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="cleartype" content="on">
  <meta http-equiv="imagetoolbar" content="false" />
  <meta name="viewport" content="1024">

  <title>Tempo</title>

  <link rel="stylesheet" href="{{ page_settings.base_url }}_css/_cp/production.{{ page_settings.timestamp_css }}.css">
  <script src="{{ page_settings.base_url }}_js/libs/modernizr-2.5.3.min.js"></script>

</head>

<body class="clearfix">

  <div class="navbar">
    <div class="navbar-inner">
      <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <a class="brand" href="{{ page_settings.admin_url }}"><img src="{{ page_settings.base_url }}_img/secure/logo.png" width="100" height="27" /></a>
      </div>
    </div><!-- /navbar-inner -->
  </div>

  <div class="container-fluid">
    <div class="row-fluid">

      {% if flash_error %}
        <div class="alert alert-error">
          <button class="close" data-dismiss="alert">x</button>
          {{ flash_error }}
        </div>
      {% endif %}
      {% if flash_notice %}
        <div class="alert alert-info">
          <button class="close" data-dismiss="alert">x</button>
          {{ flash_notice }}
        </div>
      {% endif %}
      {% if flash_warning %}
        <div class="alert">
          <button class="close" data-dismiss="alert">x</button>
          {{ flash_warning }}
        </div>
      {% endif %}
      {% if flash_success %}
        <div class="alert alert-success">
          <button class="close" data-dismiss="alert">x</button>
          {{ flash_success }}
        </div>
      {% endif %}

      {% block content %}{% endblock %}
    </div>
  </div>

  <script src="{{ page_settings.base_url }}_js/_cp/script-ck.{{ page_settings.timestamp_js }}.js"></script>

</body>
</html>
