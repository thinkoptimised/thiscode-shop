<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Public_Controller extends MY_Controller {

  var $_login_redirect = '/account/login';

  public function __construct()
  {
    parent::__construct();
    $this->initPageSettings();
    $this->addCurrentUserToView();
  }



  protected function checkLogin()
  {
    if (!$this->isLoggedIn())
    {
      redirect($this->_login_redirect);
      die();
    }
  }
  protected function loginUser($member)
  {
    $this->session->set_userdata(array(
      'current_member_id' => $member->id,
      'member_logged_in' => TRUE
    ));
  }
  protected function logoutUser()
  {
		$this->session->unset_userdata(array('current_member_id'=>'','member_logged_in'=>''));
  }
  protected function currentUser()
  {
    try {
      return Member::find($this->session->userdata('current_member_id'));
    } catch(ActiveRecord\RecordNotFound $ex) {
      return new Member;
    }
  }
  protected function isLoggedIn()
  {
    return $this->session->userdata('member_logged_in') !== FALSE;
  }
  protected function addCurrentUserToView()
  {
    if($this->isLoggedIn())
    {
      $this->addTemplateData(array('current_user' => $this->currentUser()));
    }
  }


  private function initPageSettings()
  {
    $this->addTemplateData(array(
      'page_settings' => array(
        'timestamp_css' => get_css_timestamp(),
        'timestamp_js' => get_js_timestamp(),
        'base_url' => base_url(),
        'sections' => Site_section::allPublic(),
        'member_sections' => Site_section::allPrivate(),
        'footer_copyright' => Setting::get('footer_copyright'),
        'footer_address' => Setting::get('footer_address'),
        'footer_telephone' => Setting::get('footer_telephone'),
        'footer_email' => Setting::get('footer_email'),
        'site_name' => Setting::get('site_name')
      )
    ));
  }

}
