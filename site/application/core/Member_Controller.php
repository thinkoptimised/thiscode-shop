<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_Controller extends Public_Controller {

  var $_requires_login = true;

  public function __construct()
  {
    parent::__construct();
    $this->checkLogin();
  }

  protected function checkLogin() 
  {
    if(!$this->_requires_login) return;
    if (!$this->isLoggedIn())
    {
      redirect($this->_login_redirect);
      die();
    }
  }

}
