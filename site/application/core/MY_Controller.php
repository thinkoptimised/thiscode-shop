<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

  var $_flash_data = array();
  var $_template_data = array();

  public function __construct()
  {
    parent::__construct();
    $this->initFlash();
  }

  private function initFlash()
  {
    $this->_flash_data = array(
      'flash_error' => $this->session->flashdata('error'),
      'flash_notice' => $this->session->flashdata('notice'),
      'flash_warning' => $this->session->flashdata('warning'),
      'flash_success' => $this->session->flashdata('success')
    );
  }
  protected function setFlash($message,$type='notice')
  {
    $this->session->set_flashdata($type,$message);
  }

  protected function is_post() 
  {
    return strcmp(strtolower($_SERVER['REQUEST_METHOD']),'post') === 0;
  }

  protected function addTemplateData($data=array())
  {
    $this->_template_data = array_merge($this->_template_data,$data);
  }
  protected function render($template,$additional_data = array())
  {
    $template_data = array_merge($this->_template_data, $this->_flash_data,$additional_data);
    $this->twig->display($template,$template_data);
  }


  protected function redirectTo($url)
  {
    redirect($url);
    die();
  }

}

require(__DIR__.'/Public_Controller.php');
require(__DIR__.'/Member_Controller.php');
require(__DIR__.'/Admin_Controller.php');
