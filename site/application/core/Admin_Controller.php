<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Controller extends MY_Controller {

  var $_requires_login = true;
  var $_login_redirect = '/secure/auth';

  public function __construct()
  {
    parent::__construct();
    $this->checkLogin();
    $this->initPageSettings();
  }

  private function checkLogin()
  {
    if(!$this->_requires_login) return;
    if (!$this->isLoggedIn())
    {
      redirect($this->_login_redirect);
      die();
    }
  }
  protected function loginUser($admin_user)
  {
    $this->session->set_userdata(array(
      'current_user_id' => $admin_user->id,
      'current_user_name' => $admin_user->name,
      'logged_in' => TRUE
    ));
  }
  protected function logoutUser()
  {
		$this->session->unset_userdata(array('current_user_id'=>'','logged_in'=>''));
  }
  protected function currentUser()
  {
    try {
      return Admin_user::find($this->session->userdata('current_user_id'));
    } catch(ActiveRecord\RecordNotFound $ex) {
      return new Admin_user;
    }
  }
  protected function isLoggedIn()
  {
    return $this->session->userdata('logged_in') !== FALSE;
  }

  private function initPageSettings()
  {
    $this->addTemplateData(array(
      'page_settings' => array(
        'timestamp_css' => get_css_timestamp(),
        'timestamp_js' => get_js_timestamp(),
        'base_url' => base_url(),
        'admin_url' => config_item('admin_url'),
        'site_name' => Setting::get('site_name'),
        'current_user' => $this->session->userdata('current_user_name')
      )
    ));
  }

}
