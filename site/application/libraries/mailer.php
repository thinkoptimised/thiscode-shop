<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}

/**
 * @author Tom Cowan <tom@weareboomerang.com>
 * @since 10.08.2012 - 11:51:00
 * @version 1.0
 */

require_once APPPATH.'/third_party/phpmailer/class.phpmailer.php';

class Mailer {

    var $_ci;
    var $_phpmailer;

    var $_subject     = '';
    var $_to_name     = '';
    var $_to_email    = '';
    var $_from_name   = '';
    var $_from_email  = '';

    var $_template      = '';
    var $_template_data = array();

    public function __construct($config=array())
    {
      $this->_ci =& get_instance();
      $this->_ci->load->library('twig');
      $this->_ci->load->library('email');
      $this->_config = $config;
      $this->init();
    }

    private function init()
    {
      $this->_phpmailer = new PHPMailer();
      $this->_phpmailer->isHTML();
      $this->_phpmailer->isSMTP();

      $this->_phpmailer->Mailer = $this->_config['protocol'];
      $this->_phpmailer->SMTPAuth = $this->_config['smtp_auth'];
      $this->_phpmailer->Timeout = $this->_config['smtp_timeout'];
      
      $this->_phpmailer->Host = $this->_config['smtp_host'];
      $this->_phpmailer->Username = $this->_config['smtp_user'];
      $this->_phpmailer->Password = $this->_config['smtp_pass'];
      $this->_phpmailer->Port = $this->_config['smtp_port'];
    }

    public function subject($s) { $this->_subject = $s; return $this; }
    public function to_name($n) { $this->_to_name = $n; return $this; }
    public function to_email($e) { $this->_to_email = $e; return $this; }
    public function from_name($n) { $this->_from_name = $n; return $this; }
    public function from_email($e) { $this->_from_email = $e; return $this; }

    public function template($t) { $this->_template = $t; return $this; }
    public function template_data($d) { $this->_template_data = $d; return $this; }

    public function send()
    {
      $this->_phpmailer->ClearAddresses();

      $this->_phpmailer->From = $this->_from_email;
      $this->_phpmailer->FromName = $this->_from_name;
      $this->_phpmailer->AddAddress($this->_to_email,$this->_to_name);
      $this->_phpmailer->MsgHTML($this->getRenderedTemplate());
      $this->_phpmailer->Subject = $this->_subject;

      try {
        return $this->_phpmailer->Send();
      } catch(Exception $ex) {
        return false;
      }

    }

    private function getRenderedTemplate()
    {
      return $this->_ci->twig->render($this->_template,$this->_template_data);
    }

}
