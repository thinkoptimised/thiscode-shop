<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_css_timestamp() {
  return filemtime(config_item('css_path'));
}

function get_js_timestamp() {
  return filemtime(config_item('js_path'));
}