<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email_template extends BaseModel
{

  public static function findByShortName($shortname)
  {
    return self::find('first',array('conditions' => array('short_name=?',$shortname)));
  }


}
