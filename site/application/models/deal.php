<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deal extends BaseModel
{

  static $before_save = array(
    'generate_unique_slug'
  );

  public function generate_unique_slug()
  {
    if(strlen($this->slug) > 0) return;
    $this->makeUniqueSlug('name','slug','deal_section_id');
  }

  var $_cached_section = null;
  public function section()
  {
    if($this->_cached_section === null)
    {
      if($this->site_section_id > 0)
      {
        $this->_cached_section = Site_section::find($this->site_section_id);
      }
      else
      {
        $this->_cached_section = new Site_section;
      }
    }
    return $this->_cached_section;
  }
  public function members_only()
  {
    return $this->section()->members_only;
  }



  public function url()
  {
    if($this->site_section_id !== 0)
    {
      return '/'.$this->section()->slug.'/'.$this->slug;
    }
    return '/'.$this->slug;
  }


  public static function findBySection($section_id=0)
  {
    return self::find('all',array('conditions' => array('deal_section_id=?',$section_id), 'order' => 'position,name'));
  }

  public static function findBySlug($slug,$section_id)
  {
    return self::find('first',array('conditions' => array('slug=? AND deal_section_id=?',$slug,$section_id)));
  }


}
