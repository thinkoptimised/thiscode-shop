<?php

  class Setting extends BaseModel { 

    /***
     * Validations
     */
    public function validate()
    {
      if($this->email) $this->validateEmail('value','must be an email address');
      if($this->required && strlen($this->value) == 0) $this->errors->add('value','must not be blank');
    }


    /***
     * Finders
     */
    static $_cached_settings = null;
    public static function get($key) {
      if(self::$_cached_settings === null) {
        self::$_cached_settings = self::find('all');
      }
      foreach(self::$_cached_settings as $setting) {
        if($setting->short_name == $key) return $setting->value;
      }
      return '';
    }

  }
