<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends BaseModel {

  /*
   * Validations
   */
  static $validates_uniqueness_of = array(
    array('email', 'message' => 'must be unique. Another account already exists with this email address.')
  );
  static $validates_presence_of = array(
    array('name', 'message' => 'cannot be blank'),
    array('company', 'message' => 'cannot be blank'),
    array('email', 'message' => 'cannot be blank')
  );
  public function validate() 
  {
    $this->validatePassword();
    $this->validateEmailAddress();
  }
  protected function validatePassword() 
  {
    // Don't bother validating if no password was set by the 
    // controller (e.g. if password has not been entered, or user has 
    // just been created by an admin without a password.
    if($this->_password_set === true) {
      if($this->_plain_text_password != $this->_plain_text_password_confirmation) {
        $this->errors->add('password','must match');
      } 
      if(strlen($this->_plain_text_password) === 0) {
        $this->errors->add('password','must not be blank');
      }
    } 
  }
  protected function validateEmailAddress() 
  {
    $this->validateEmail('email');
  }



  /*
   * Activation Process
   */
  public function activate() 
  {
    $this->activation_token = '';
    $this->activated = true;
  }
  public static function FindByActivationToken($token) 
  {
    // Token contains the user_id, then a z character, then the reset token
    $items = explode('z',$token);
    if(count($items) != 2) return false;
    try {
      $user = self::find($items[0]);
      if($user->activation_token != $items[1]) return false;
      return $user;
    } catch(Exception $e) {
      return false;
    }
  }
  public function generateActivationToken() {
    $token = md5(time().$this->id.$this->email.rand());
    $this->activation_token = substr($token,0,8);
  }
  public function activation_url_token() {
    return "{$this->id}z{$this->activation_token}";
  }



  /*
   * Password Reset
   *
   * Process proceeds as follows:
   *  - User requests a password reset. This generates a token
   *    which is saved to the database. User is then emailed
   *    with a reset URL containing the token.
   *  - User clicks the link & then is able to create a new password.
   *  - Once new password is saved, reset token is cleaered
   */ 
  public function initiate_password_reset() {
    $this->generate_password_reset_token();
    return $this->save();
  }
  private function generate_password_reset_token() {
    $token = md5(time().$this->id.$this->email.rand());
    $this->reset_token = substr($token,0,8);
  }
  public function password_reset_url_token() {
    return "{$this->id}z{$this->reset_token}";
  }
  public function complete_password_reset() {
    $this->reset_token = '';
    $this->save();
  }
  public static function FindByResetToken($token) {
    // Token contains the user_id, then a z character, then the reset token
    $items = explode('z',$token);
    if(count($items) != 2) return false;
    try {
      $user = self::find($items[0]);
      if($user->reset_token != $items[1]) return false;
      return $user;
    } catch(Exception $e) {
      return false;
    }
  }

  /*
   * Handling setting a plain-text version of user password.
   * Password is always stored as a hash, so this is a virtual
   * attribute
   */
  var $_plain_text_password = '';
  var $_plain_text_password_confirmation = '';
  var $_password_set = false;
  public function set_plain_text_password($password) {
    $this->_password_set = true;
    $this->password = crypt($password);
    $this->_plain_text_password = $password;
  }
  public function set_plain_text_password_confirmation($password) {
    $this->_password_set = true;
    $this->_plain_text_password_confirmation = $password;
  }






  // Authenticates a user by email and password
  // If authentication fails, returns FALSE
  // On successful authentication, returns the user
  public static function authenticate($email,$password) 
  {
    $user = self::find('first',array(
      'conditions' => array('email=?',$email) 
    ));
    if($user) 
    {
      if(!$user->activated) return false;
      if(crypt($password, $user->password) === $user->password)
      {
        return $user;
      }
    } 
    return FALSE;
  }


  public static function findByEmail($email)
  {
    $results = self::find('first',array('conditions' => array('email=?',$email)));
    if(count($results) === 0) return false;
    return $results;
  }

  public static function findAllAwaitingApproval()
  {
    return self::find('all',array('conditions' => 'activated=0 AND approved=0', 'order' => 'created_at'));
  }


}
