<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Document extends BaseModel 
{

  static $has_many = array(
    array('document_pages', 'class_name' => 'Document_Page'),
    array('pages', 'through' => 'document_pages')
  );

  static $validates_presence_of = array(
    array('name')
  );

  public function page_count()
  {
    return count($this->document_pages);
  }
  public function myPages()
  {
    return $this->pages;
  }

  public function has_file()
  {
    return strlen($this->original_filename) > 0;
  }

  public function admin_url()
  {
    return '/secure/documents/view/'.$this->id;
  }
  public function public_url($page_id)
  {
    return '/downloads/'.$page_id.'/'.$this->id;
  }


  public function sanitized_filename()
  {
    return $this->stringToSlug($this->name).'.'.$this->filetype;
  }

  public function get_contents()
  {
    $filename = $this->base_path().$this->filename;
    return file_get_contents($filename);
  }
  private function base_path()
  {
    return dirname(BASEPATH).'/uploads/documents/';
  }


}
