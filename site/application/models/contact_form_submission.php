<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_form_submission extends BaseModel {

  static $validates_presence_of = array(
    'name', 'email', 'message'
  );

  public function validate()
  {
    $this->validateEmail('email','must be valid');
  }

  public static function findRecent($limit)
  {
    return self::find('all',array('order' => 'created_at desc', 'limit' => $limit));
  }

  public static function findAll()
  {
    return self::find('all',array('order' => 'created_at desc'));
  }

  public static function findIncomplete()
  {
    return self::find('all',array('order' => 'created_at asc', 'conditions' => 'is_actioned IS NULL'));
  }

}
