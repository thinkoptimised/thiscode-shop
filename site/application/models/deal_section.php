<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deal_section extends BaseModel
{

  static $has_many = array(
    array('deals', 'order' => 'position,name')
  );

  static $before_save = array(
    'generate_unique_slug'
  );


  public function generate_unique_slug()
  {
    $this->makeUniqueSlug('name','slug');
  }


  public function url()
  {
    return '/procurement/'.$this->slug;
  }

  public function myDeals()
  {
    return $this->deals;
  }
  public function deal_count()
  {
    return count($this->deals);
  }
  public function first_deal()
  {
    if($this->deal_count() > 0)
    {
      return $this->deals[0];
    }
    return false;
  }


  public static function allPublic()
  {
    return self::find('all',array('conditions' => array('members_only=0'), 'order' => 'position'));
  }
  public static function allPrivate()
  {
    return self::find('all',array('conditions' => array('members_only=1'), 'order' => 'position'));
  }

  public static function findBySlug($slug)
  {
    return self::find('first',array('conditions' => array('slug=?',$slug)));
  }
  public static function first()
  {
    return self::find('first',array('order' => 'position,name'));
  }

}
