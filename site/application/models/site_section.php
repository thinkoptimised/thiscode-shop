<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site_section extends BaseModel 
{

  static $has_many = array(
    array('pages', 'order' => 'position,name')
  );

  static $before_save = array(
    'generate_unique_slug'
  );


  public function generate_unique_slug()
  {
    $this->makeUniqueSlug('name','slug');
  }


  public function url()
  {
    if($this->members_only)
    {
      return '/members/'.$this->slug;
    } else return '/'.$this->slug;
  }

  public function myPages()
  {
    return $this->pages;
  }
  public function page_count()
  {
    return count($this->pages);
  }
  public function first_page()
  {
    if($this->page_count() > 0)
    {
      return $this->pages[0];
    }
    return false;
  }


  public static function allPublic()
  {
    return self::find('all',array('conditions' => array('members_only=0'), 'order' => 'position'));
  }
  public static function allPrivate()
  {
    return self::find('all',array('conditions' => array('members_only=1'), 'order' => 'position'));
  }

  public static function findBySlug($slug)
  {
    return self::find('first',array('conditions' => array('slug=?',$slug)));
  }
  public static function first()
  {
    return self::find('first',array('order' => 'position,name'));
  }

}
