<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BaseModel extends ActiveRecord\Model {

  /*
   * Validates an email address - 2 level test
   *  - Syntax
   *  - MX Record Check
   */
  protected function validateEmail($email_field,$error_message='Please enter a valid email address') {

    $email = $this->$email_field;

    $isValid = true;
    $atIndex = strrpos($email, "@");
    if (is_bool($atIndex) && !$atIndex) {
      $isValid = false;
    } else {
      $domain = substr($email, $atIndex+1);
      $local = substr($email, 0, $atIndex);
      $localLen = strlen($local);
      $domainLen = strlen($domain);
      if ($localLen < 1 || $localLen > 64) {
        // local part length exceeded
        $isValid = false;
      } else if ($domainLen < 1 || $domainLen > 255) {
        // domain part length exceeded
        $isValid = false;
      } else if ($local[0] == '.' || $local[$localLen-1] == '.') {
         // local part starts or ends with '.'
         $isValid = false;
      } else if (preg_match('/\\.\\./', $local)) {
        // local part has two consecutive dots
        $isValid = false;
      } else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
        // character not valid in domain part
        $isValid = false;
      } else if (preg_match('/\\.\\./', $domain)) {
        // domain part has two consecutive dots
        $isValid = false;
      } else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/',str_replace("\\\\","",$local))) {
        // character not valid in local part unless 
        // local part is quoted
        if (!preg_match('/^"(\\\\"|[^"])+"$/',str_replace("\\\\","",$local))) $isValid = false;
      }
      
      // Finally - Check the DNS is valid....
      if ($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A"))) {
        // domain not found in DNS
        $isValid = false;
      }
      
    }

    // If email not valid, add to the errors collection
    if($isValid === false) {
      $this->errors->add($email_field,$error_message); 
    }
    
  }



  /*
   * Generates a unique slug based on the source field.
   * Resulting slug is stored in the destination field
   *
   * If scope field is specified, then slug only needs be
   * unique within the scope of that field.
   */
  protected function makeUniqueSlug($source_field,$destination_field,$scope_field='') 
  {

    // Setup a base slug by converting the source field to a valid 
    // slug
    $base = $this->stringToSlug($this->$source_field);
    $current = $base;
    $counter = 1;

    // If a duplicate exists, then append a count to the end of the 
    // slug. In the style of:
    //
    //  a-post
    //  a-post-1
    //  a-post-2
    //
    if($this->is_new_record()) 
    {
      $conditions = array("");
    }
    else 
    {
      $conditions = array("id != ? ",$this->id);
    }
    if(strlen($conditions[0]) > 0) $conditions[0] = $conditions[0].' AND ';
    if(strlen($scope_field) > 0) 
    {
      $conditions[0] = $conditions[0].$scope_field.'=? ';
      $conditions[] = $this->$scope_field;
    }
    if(strlen($conditions[0]) > 0 && !preg_match('/AND $/',$conditions[0])) $conditions[0] = $conditions[0].' AND ';
    $conditions[0] .= "$destination_field=?";

    while(self::Count(array('conditions' => array_merge($conditions,array($current)))) > 0) 
    {
      $current = $base.'-'.$counter++; 
    }

    $this->$destination_field = $current;
  }

  /*
   * Turns a string into a URL slug
   *
   * http://debuggable.com/posts/title-to-url-slug-conversion
   */
  protected function stringToSlug($string)
  {
    $unPretty = array('/ä/', '/ö/', '/ü/', '/Ä/', '/Ö/', '/Ü/', '/ß/', '/\s?-\s?/', '/\s?_\s?/', '/\s?\/\s?/', '/\s?\\\s?/', '/\s/', '/"/', '/\'/','/[^\w-]/');
    $pretty   = array('ae', 'oe', 'ue', 'Ae', 'Oe', 'Ue', 'ss', '-', '-', '-', '-', '-', '', '','');
    return strtolower(preg_replace($unPretty, $pretty, $string));
  }  




  public function add_error($field,$message)
  {
    $this->errors->add($field,$message);
  }


}
