<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ga_visits extends BaseModel {

    public function get_visits()
    {
        if(Ga_visits::is_db_upto_date())
        {
            return Ga_visits::find('all');
        }
        else
        {
            Ga_visits::clear_table();
            $this->load->library('ga_api');

            $analytics = $this->ga_api->login()
                ->dimension(array('date'))
                ->metric(array('visitors', 'newVisits'))
                ->sort_by('date')
                ->limit(100)
                ->get_array();

            unset($analytics['summary']);

            Ga_visits::save_results($analytics);
        }
    }

    public function is_db_upto_date()
    {
        return ga_visits::find('all', array('conditions' => array('date = ?', date("Y-m-d", time() - 60 * 60 * 24))));
    }

    public function clear_table()
    {
        Ga_visits::query('delete from ga_visits');
    }

    public function save_results($analytics)
    {
        foreach($analytics as $key => $value)
        {
            $visits = new ga_visits();
            $visits->date = date("Y-m-d", strtotime($key));
            $date = date("Y-m-d", strtotime($key));
            if(!ga_visits::find('all', array('conditions' => array('date = ?', $date))))
            {
                foreach($value as $key => $value)
                {
                    if($key == 'newVisits')
                    {
                        $visits->newvisits = $value;
                    }
                    else
                    {
                        $visits->visitors = $value;
                    }
                }
                $visits->save();
            }
        }
    }
}