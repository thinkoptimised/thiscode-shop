<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class news_item extends BaseModel
{

  static $before_save = array(
    'generate_unique_slug', 'generate_meta'
  );


  public function generate_unique_slug()
  {
    if(strlen($this->slug) > 0) return;
    $this->makeUniqueSlug('title','slug');
  }

  public function generate_meta()
  {
    $this->generate_meta_title();
    $this->generate_meta_description();
    $this->generate_excerpt();
  }
  private function generate_meta_title()
  {
    if(strlen($this->meta_title) === 0)
    {
      $this->meta_title = $this->title;
    }
  }
  private function generate_meta_description()
  {
    //  - If blank, then use main body content, truncated to 155 chars
    //    - Strip HTML tags
    if(strlen($this->meta_description) === 0)
    {
      $this->meta_description = truncate(strip_tags($this->main_body),155,'',true,false);
    }
  }
  private function generate_excerpt()
  {
    //  - If blank, then use main body content, truncated to 155 chars
    //    - Strip HTML tags
    if(strlen($this->excerpt) === 0)
    {
      $this->excerpt = truncate($this->main_body,300,'',true,true);
    }
  }

  public static function findAll()
  {
    return self::find('all',array('order' => 'created_at desc'));
  }

  public static function findRecentlyCreated($limit)
  {
    return self::find('all',array('order' => 'created_at desc', 'limit' => $limit));
  }

  public static function findRecentlyPublished($limit)
  {
    return self::find('all',array('order' => 'published_at desc', 'limit' => $limit, 'conditions' => 'is_published = 1'));
  }

  public static function findBySlug($slug,$section_id)
  {
    return self::find('first',array('conditions' => array('slug=? AND site_section_id=?',$slug,$section_id)));
  }


}
