<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homepage_box extends BaseModel
{
  static $before_save = array(
    'delete_previous_image'
  );

  static $before_destroy = array(
    'cleanup_for_delete'
  );

  public function delete_image($imagefile=null)
  {
    unlink('./_img/uploads/homepage_boxes/'.$imagefile);
  }

  public function has_image($id=null)
  {
    $box = self::find($id);

    if($box->imagefile != null)
    {
      return true;
    }
  }

  public function delete_previous_image()
  {
    $box = self::find($this->id);

    if($this->imagefile != $box->imagefile && $this->has_image($this->id))
    {
      $this->delete_image($box->imagefile);
    }
  }

  public function cleanup_for_delete()
  {
    if($this->has_image($this->id))
    {
      $this->delete_image($this->imagefile);
    }
  }



  public static function findAllByPosition()
  {
    return self::find('all',array('order' => 'published desc, position'));
  }

  public static function findAllForFrontPage()
  {
    return self::find('all',array('conditions' => array('published=1'), 'order' => 'position', 'limit' => 4));
  }
}
