<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends BaseModel 
{

  static $has_many = array(
    array('documents', 'through' => 'document_pages'),
    array('document_pages', 'class_name' => 'Document_page')
  );

  static $before_save = array(
    'generate_unique_slug', 'generate_meta'
  );

  static $after_save = array(
    'save_document_ids'
  );

  static $before_destroy = array(
    'delete_document_links'
  );

  
  public function generate_unique_slug()
  {
    if(strlen($this->slug) > 0 && $this->secured) return;
    $this->makeUniqueSlug('name','slug','site_section_id');
  }

  public function generate_meta()
  {
    $this->generate_meta_title();
    $this->generate_meta_description();
  }
  private function generate_meta_title()
  {
    if(strlen($this->meta_title) === 0)
    {
      $this->meta_title = $this->title;
    }
  }
  private function generate_meta_description()
  {
    //  - If blank, then use main body content, truncated to 155 chars
    //    - Strip HTML tags
    if(strlen($this->meta_description) === 0)
    {
      $this->meta_description = truncate(strip_tags($this->main_body),155,'',true,false);
    }
  }


  var $_cached_section = null;
  public function section() 
  {
    if($this->_cached_section === null)
    {
      if($this->site_section_id > 0)
      {
        $this->_cached_section = Site_section::find($this->site_section_id);
      }
      else
      {
        $this->_cached_section = new Site_section;
      }
    }
    return $this->_cached_section;
  }
  public function members_only()
  {
    return $this->section()->members_only;
  }



  public function url()
  {
    if($this->site_section_id !== 0)
    {
      return '/'.$this->section()->slug.'/'.$this->slug;
    } 
    return '/'.$this->slug;
  }


  var $_document_ids = array();
  var $_document_ids_set = false;
  public function set_document_ids($document_ids)
  {
    $this->_document_ids = $document_ids;
    $this->_document_ids_set = true;
  }

  public function is_linked_to_document($document_id)
  {
    if($this->_document_ids_set)
    {
      foreach($this->_document_ids as $d_id)
      {
        if((int)$d_id === $document_id) return true;
      }
    }
    else
    {
      if(is_array($this->document_pages)) 
      {
        foreach($this->document_pages as $dp)
        {
          if($dp->document_id === $document_id) return true;
        } 
      }
    }
    return false;
  }
  public function save_document_ids()
  {
    if($this->_document_ids_set)
    {
      $this->delete_document_links();
      foreach($this->_document_ids as $d_id)
      {
        $document_page = new Document_page;
        $document_page->document_id = $d_id;
        $document_page->page_id = $this->id;
        $document_page->save();
      }
    }
  }
  public function delete_document_links()
  {
    foreach($this->document_pages as $dp) 
    {
      $dp->delete();
    }
  }


  public static function findBySection($section_id=0)
  {
    return self::find('all',array('conditions' => array('site_section_id=?',$section_id), 'order' => 'position,name'));
  }

  public static function findBySlug($slug,$section_id)
  {
    return self::find('first',array('conditions' => array('slug=? AND site_section_id=?',$slug,$section_id)));
  }


}
