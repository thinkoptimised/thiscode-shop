# -*- mode: ruby -*-
# vi: set ft=ruby :
project_dir = File.basename(Dir.getwd)
project_name = project_dir.gsub('-','_')

Vagrant::Config.run do |config|
  config.vm.box = "Ubuntu 12.04 64bit"
  config.vm.box_url = "http://files.vagrantup.com/precise64.box"

  config.vm.network :hostonly, "33.33.33.10"

  config.vm.forward_port 80, 8080

  config.vm.share_folder("site", "/htdocs/site/", "site/", :nfs => true)
  config.vm.share_folder("database", "/assets", "assets/", :nfs => true)
 
  config.vm.provision :chef_solo do |chef|
    chef.cookbooks_path = ["_builds/cookbooks/"]
    chef.json.merge!({
      :project_name => "#{project_name}",
      :project_dir => "#{project_dir}",
      :mysql => {
        :server_root_password => 'root',
        :bind_address => '0.0.0.0',
        :mysql_connection_info => {
          :host => "localhost", 
          :username => 'root', 
          :password => "node[:mysql][:server_root_password]"
        },
        :tunable => {
          :key_buffer => "16K",
          :max_allowed_packet => "1M",
          :table_cache => "4",
          :thread_stack => "64K",
          :query_cache_size => "8M",
          :innodb_buffer_pool_size => "1M"
        }
      },
      :apache => {
        :listen_ports => ['80'],
        :prefork => {
          :startservers => '1',
          :minspareservers => '1',
          :maxspareservers => '5',
          :serverlimit => '50',
          :maxclients => '50',
          :maxrequestsperchild => '5000'
        }
      }
    })
    chef.add_recipe "__local_env"
  end
end