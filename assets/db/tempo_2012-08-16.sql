# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 33.33.33.10 (MySQL 5.5.24-0ubuntu0.12.04.1-log)
# Database: tempo
# Generation Time: 2012-08-16 16:26:28 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _ci_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_ci_sessions`;

CREATE TABLE `_ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL DEFAULT '',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `_ci_sessions` WRITE;
/*!40000 ALTER TABLE `_ci_sessions` DISABLE KEYS */;

INSERT INTO `_ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`)
VALUES
	('92d38d8c6081485a309c315747690388','10.0.2.2','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.79 Safari/537.1',1345134026,'a:5:{s:9:\"user_data\";s:0:\"\";s:15:\"current_user_id\";i:1;s:9:\"logged_in\";b:1;s:17:\"current_member_id\";i:1;s:16:\"member_logged_in\";b:1;}');

/*!40000 ALTER TABLE `_ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table admin_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_users`;

CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `activated` tinyint(4) DEFAULT '0',
  `name` varchar(255) DEFAULT '',
  `activation_token` varchar(8) DEFAULT '',
  `activation_token_created` datetime DEFAULT NULL,
  `reset_token` varchar(8) DEFAULT '',
  `reset_token_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;

INSERT INTO `admin_users` (`id`, `email`, `password`, `activated`, `name`, `activation_token`, `activation_token_created`, `reset_token`, `reset_token_created`)
VALUES
	(1,'tom@cobuso.co.uk','$6$V6uX9eWz$73ZSnVdvD/199OkvxqYBgyHXMWP.4TtallV0Z4EoqtOOAYtW19ZbZIgXY3JqIABQ1JsJLosFqma6DLUd7wN/M0',1,'Tom Cowan','',NULL,'',NULL);

/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table contact_form_submissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contact_form_submissions`;

CREATE TABLE `contact_form_submissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `message` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table document_pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `document_pages`;

CREATE TABLE `document_pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `document_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table documents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `documents`;

CREATE TABLE `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `original_filename` varchar(255) DEFAULT NULL,
  `filetype` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table members
# ------------------------------------------------------------

DROP TABLE IF EXISTS `members`;

CREATE TABLE `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `approved` tinyint(4) DEFAULT '0',
  `approved_by` varchar(255) DEFAULT NULL,
  `approved_at` datetime DEFAULT NULL,
  `activated` tinyint(4) DEFAULT '0',
  `activated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `reset_token` varchar(255) DEFAULT NULL,
  `activation_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `members` WRITE;
/*!40000 ALTER TABLE `members` DISABLE KEYS */;

INSERT INTO `members` (`id`, `name`, `company`, `email`, `password`, `approved`, `approved_by`, `approved_at`, `activated`, `activated_at`, `created_at`, `updated_at`, `reset_token`, `activation_token`)
VALUES
	(1,'Tom Cowan','Boomerang','tom@cobuso.co.uk','$6$scq9IYdP$ANexEnRgv08Cj3kMyvjy28bl9PVulSR7e/e892c/nt/ZxoYz7iTY7KpP3D.IM14dmi2peIVAYeyuHemrObUbH0',1,NULL,NULL,1,NULL,'2012-08-16 11:01:21','2012-08-16 16:21:50','','b28ec813'),
	(2,'Tom Cowan','Boomerang Stuff','tom@weareboomerang.com','$6$B4gl3gpm$Ai5Z1bmr3yRE9BhR2SzpsVTa45zlaNlDx0zLlhZYzmtcj6dW1xdgttXAA2XXr1gyWKOnZDn8nb3tEfLeUM.vK/',1,NULL,NULL,1,NULL,'2012-08-16 16:15:27','2012-08-16 16:15:43',NULL,'');

/*!40000 ALTER TABLE `members` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_section_id` int(11) DEFAULT '0',
  `slug` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text,
  `main_body` text,
  `edited_by` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  `secured` tinyint(4) DEFAULT '0',
  `contact_form` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `site_section_id`, `slug`, `name`, `title`, `meta_title`, `meta_description`, `main_body`, `edited_by`, `created_at`, `updated_at`, `position`, `secured`, `contact_form`)
VALUES
	(1,16,'contact-us','Contact','Contact Us','Contact Us','This is the contact page...','<p>This is the contact page...</p>',0,'2012-08-15 15:08:40','2012-08-15 15:13:32',1,0,1),
	(2,9,'the-voice','The Voice','This is a page','This is a page','Heloo there...','<p>Heloo there...</p>',0,'2012-08-15 15:14:36','2012-08-16 16:03:29',2,0,0),
	(3,9,'scaary','I hear voices','Scaary','Scaary','Dunno what to do....\nIt\'s fricking scary','<p>Dunno what to do....</p>\n<p>It\'s fricking scary</p>',0,'2012-08-15 15:47:54','2012-08-15 15:48:17',3,0,0),
	(4,0,'terms-and-conditions','Terms and Conditions','Terms and Conditions','Terms and Conditions','Booring&nbsp;','<p>Booring&nbsp;</p>',0,'2012-08-15 15:57:20','2012-08-15 16:01:27',1,1,0),
	(5,0,'privacy-policy','Privacy Policy','Privacy Policy','Privacy Policy','This is our privacy policy page...','<p>This is our privacy policy page...</p>',0,'2012-08-15 16:09:25','2012-08-15 16:09:25',1,1,0),
	(6,0,'page-not-found','404','Page Not Found','Page Not Found','Sorry, we couldn\'t find the page you were looking for...','<p>Sorry, we couldn\'t find the page you were looking for...</p>',0,'2012-08-15 16:15:10','2012-08-15 16:15:10',1,1,0),
	(7,0,'homepage','Homepage','Raising Standards','Raising Standards','For a moment perhaps I stood there, breast-high in the almost boiling water, dumbfounded at my position, hopeless of escape. Through the reek I could see t','<p>For a moment perhaps I stood there, breast-high in the almost boiling water, dumbfounded at my position, hopeless of escape. Through the reek I could see the people who had been with me in the river scrambling out of the water through the reeds, like little frogs hurrying through grass from the advance of a man, or running to and fro in utter dismay on the towing path.</p>\n<p><a title=\"\" href=\"#\">Find out more &raquo;</a></p>',0,'2012-08-15 16:27:25','2012-08-15 16:30:30',1,1,0),
	(8,0,'thank-you-for-registering','Thank You For Registering','Thanks for registering your interest','Thank you for registering an interest in Tempo','Many thanks for registering your interest. You will recieve an invite email if your account is approved.','<p>Many thanks for registering your interest. You will recieve an invite email if your account is approved.</p>',0,'2012-08-16 09:08:43','2012-08-16 09:08:43',1,1,0),
	(9,0,'member-register','Member Register','Register your interest','Register your interest','Fill in the form below to register your interest in Tempo.','<p>Fill in the form below to register your interest in Tempo.</p>',0,'2012-08-16 09:15:54','2012-08-16 09:15:54',1,1,0),
	(10,0,'account-activation','Account Activation','Activate Your Account','Activate Your Tempo Account','Please fill in your details below to activate your Tempo account.','<p>Please fill in your details below to activate your Tempo account.</p>',0,'2012-08-16 10:53:14','2012-08-16 10:53:14',1,1,0),
	(11,0,'login','Login','Member Login','Member Login','Please use the form below to login to your Tempo account.','<p>Please use the form below to login to your Tempo account.</p>',0,'2012-08-16 11:07:04','2012-08-16 13:43:15',1,1,0),
	(12,21,'dashboard','Welcome','Welcome to Tempo','Welcome to Tempo','Welcome to the exclusive members-only section','<p>Welcome to the exclusive members-only section</p>',0,'2012-08-16 11:23:16','2012-08-16 15:54:45',1,1,0),
	(13,17,'training','Training','Training','Training','This is the training page','<p>This is the training page</p>',0,'2012-08-16 11:44:27','2012-08-16 11:44:27',1,0,0),
	(14,0,'member-forgotten-password','Member Forgotten Password Request','Forgotten Your Password?','Forgotten Password','','<p>Enter your email address below and we\'ll send you instructions on how to reset your password.</p>',0,'2012-08-16 09:15:54','2012-08-16 09:15:54',1,1,0),
	(15,0,'member-reset-password','Member Reset Password','Reset Your Password','Reset Your Password','','<p>Please enter your new password below and we\'ll reset it for you.</p>',0,'2012-08-16 09:15:54','2012-08-16 09:15:54',1,1,0);

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `short_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `value` text,
  `required` tinyint(4) DEFAULT '0',
  `email` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`id`, `category`, `short_name`, `name`, `description`, `value`, `required`, `email`)
VALUES
	(1,'Front End - Footer','footer_email','Footer Email Address','This is the email address shown on the footer of the website','tempo@tempouk.org',1,1),
	(2,'Front End - Footer','footer_copyright','Footer Copyright','Copyright message shown in footer','&copy; Tempo 2012',1,0),
	(3,'Front End - Footer','footer_address','Footer Address','Address shown in the footer','Somewhere, Street name, Knutsford, WA16 8RB',1,0),
	(4,'Front End - Footer','footer_telephone','Footer Telephone','Telephone number shown in footer','Tel: 01565 000 000',0,0),
	(5,'Admin Panel','admin_user_invite_email_from_email','Admin User Invite - From Email','The \'From\' email address used when inviting users to create an account on the admin panel','tempo@tempouk.org',1,1),
	(6,'Admin Panel','admin_user_invite_email_from_name','Admin User Invite - From Name','The \'From\' name used when inviting users to create an account on the admin panel','Tempo',1,0),
	(7,'Members','member_invite_email_from_email','Member Invite - From Email','The \'From\' email used when inviting members to activate their account','tempo@tempouk.org',1,1),
	(8,'Members','member_invite_email_from_name','Member Invite - From Name','The \'From\' name used when inviting members to activate their account','Tempo',1,0),
	(9,'Administrator','admin_email','Adminstrator Notification Email','The email address where administrator notifications are sent','cward@depoel.co.uk',1,1),
	(10,'Administrator','admin_name','Adminstrator Notification Name','The name of the person to whom administrator notifications are sent','Tempo Administrator',1,0),
	(11,'Contact Form','contact_form_email','Contact Form Notification Email','The email address where contact form notifications are sent ','cward@depoel.co.uk',1,1),
	(12,'Contact Form','contact_form_name','Contact Form Notification Name','The name of the person to whom contact form notifications are sent','Tempo Administrator',1,0);

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table site_sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `site_sections`;

CREATE TABLE `site_sections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `members_only` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  `slug` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `site_sections` WRITE;
/*!40000 ALTER TABLE `site_sections` DISABLE KEYS */;

INSERT INTO `site_sections` (`id`, `name`, `members_only`, `created_at`, `updated_at`, `position`, `slug`)
VALUES
	(9,'Industry Voice',0,'2012-08-15 15:02:19','2012-08-15 15:18:47',1,'industry-voice'),
	(10,'Standards / Auditing',0,'2012-08-15 15:02:30','2012-08-15 15:18:58',2,'standards-auditing'),
	(11,'Procurement',0,'2012-08-15 15:02:45','2012-08-15 15:19:00',3,'procurement'),
	(12,'Legal Support',0,'2012-08-15 15:02:53','2012-08-15 15:19:03',4,'legal-support'),
	(13,'Training',0,'2012-08-15 15:02:56','2012-08-15 15:19:05',5,'training'),
	(14,'Membership',0,'2012-08-15 15:02:59','2012-08-15 15:19:08',6,'membership'),
	(15,'About Us',0,'2012-08-15 15:03:06','2012-08-15 15:19:10',7,'about-us'),
	(16,'Contact',0,'2012-08-15 15:03:10','2012-08-15 15:19:12',8,'contact'),
	(17,'Training',1,'2012-08-16 11:19:11','2012-08-16 11:21:43',2,'training-1'),
	(18,'Legal Resources',1,'2012-08-16 11:19:19','2012-08-16 11:21:48',3,'legal-resources'),
	(19,'Procurement',1,'2012-08-16 11:19:26','2012-08-16 11:21:58',5,'procurement-1'),
	(20,'Finance',1,'2012-08-16 11:19:33','2012-08-16 11:19:33',4,'finance'),
	(21,'Dashboard',1,'2012-08-16 11:21:39','2012-08-16 11:23:53',1,'dashboard');

/*!40000 ALTER TABLE `site_sections` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
