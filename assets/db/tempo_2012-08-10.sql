# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 33.33.33.10 (MySQL 5.5.24-0ubuntu0.12.04.1-log)
# Database: tempo
# Generation Time: 2012-08-10 15:26:38 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _ci_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_ci_sessions`;

CREATE TABLE `_ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL DEFAULT '',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `_ci_sessions` WRITE;
/*!40000 ALTER TABLE `_ci_sessions` DISABLE KEYS */;

INSERT INTO `_ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`)
VALUES
	('9c021deeffa22938ba94f489d4ccfd76','10.0.2.2','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.75 Safari/537.1',1344612121,'a:3:{s:9:\"user_data\";s:0:\"\";s:15:\"current_user_id\";i:1;s:9:\"logged_in\";b:1;}');

/*!40000 ALTER TABLE `_ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table admin_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_groups`;

CREATE TABLE `admin_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table admin_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_users`;

CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `activated` tinyint(4) DEFAULT '0',
  `name` varchar(255) DEFAULT '',
  `activation_token` varchar(8) DEFAULT '',
  `activation_token_created` datetime DEFAULT NULL,
  `reset_token` varchar(8) DEFAULT '',
  `reset_token_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;

INSERT INTO `admin_users` (`id`, `email`, `password`, `activated`, `name`, `activation_token`, `activation_token_created`, `reset_token`, `reset_token_created`)
VALUES
	(1,'tom@cobuso.co.uk','$6$V6uX9eWz$73ZSnVdvD/199OkvxqYBgyHXMWP.4TtallV0Z4EoqtOOAYtW19ZbZIgXY3JqIABQ1JsJLosFqma6DLUd7wN/M0',1,'Tom Cowan','',NULL,'',NULL);

/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table admin_users_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_users_groups`;

CREATE TABLE `admin_users_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table contact
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contact`;

CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table documents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `documents`;

CREATE TABLE `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) DEFAULT NULL,
  `original_filename` varchar(255) DEFAULT NULL,
  `filetype` varchar(10) DEFAULT NULL,
  `content_type` varchar(50) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` text,
  `meta_description` text,
  `main_body` text,
  `template` varchar(100) DEFAULT NULL,
  `body_class` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `og_title` varchar(255) DEFAULT NULL,
  `og_description` text,
  `og_image` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `section` tinyint(4) DEFAULT NULL,
  `edited_by` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `slug`, `name`, `title`, `meta_title`, `meta_keywords`, `meta_description`, `main_body`, `template`, `body_class`, `filename`, `og_title`, `og_description`, `og_image`, `updated_at`, `created_at`, `section`, `edited_by`)
VALUES
	(1,'homepage','Homepage','Homepage PART 2','Homepage','this,is, the, homepage','Other poets have warbled the praises of the soft eye of the antelope, and the lovely plumage of the bird that never alights; less celestial, I celebrate a tail.','&lt;p&gt;Other poets have warbled the praises of the soft eye of the antelope, and the lovely plumage of the bird that never alights; less celestial, I celebrate a tail.&lt;/p&gt;\n\n&lt;p&gt;Reckoning the largest sized Sperm Whale\'s tail to begin at that point of the trunk where it tapers to about the girth of a man, it comprises upon its upper surface alone, an area of at least fifty square feet. The compact round body of its root expands into two broad, firm, flat palms or flukes, gradually shoaling away to less than an inch in thickness. At the crotch or junction, these flukes slightly overlap, then sideways recede from each other like wings, leaving a wide vacancy between. In no living thing are the lines of beauty more exquisitely defined than in the crescentic borders of these flukes. At its utmost expansion in the full grown whale, the tail will considerably exceed twenty feet across.&lt;/p&gt;\n\n&lt;p&gt;The entire member seems a dense webbed bed of welded sinews; but cut into it, and you find that three distinct strata compose it:â€”upper, middle, and lower. The fibres in the upper and lower layers, are long and horizontal; those of the middle one, very short, and running crosswise between the outside layers. This triune structure, as much as anything else, imparts power to the tail. To the student of old Roman walls, the middle layer will furnish a curious parallel to the thin course of tiles always alternating with the stone in those wonderful relics of the antique, and which undoubtedly contribute so much to the great strength of the masonry.&lt;/p&gt;\n\n&lt;p&gt;But as if this vast local power in the tendinous tail were not enough, the whole bulk of the leviathan is knit over with a warp and woof of muscular fibres and filaments, which passing on either side the loins and running down into the flukes, insensibly blend with them, and largely contribute to their might; so that in the tail the confluent measureless force of the whole whale seems concentrated to a point. Could annihilation occur to matter, this were the thing to do it.&lt;/p&gt;\n\n&lt;p&gt;Nor does thisâ€”its amazing strength, at all tend to cripple the graceful flexion of its motions; where infantileness of ease undulates through a Titanism of power. On the contrary, those motions derive their most appalling beauty from it. Real strength never impairs beauty or harmony, but it often bestows it; and in everything imposingly beautiful, strength has much to do with the magic. Take away the tied tendons that all over seem bursting from the marble in the carved Hercules, and its charm would be gone. As devout Eckerman lifted the linen sheet from the naked corpse of Goethe, he was overwhelmed with the massive chest of the man, that seemed as a Roman triumphal arch. When Angelo paints even God the Father in human form, mark what robustness is there. And whatever they may reveal of the divine love in the Son, the soft, curled, hermaphroditical Italian pictures, in which his idea has been most successfully embodied; these pictures, so destitute as they are of all brawniness, hint nothing of any power, but the mere negative, feminine one of submission and endurance, which on all hands it is conceded, form the peculiar practical virtues of his teachings.&lt;/p&gt;\n\n&lt;p&gt;Such is the subtle elasticity of the organ I treat of, that whether wielded in sport, or in earnest, or in anger, whatever be the mood it be in, its flexions are invariably marked by exceeding grace. Therein no fairy\'s arm can transcend it.&lt;/p&gt;\n\n&lt;p&gt;Five great motions are peculiar to it. First, when used as a fin for progression; Second, when used as a mace in battle; Third, in sweeping; Fourth, in lobtailing; Fifth, in peaking flukes.&lt;/p&gt;\n\n&lt;p&gt;First: Being horizontal in its position, the Leviathan\'s tail acts in a different manner from the tails of all other sea creatures. It never wriggles. In man or fish, wriggling is a sign of inferiority. To the whale, his tail is the sole means of propulsion. Scroll-wise coiled forwards beneath the body, and then rapidly sprung backwards, it is this which gives that singular darting, leaping motion to the monster when furiously swimming. His side-fins only serve to steer by.&lt;/p&gt;\n\n&lt;p&gt;Second: It is a little significant, that while one sperm whale only fights another sperm whale with his head and jaw, nevertheless, in his conflicts with man, he chiefly and contemptuously uses his tail. In striking at a boat, he swiftly curves away his flukes from it, and the blow is only inflicted by the recoil. If it be made in the unobstructed air, especially if it descend to its mark, the stroke is then simply irresistible. No ribs of man or boat can withstand it. Your only salvation lies in eluding it; but if it comes sideways through the opposing water, then partly owing to the light buoyancy of the whale boat, and the elasticity of its materials, a cracked rib or a dashed plank or two, a sort of stitch in the side, is generally the most serious result. These submerged side blows are so often received in the fishery, that they are accounted mere child\'s play. Some one strips off a frock, and the hole is stopped.&lt;/p&gt;','homepage','homepage',NULL,'Open Graph Title','Then suddenly he noticed with a start that some of the grey clinker, the ashy incrustation that covered the meteorite, was falling off the circular edge of the end.  It was dropping off in flakes and raining down upon the sand.  A large piece suddenly came off and fell with a sharp noise that brought his heart into his mouth.','','2010-07-30 09:19:20','2010-07-30 09:00:00',2,1),
	(18,'This-is-a-test',NULL,'This is a test','','','','test',NULL,NULL,NULL,'','','','2012-07-30 15:10:11','2012-07-30 15:06:25',0,2),
	(19,'this-is-my-test',NULL,'This is another bloody test','This is another bloody test','','&lt;p&gt;&quot;De balena vero sufficit, si rex habeat caput, et regina caudam.&quot; BRACTON, L. 3, C. 3.&lt;/p&gt; &lt;p&gt;Latin from the books of the Laws of&#8230;','&lt;p&gt;&quot;De balena vero sufficit, si rex habeat caput, et regina caudam.&quot; BRACTON, L. 3, C. 3.&lt;/p&gt;\n\n&lt;p&gt;Latin from the books of the Laws of England, which taken along with the context, means, that of all whales captured by anybody on the coast of that land, the King, as Honourary Grand Harpooneer, must have the head, and the Queen be respectfully presented with the tail. A division which, in the whale, is much like halving an apple; there is no intermediate remainder. Now as this law, under a modified form, is to this day in force in England; and as it offers in various respects a strange anomaly touching the general law of Fast and Loose-Fish, it is here treated of in a separate chapter, on the same courteous principle that prompts the English railways to be at the expense of a separate car, specially reserved for the accommodation of royalty. In the first place, in curious proof of the fact that the above-mentioned law is still in force, I proceed to lay before you a circumstance that happened within the last two years.&lt;/p&gt;\n\n&lt;p&gt;It seems that some honest mariners of Dover, or Sandwich, or some one of the Cinque Ports, had after a hard chase succeeded in killing and beaching a fine whale which they had originally descried afar off from the shore. Now the Cinque Ports are partially or somehow under the jurisdiction of a sort of policeman or beadle, called a Lord Warden. Holding the office directly from the crown, I believe, all the royal emoluments incident to the Cinque Port territories become by assignment his. By some writers this office is called a sinecure. But not so. Because the Lord Warden is busily employed at times in fobbing his perquisites; which are his chiefly by virtue of that same fobbing of them.&lt;/p&gt;\n\n&lt;p&gt;Now when these poor sun-burnt mariners, bare-footed, and with their trowsers rolled high up on their eely legs, had wearily hauled their fat fish high and dry, promising themselves a good L150 from the precious oil and bone; and in fantasy sipping rare tea with their wives, and good ale with their cronies, upon the strength of their respective shares; up steps a very learned and most Christian and charitable gentleman, with a copy of Blackstone under his arm; and laying it upon the whale\'s head, he says&amp;mdash;&quot;Hands off! this fish, my masters, is a Fast-Fish. I seize it as the Lord Warden\'s.&quot; Upon this the poor mariners in their respectful consternation&amp;mdash;so truly English&amp;mdash;knowing not what to say, fall to vigorously scratching their heads all round; meanwhile ruefully glancing from the whale to the stranger. But that did in nowise mend the matter, or at all soften the hard heart of the learned gentleman with the copy of Blackstone. At length one of them, after long scratching about for his ideas, made bold to speak,&lt;/p&gt;\n\n&lt;p&gt;&quot;Please, sir, who is the Lord Warden?&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;The Duke.&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;But the duke had nothing to do with taking this fish?&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;It is his.&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;We have been at great trouble, and peril, and some expense, and is all that to go to the Duke\'s benefit; we getting nothing at all for our pains but our blisters?&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;It is his.&quot;&lt;/p&gt;',NULL,NULL,'benedmunds-CodeIgniter-Ion-Auth-de1bccc.zip','This is another bloody test','&lt;p&gt;&quot;De balena vero sufficit, si rex habeat caput, et regina caudam.&quot; BRACTON, L. 3, C. 3.&lt;/p&gt; &lt;p&gt;Latin from the books of the Laws of&#8230;','benedmunds-CodeIgniter-Ion-Auth-de1bccc.zip','2012-07-30 15:26:30','2012-07-30 15:10:28',3,3),
	(20,'And-another-test-the-last',NULL,'And another test, the last?','And another test, the last?','','&lt;p&gt;&quot;De balena vero sufficit, si rex habeat caput, et regina caudam.&quot; BRACTON, L. 3, C. 3.&lt;/p&gt; &lt;p&gt;Latin from the books of the Laws of&#8230;','&lt;p&gt;&quot;De balena vero sufficit, si rex habeat caput, et regina caudam.&quot; BRACTON, L. 3, C. 3.&lt;/p&gt;\n\n&lt;p&gt;Latin from the books of the Laws of England, which taken along with the context, means, that of all whales captured by anybody on the coast of that land, the King, as Honourary Grand Harpooneer, must have the head, and the Queen be respectfully presented with the tail. A division which, in the whale, is much like halving an apple; there is no intermediate remainder. Now as this law, under a modified form, is to this day in force in England; and as it offers in various respects a strange anomaly touching the general law of Fast and Loose-Fish, it is here treated of in a separate chapter, on the same courteous principle that prompts the English railways to be at the expense of a separate car, specially reserved for the accommodation of royalty. In the first place, in curious proof of the fact that the above-mentioned law is still in force, I proceed to lay before you a circumstance that happened within the last two years.&lt;/p&gt;\n\n&lt;p&gt;It seems that some honest mariners of Dover, or Sandwich, or some one of the Cinque Ports, had after a hard chase succeeded in killing and beaching a fine whale which they had originally descried afar off from the shore. Now the Cinque Ports are partially or somehow under the jurisdiction of a sort of policeman or beadle, called a Lord Warden. Holding the office directly from the crown, I believe, all the royal emoluments incident to the Cinque Port territories become by assignment his. By some writers this office is called a sinecure. But not so. Because the Lord Warden is busily employed at times in fobbing his perquisites; which are his chiefly by virtue of that same fobbing of them.&lt;/p&gt;\n\n&lt;p&gt;Now when these poor sun-burnt mariners, bare-footed, and with their trowsers rolled high up on their eely legs, had wearily hauled their fat fish high and dry, promising themselves a good L150 from the precious oil and bone; and in fantasy sipping rare tea with their wives, and good ale with their cronies, upon the strength of their respective shares; up steps a very learned and most Christian and charitable gentleman, with a copy of Blackstone under his arm; and laying it upon the whale\'s head, he says&amp;mdash;&quot;Hands off! this fish, my masters, is a Fast-Fish. I seize it as the Lord Warden\'s.&quot; Upon this the poor mariners in their respectful consternation&amp;mdash;so truly English&amp;mdash;knowing not what to say, fall to vigorously scratching their heads all round; meanwhile ruefully glancing from the whale to the stranger. But that did in nowise mend the matter, or at all soften the hard heart of the learned gentleman with the copy of Blackstone. At length one of them, after long scratching about for his ideas, made bold to speak,&lt;/p&gt;\n\n&lt;p&gt;&quot;Please, sir, who is the Lord Warden?&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;The Duke.&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;But the duke had nothing to do with taking this fish?&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;It is his.&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;We have been at great trouble, and peril, and some expense, and is all that to go to the Duke\'s benefit; we getting nothing at all for our pains but our blisters?&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;It is his.&quot;&lt;/p&gt;',NULL,NULL,'Tempo.pdf','And another test, the last?','&lt;p&gt;&quot;De balena vero sufficit, si rex habeat caput, et regina caudam.&quot; BRACTON, L. 3, C. 3.&lt;/p&gt; &lt;p&gt;Latin from the books of the Laws of&#8230;','Tempo.pdf','2012-07-30 15:35:20','2012-07-30 15:35:20',4,4),
	(21,'here-is-another-test',NULL,'here is another test','here is another test','','&lt;p&gt;&quot;De balena vero sufficit, si rex habeat caput, et regina caudam.&quot; BRACTON, L. 3, C. 3.&lt;/p&gt; &lt;p&gt;Latin from the books of the Laws of&#8230;','&lt;p&gt;&quot;De balena vero sufficit, si rex habeat caput, et regina caudam.&quot; BRACTON, L. 3, C. 3.&lt;/p&gt;\n\n&lt;p&gt;Latin from the books of the Laws of England, which taken along with the context, means, that of all whales captured by anybody on the coast of that land, the King, as Honourary Grand Harpooneer, must have the head, and the Queen be respectfully presented with the tail. A division which, in the whale, is much like halving an apple; there is no intermediate remainder. Now as this law, under a modified form, is to this day in force in England; and as it offers in various respects a strange anomaly touching the general law of Fast and Loose-Fish, it is here treated of in a separate chapter, on the same courteous principle that prompts the English railways to be at the expense of a separate car, specially reserved for the accommodation of royalty. In the first place, in curious proof of the fact that the above-mentioned law is still in force, I proceed to lay before you a circumstance that happened within the last two years.&lt;/p&gt;\n\n&lt;p&gt;It seems that some honest mariners of Dover, or Sandwich, or some one of the Cinque Ports, had after a hard chase succeeded in killing and beaching a fine whale which they had originally descried afar off from the shore. Now the Cinque Ports are partially or somehow under the jurisdiction of a sort of policeman or beadle, called a Lord Warden. Holding the office directly from the crown, I believe, all the royal emoluments incident to the Cinque Port territories become by assignment his. By some writers this office is called a sinecure. But not so. Because the Lord Warden is busily employed at times in fobbing his perquisites; which are his chiefly by virtue of that same fobbing of them.&lt;/p&gt;\n\n&lt;p&gt;Now when these poor sun-burnt mariners, bare-footed, and with their trowsers rolled high up on their eely legs, had wearily hauled their fat fish high and dry, promising themselves a good L150 from the precious oil and bone; and in fantasy sipping rare tea with their wives, and good ale with their cronies, upon the strength of their respective shares; up steps a very learned and most Christian and charitable gentleman, with a copy of Blackstone under his arm; and laying it upon the whale\'s head, he says&amp;mdash;&quot;Hands off! this fish, my masters, is a Fast-Fish. I seize it as the Lord Warden\'s.&quot; Upon this the poor mariners in their respectful consternation&amp;mdash;so truly English&amp;mdash;knowing not what to say, fall to vigorously scratching their heads all round; meanwhile ruefully glancing from the whale to the stranger. But that did in nowise mend the matter, or at all soften the hard heart of the learned gentleman with the copy of Blackstone. At length one of them, after long scratching about for his ideas, made bold to speak,&lt;/p&gt;\n\n&lt;p&gt;&quot;Please, sir, who is the Lord Warden?&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;The Duke.&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;But the duke had nothing to do with taking this fish?&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;It is his.&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;We have been at great trouble, and peril, and some expense, and is all that to go to the Duke\'s benefit; we getting nothing at all for our pains but our blisters?&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;It is his.&quot;&lt;/p&gt;',NULL,NULL,'Tempo.pdf','here is another test','&lt;p&gt;&quot;De balena vero sufficit, si rex habeat caput, et regina caudam.&quot; BRACTON, L. 3, C. 3.&lt;/p&gt; &lt;p&gt;Latin from the books of the Laws of&#8230;','Tempo.pdf','2012-07-30 15:36:43','2012-07-30 15:36:43',7,5),
	(22,'another-page',NULL,'another page','another page','','done','&lt;p&gt;&quot;De balena vero sufficit, si rex habeat caput, et regina caudam.&quot; BRACTON, L. 3, C. 3.&lt;/p&gt;\n\n&lt;p&gt;Latin from the books of the Laws of England, which taken along with the context, means, that of all whales captured by anybody on the coast of that land, the King, as Honourary Grand Harpooneer, must have the head, and the Queen be respectfully presented with the tail. A division which, in the whale, is much like halving an apple; there is no intermediate remainder. Now as this law, under a modified form, is to this day in force in England; and as it offers in various respects a strange anomaly touching the general law of Fast and Loose-Fish, it is here treated of in a separate chapter, on the same courteous principle that prompts the English railways to be at the expense of a separate car, specially reserved for the accommodation of royalty. In the first place, in curious proof of the fact that the above-mentioned law is still in force, I proceed to lay before you a circumstance that happened within the last two years.&lt;/p&gt;\n\n&lt;p&gt;It seems that some honest mariners of Dover, or Sandwich, or some one of the Cinque Ports, had after a hard chase succeeded in killing and beaching a fine whale which they had originally descried afar off from the shore. Now the Cinque Ports are partially or somehow under the jurisdiction of a sort of policeman or beadle, called a Lord Warden. Holding the office directly from the crown, I believe, all the royal emoluments incident to the Cinque Port territories become by assignment his. By some writers this office is called a sinecure. But not so. Because the Lord Warden is busily employed at times in fobbing his perquisites; which are his chiefly by virtue of that same fobbing of them.&lt;/p&gt;\n\n&lt;p&gt;Now when these poor sun-burnt mariners, bare-footed, and with their trowsers rolled high up on their eely legs, had wearily hauled their fat fish high and dry, promising themselves a good L150 from the precious oil and bone; and in fantasy sipping rare tea with their wives, and good ale with their cronies, upon the strength of their respective shares; up steps a very learned and most Christian and charitable gentleman, with a copy of Blackstone under his arm; and laying it upon the whale\'s head, he says&amp;mdash;&quot;Hands off! this fish, my masters, is a Fast-Fish. I seize it as the Lord Warden\'s.&quot; Upon this the poor mariners in their respectful consternation&amp;mdash;so truly English&amp;mdash;knowing not what to say, fall to vigorously scratching their heads all round; meanwhile ruefully glancing from the whale to the stranger. But that did in nowise mend the matter, or at all soften the hard heart of the learned gentleman with the copy of Blackstone. At length one of them, after long scratching about for his ideas, made bold to speak,&lt;/p&gt;\n\n&lt;p&gt;&quot;Please, sir, who is the Lord Warden?&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;The Duke.&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;But the duke had nothing to do with taking this fish?&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;It is his.&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;We have been at great trouble, and peril, and some expense, and is all that to go to the Duke\'s benefit; we getting nothing at all for our pains but our blisters?&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;It is his.&quot;&lt;/p&gt;',NULL,NULL,NULL,'another page','done',NULL,'2012-07-30 15:38:20','2012-07-30 15:38:20',9,6),
	(23,'This-is-the-page-title',NULL,'This is the page title','This is the page title','','&lt;p&gt;&quot;De balena vero sufficit, si rex habeat caput, et regina caudam.&quot; BRACTON, L. 3, C. 3.&lt;/p&gt; &lt;p&gt;Latin from the books of the Laws of&#8230;','&lt;p&gt;&quot;De balena vero sufficit, si rex habeat caput, et regina caudam.&quot; BRACTON, L. 3, C. 3.&lt;/p&gt;\n\n&lt;p&gt;Latin from the books of the Laws of England, which taken along with the context, means, that of all whales captured by anybody on the coast of that land, the King, as Honourary Grand Harpooneer, must have the head, and the Queen be respectfully presented with the tail. A division which, in the whale, is much like halving an apple; there is no intermediate remainder. Now as this law, under a modified form, is to this day in force in England; and as it offers in various respects a strange anomaly touching the general law of Fast and Loose-Fish, it is here treated of in a separate chapter, on the same courteous principle that prompts the English railways to be at the expense of a separate car, specially reserved for the accommodation of royalty. In the first place, in curious proof of the fact that the above-mentioned law is still in force, I proceed to lay before you a circumstance that happened within the last two years.&lt;/p&gt;\n\n&lt;p&gt;It seems that some honest mariners of Dover, or Sandwich, or some one of the Cinque Ports, had after a hard chase succeeded in killing and beaching a fine whale which they had originally descried afar off from the shore. Now the Cinque Ports are partially or somehow under the jurisdiction of a sort of policeman or beadle, called a Lord Warden. Holding the office directly from the crown, I believe, all the royal emoluments incident to the Cinque Port territories become by assignment his. By some writers this office is called a sinecure. But not so. Because the Lord Warden is busily employed at times in fobbing his perquisites; which are his chiefly by virtue of that same fobbing of them.&lt;/p&gt;\n\n&lt;p&gt;Now when these poor sun-burnt mariners, bare-footed, and with their trowsers rolled high up on their eely legs, had wearily hauled their fat fish high and dry, promising themselves a good L150 from the precious oil and bone; and in fantasy sipping rare tea with their wives, and good ale with their cronies, upon the strength of their respective shares; up steps a very learned and most Christian and charitable gentleman, with a copy of Blackstone under his arm; and laying it upon the whale\'s head, he says&amp;mdash;&quot;Hands off! this fish, my masters, is a Fast-Fish. I seize it as the Lord Warden\'s.&quot; Upon this the poor mariners in their respectful consternation&amp;mdash;so truly English&amp;mdash;knowing not what to say, fall to vigorously scratching their heads all round; meanwhile ruefully glancing from the whale to the stranger. But that did in nowise mend the matter, or at all soften the hard heart of the learned gentleman with the copy of Blackstone. At length one of them, after long scratching about for his ideas, made bold to speak,&lt;/p&gt;\n\n&lt;p&gt;&quot;Please, sir, who is the Lord Warden?&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;The Duke.&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;But the duke had nothing to do with taking this fish?&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;It is his.&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;We have been at great trouble, and peril, and some expense, and is all that to go to the Duke\'s benefit; we getting nothing at all for our pains but our blisters?&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;It is his.&quot;&lt;/p&gt;',NULL,NULL,'Tempo.pdf','This is the page title','&lt;p&gt;&quot;De balena vero sufficit, si rex habeat caput, et regina caudam.&quot; BRACTON, L. 3, C. 3.&lt;/p&gt; &lt;p&gt;Latin from the books of the Laws of&#8230;','Tempo.pdf','2012-07-30 15:40:52','2012-07-30 15:40:52',5,7),
	(24,'asdkjnaskjn',NULL,'asdkjnaskjn','asdkjnaskjn','','&lt;p&gt;&quot;What\'s that about Cods, ma\'am?&quot; said I, with much politeness.&lt;/p&gt;\n\n&lt;p&gt;&quot;Clam or Cod?&quot; she repeated.&lt;/p&gt;\n\n&lt;p&gt;&quot;A clam for supper? a cold clam; is THAT what you mean, Mrs. Hussey?&quot; says I, &quot;but that\'s a rather cold and clammy reception in the winter time, ain\'t it, Mrs. Hussey?&quot;&lt;/p&gt;\n\n&lt;p&gt;But being in a great hurry to resume scolding the man in the purple Shirt, who was waiting for it in the entry, and seeming to hear nothing but the word &quot;clam,&quot; Mrs. Hussey hurried towards an open door leading to the kitchen, and bawling out &quot;clam for two,&quot; disappeared.&lt;/p&gt;\n\n&lt;p&gt;&quot;Queequeg,&quot; said I, &quot;do you think that we can make out a supper for us both on one clam?&quot;&lt;/p&gt;\n\n&lt;p&gt;However, a warm savory steam from the kitchen served to belie the apparently cheerless prospect before us. But when that smoking chowder came in, the mystery was delightfully explained. Oh, sweet friends! hearken to me. It was made of small juicy clams, scarcely bigger than hazel nuts, mixed with pounded ship biscuit, and salted pork cut up into little flakes; the whole enriched with butter, and plentifully seasoned with pepper and salt. Our appetites being sharpened by the frosty voyage, and in particular, Queequeg seeing his favourite fishing food before him, and the chowder being surpassingly excellent, we despatched it with great expedition: when leaning back a moment and bethinking me of Mrs. Hussey\'s clam and cod announcement, I thought I would try a little experiment. Stepping to the kitchen door, I uttered the word &quot;cod&quot; with great emphasis, and resumed my seat. In a few moments the savoury steam came forth again, but with a different flavor, and in good time a fine cod-chowder was placed before us.&lt;/p&gt;\n\n&lt;p&gt;We resumed business; and while plying our spoons in the bowl, thinks I to myself, I wonder now if this here has any effect on the head? What\'s that stultifying saying about chowder-headed people? &quot;But look, Queequeg, ain\'t that a live eel in your bowl? Where\'s your harpoon?&quot;&lt;/p&gt;\n\n&lt;p&gt;Fishiest of all fishy places was the Try Pots, which well deserved its name; for the pots there were always boiling chowders. Chowder for breakfast, and chowder for dinner, and chowder for supper, till you began to look for fish-bones coming through your clothes. The area before the house was paved with clam-shells. Mrs. Hussey wore a polished necklace of codfish vertebra; and Hosea Hussey had his account books bound in superior old shark-skin. There was a fishy flavor to the milk, too, which I could not at all account for, till one morning happening to take a stroll along the beach among some fishermen\'s boats, I saw Hosea\'s brindled cow feeding on fish remnants, and marching along the sand with each foot in a cod\'s decapitated head, looking very slip-shod, I assure ye.&lt;/p&gt;\n\n&lt;p&gt;&lt;/p&gt;\n\n&lt;p&gt;&quot;Both,&quot; says I; &quot;and let\'s have a couple of smoked herring by way of variety.&quot;&lt;/p&gt;','&lt;p&gt;&quot;What\'s that about Cods, ma\'am?&quot; said I, with much politeness.&lt;/p&gt;\n\n&lt;p&gt;&quot;Clam or Cod?&quot; she repeated.&lt;/p&gt;\n\n&lt;p&gt;&quot;A clam for supper? a cold clam; is THAT what you mean, Mrs. Hussey?&quot; says I, &quot;but that\'s a rather cold and clammy reception in the winter time, ain\'t it, Mrs. Hussey?&quot;&lt;/p&gt;\n\n&lt;p&gt;But being in a great hurry to resume scolding the man in the purple Shirt, who was waiting for it in the entry, and seeming to hear nothing but the word &quot;clam,&quot; Mrs. Hussey hurried towards an open door leading to the kitchen, and bawling out &quot;clam for two,&quot; disappeared.&lt;/p&gt;\n\n&lt;p&gt;&quot;Queequeg,&quot; said I, &quot;do you think that we can make out a supper for us both on one clam?&quot;&lt;/p&gt;\n\n&lt;p&gt;However, a warm savory steam from the kitchen served to belie the apparently cheerless prospect before us. But when that smoking chowder came in, the mystery was delightfully explained. Oh, sweet friends! hearken to me. It was made of small juicy clams, scarcely bigger than hazel nuts, mixed with pounded ship biscuit, and salted pork cut up into little flakes; the whole enriched with butter, and plentifully seasoned with pepper and salt. Our appetites being sharpened by the frosty voyage, and in particular, Queequeg seeing his favourite fishing food before him, and the chowder being surpassingly excellent, we despatched it with great expedition: when leaning back a moment and bethinking me of Mrs. Hussey\'s clam and cod announcement, I thought I would try a little experiment. Stepping to the kitchen door, I uttered the word &quot;cod&quot; with great emphasis, and resumed my seat. In a few moments the savoury steam came forth again, but with a different flavor, and in good time a fine cod-chowder was placed before us.&lt;/p&gt;\n\n&lt;p&gt;We resumed business; and while plying our spoons in the bowl, thinks I to myself, I wonder now if this here has any effect on the head? What\'s that stultifying saying about chowder-headed people? &quot;But look, Queequeg, ain\'t that a live eel in your bowl? Where\'s your harpoon?&quot;&lt;/p&gt;\n\n&lt;p&gt;Fishiest of all fishy places was the Try Pots, which well deserved its name; for the pots there were always boiling chowders. Chowder for breakfast, and chowder for dinner, and chowder for supper, till you began to look for fish-bones coming through your clothes. The area before the house was paved with clam-shells. Mrs. Hussey wore a polished necklace of codfish vertebra; and Hosea Hussey had his account books bound in superior old shark-skin. There was a fishy flavor to the milk, too, which I could not at all account for, till one morning happening to take a stroll along the beach among some fishermen\'s boats, I saw Hosea\'s brindled cow feeding on fish remnants, and marching along the sand with each foot in a cod\'s decapitated head, looking very slip-shod, I assure ye.&lt;/p&gt;\n\n&lt;p&gt;&lt;/p&gt;\n\n&lt;p&gt;&quot;Both,&quot; says I; &quot;and let\'s have a couple of smoked herring by way of variety.&quot;&lt;/p&gt;',NULL,NULL,NULL,'asdkjnaskjn','&lt;p&gt;&quot;What\'s that about Cods, ma\'am?&quot; said I, with much politeness.&lt;/p&gt;\n\n&lt;p&gt;&quot;Clam or Cod?&quot; she repeated.&lt;/p&gt;\n\n&lt;p&gt;&quot;A clam for supper? a cold clam; is THAT what you mean, Mrs. Hussey?&quot; says I, &quot;but that\'s a rather cold and clammy reception in the winter time, ain\'t it, Mrs. Hussey?&quot;&lt;/p&gt;\n\n&lt;p&gt;But being in a great hurry to resume scolding the man in the purple Shirt, who was waiting for it in the entry, and seeming to hear nothing but the word &quot;clam,&quot; Mrs. Hussey hurried towards an open door leading to the kitchen, and bawling out &quot;clam for two,&quot; disappeared.&lt;/p&gt;\n\n&lt;p&gt;&quot;Queequeg,&quot; said I, &quot;do you think that we can make out a supper for us both on one clam?&quot;&lt;/p&gt;\n\n&lt;p&gt;However, a warm savory steam from the kitchen served to belie the apparently cheerless prospect before us. But when that smoking chowder came in, the mystery was delightfully explained. Oh, sweet friends! hearken to me. It was made of small juicy clams, scarcely bigger than hazel nuts, mixed with pounded ship biscuit, and salted pork cut up into little flakes; the whole enriched with butter, and plentifully seasoned with pepper and salt. Our appetites being sharpened by the frosty voyage, and in particular, Queequeg seeing his favourite fishing food before him, and the chowder being surpassingly excellent, we despatched it with great expedition: when leaning back a moment and bethinking me of Mrs. Hussey\'s clam and cod announcement, I thought I would try a little experiment. Stepping to the kitchen door, I uttered the word &quot;cod&quot; with great emphasis, and resumed my seat. In a few moments the savoury steam came forth again, but with a different flavor, and in good time a fine cod-chowder was placed before us.&lt;/p&gt;\n\n&lt;p&gt;We resumed business; and while plying our spoons in the bowl, thinks I to myself, I wonder now if this here has any effect on the head? What\'s that stultifying saying about chowder-headed people? &quot;But look, Queequeg, ain\'t that a live eel in your bowl? Where\'s your harpoon?&quot;&lt;/p&gt;\n\n&lt;p&gt;Fishiest of all fishy places was the Try Pots, which well deserved its name; for the pots there were always boiling chowders. Chowder for breakfast, and chowder for dinner, and chowder for supper, till you began to look for fish-bones coming through your clothes. The area before the house was paved with clam-shells. Mrs. Hussey wore a polished necklace of codfish vertebra; and Hosea Hussey had his account books bound in superior old shark-skin. There was a fishy flavor to the milk, too, which I could not at all account for, till one morning happening to take a stroll along the beach among some fishermen\'s boats, I saw Hosea\'s brindled cow feeding on fish remnants, and marching along the sand with each foot in a cod\'s decapitated head, looking very slip-shod, I assure ye.&lt;/p&gt;\n\n&lt;p&gt;&lt;/p&gt;\n\n&lt;p&gt;&quot;Both,&quot; says I; &quot;and let\'s have a couple of smoked herring by way of variety.&quot;&lt;/p&gt;',NULL,'2012-07-30 15:42:13','2012-07-30 15:42:13',0,8),
	(26,'This-is-a-new-great-title',NULL,'This is a new great title','This is a new great title','','Granser recollected himself, and with a start tore himself away from the rostrum of the lecture-hall, where, to another world audience, he had been expounding the','&lt;p&gt;Granser recollected himself, and with a start tore himself away from the  rostrum of the lecture-hall, where, to another world audience, he  had been expounding the latest theory, sixty years gone, of germs and  germ-diseases.&lt;/p&gt;\n\n&lt;p&gt;&quot;Yes, yes, Edwin; I had forgotten. Sometimes the memory of the past is  very strong upon me, and I forget that I am a dirty old man, clad in  goat-skin, wandering with my savage grandsons who are goatherds in  the primeval wilderness. \'The fleeting systems lapse like foam,\' and so  lapsed our glorious, colossal civilization. I am Granser, a tired old  man. I belong to the tribe of Santa Rosans. I married into that tribe.  My sons and daughters married into the Chauffeurs, the Sacramen-tos, and  the Palo-Altos. You, Hare-Lip, are of the Chauffeurs. You, Edwin, are  of the Sacramentos. And you, Hoo-Hoo, are of the Palo-Altos. Your tribe  takes its name from a town that was near the seat of another great  institution of learning. It was called Stanford University. Yes, I  remember now. It is perfectly clear. I was telling you of the Scarlet  Death. Where was I in my story?&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;You was telling about germs, the things you can\'t see but which make  men sick,&quot; Edwin prompted.&lt;/p&gt;\n\n&lt;p&gt;&quot;Yes, that\'s where I was. A man did not notice at first when only a few  of these germs got into his body. But each germ broke in half and became  two germs, and they kept doing this very rapidly so that in a short time  there were many millions of them in the body. Then the man was sick. He  had a disease, and the disease was named after the kind of a germ that  was in him. It might be measles, it might be influenza, it might be  yellow fever; it might be any of thousands and thousands of kinds of  diseases.&lt;/p&gt;\n\n&lt;p&gt;&quot;Now this is the strange thing about these germs. There were always new  ones coming to live in men\'s bodies. Long and long and long ago, when  there were only a few men in the world, there were few diseases. But  as men increased and lived closely together in great cities and  civilizations, new diseases arose, new kinds of germs entered their  bodies. Thus were countless millions and billions of human beings  killed. And the more thickly men packed together, the more terrible were  the new diseases that came to be. Long before my time, in the middle  ages, there was the Black Plague that swept across Europe. It swept  across Europe many times. There was tuberculosis, that entered into men  wherever they were thickly packed. A hundred years before my time there  was the bubonic plague. And in Africa was the sleeping sickness. The  bacteriologists fought all these sicknesses and destroyed them, just as  you boys fight the wolves away from your goats, or squash the mosquitoes  that light on you. The bacteriologists&amp;mdash;&quot;&lt;/p&gt;\n\n&lt;p&gt;&quot;But, Granser, what is a what-you-call-it?&quot; Edwin interrupted.&lt;/p&gt;\n\n&lt;p&gt;&quot;You, Edwin, are a goatherd. Your task is to watch the goats. You know a  great deal about goats. A bacteriologist watches germs. That\'s his  task, and he knows a great deal about them. So, as I was saying, the  bacteriologists fought with the germs and destroyed them&amp;mdash;sometimes.  There was leprosy, a horrible disease. A hundred years before I was  born, the bacteriologists discovered the germ of leprosy. They knew all  about it. They made pictures of it. I have seen those pictures. But  they never found a way to kill it. But in 1984, there was the Pantoblast  Plague, a disease that broke out in a country called Brazil and that  killed millions of people. But the bacteriologists found it out, and  found the way to kill it, so that the Pantoblast Plague went no farther.  They made what they called a serum, which they put into a man\'s body and  which killed the pantoblast germs without killing the man. And in 1910,  there was Pellagra, and also the hookworm. These were easily killed  by the bacteriologists. But in 1947 there arose a new disease that had  never been seen before. It got into the bodies of babies of only ten  months old or less, and it made them unable to move their hands and  feet, or to eat, or anything; and the bacteriologists were eleven years  in discovering how to kill that particular germ and save the babies.&lt;/p&gt;\n\n&lt;p&gt;&quot;In spite of all these diseases, and of all the new ones that continued  to arise, there were more and more men in the world. This was because it  was easy to get food. The easier it was to get food, the more men  there were; the more men there were, the more thickly were they packed  together on the earth; and the more thickly they were packed, the more  new kinds of germs became diseases. There were warnings. Soldervetzsky,  as early as 1929, told the bacteriologists that they had no guaranty  against some new disease, a thousand times more deadly than any they  knew, arising and killing by the hundreds of millions and even by the  billion. You see, the micro-organic world remained a mystery to the end.  They knew there was such a world, and that from time to time armies of  new germs emerged from it to kill men.&lt;/p&gt;',NULL,NULL,'Tempo.pdf','This is a new great title','Granser recollected himself, and with a start tore himself away from the rostrum of the lecture-hall, where, to another world audience, he had been expounding theâ€¦','Tempo.pdf','2012-07-31 13:01:58','2012-07-31 12:47:59',6,9);

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table registrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `registrations`;

CREATE TABLE `registrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `short_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `value` text,
  `required` tinyint(4) DEFAULT '0',
  `email` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `activated` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table users_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
